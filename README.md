# dna-automation
for running autotest with tags library 'cypress-grep' is used
https://github.com/cypress-io/cypress/tree/develop/npm/grep
command example:
 npx cypress run --env grepTags=@smoke
 # Use + to require both tags to be present
 npx cypress run --env grepTags=@smoke+@ui
 # do not run any tests with tag "@regression"   
 npx cypress run --env grepTags=-@regression
 # run only the tests without any tag
 npx cypress run --env grepUntagged=true
 #  to run all tests with tag @ui but without tag @validation
 npx cypress run --env grepTags=@ui+-@validation
 #  burn the filtered tests to make sure they are flake-free
 npx cypress run --env grepTags=@smoke, burn=5
 # run the tests by title
 npx cypress run --env grep="patterns"   
# run tests with "outcome", but without "create" in the titles 
 npx cypress run --env grep="outcome; -create"
 
created tags - @smoke, @regression, @integration, @reset, @exit, @api, @ui, @validation,

all tests that don't have tag @smoke are regression tests. Tag  @regression will be removed as needless

/--/--/
 mochawesome is used for reporting - https://www.npmjs.com/package/mochawesome
