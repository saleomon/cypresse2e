declare namespace Cypress {
  interface Cypress {
    env<T>(key: string): T
  }
}
