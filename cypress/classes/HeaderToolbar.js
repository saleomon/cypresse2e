export default class HeaderToolbar {
    constructor(){
        this.projectMenuButtonClassNameSelector = '.openProjectMenuIcon'
        this.profileButtonIdSelector = '#user'
    }

    projectMenuButton(){
        return cy.get(this.projectMenuButtonClassNameSelector).then($button =>{
            cy.wrap($button).then($wrapped =>{
                $wrapped
            })
        })
    }

    profileButton(){
        return cy.get(this.profileButtonIdSelector).then($button =>{
            cy.wrap($button).then($wrapped =>{
                $wrapped
            })
        })
    }  
}