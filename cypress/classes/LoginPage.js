export default class LoginPage {
    constructor(){
        this.userInputSelector = '[autocomplete="username"]'
        this.passwordInputSelector = '[autocomplete="password"]' 
        this.logInButtonSelector = '.ant-btn'
    }

    userInput(){
        return cy.get(this.userInputSelector).then($user =>{
            cy.wrap($user)
        })
    }

    passwordInput(){
        return cy.get(this.passwordInputSelector).then($password =>{
            cy.wrap($password)
        })
    }

    logInButton(){
        return cy.get(this.logInButtonSelector).then($loginB =>{
            cy.wrap($loginB)
        })
    }
    
    // static userInput = '[autocomplete="username"]'
    // static passwordInput = '[autocomplete="password"]'
    // static logInButton = '.ant-btn'
}