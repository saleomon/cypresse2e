export default class NodeDetailsPanel {
  constructor() {
    this.elementSelector = 'div[selectednode="[object Object]"]' // any other ideas? ;)
    this.headerSelector = '.header '
    this.closeButtonSelector = this.headerSelector + '.closeButton'
    this.tabsSelector = '.ui-tabs__header'
    this.collapseElementSelector = '.ant-collapse-header'
    this.propertiesTabSelector = this.elementSelector + ' #propertyTab'
    this.flowTabSelector = this.elementSelector + ' #flowTab'
    this.channelTabSelector = this.elementSelector + ' #channelTab'
    this.skillTabSelector = this.elementSelector + ' #skillTab'
    this.outputTabSelector = this.elementSelector + ' #OutputTab'
    this.inputTabSelector = this.elementSelector + ' #inputTab'
    this.nodeEdit = '[style="margin-top: 5px; font-weight: normal;"]'
    this.nodeDescriptionEditBtn = () => cy.get('[placeholder="Enter Node Description"]').parent().find('.contentEditButton')
    this.nodeDescriptionSaveBtn = () => cy.get('div').contains('Node Description').find('.editControls .saveItEditButton')
    this.addOutputPopOver = () => cy.get('.ant-popover').contains('Add New Output Condition')
    this.validationMessage = () => cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-form-explain')
    this.outputValidationMessage = () => cy.get('.ant-popover').contains('Add New Output Condition').next().find('.ant-form-explain')
    this.flowValidationMessage = () => cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-form-explain')
    this.flowNameField = () => cy.get('.ant-popover').contains('Add New Flow Condition').next().find('input').filter(':visible')
    this.outputNameField = () => cy.get('.ant-popover')
        .contains('Add New Output Condition')
        .next()
        .find('[placeholder="Enter a condition label."]')
        .filter(':visible')
    this.errorText = '[style="color: red;"]'
  }

  element() {
    return cy.get(this.elementSelector).then(($obj) => {
      cy.wrap($obj).then(($wrapped) => {
        $wrapped
      })
    })
  }

  closeButton() {
    return cy
      .get(this.elementSelector)
      .find(this.closeButtonSelector)
      .then(($obj) => {
        cy.wrap($obj).then(($wrapped) => {
          $wrapped
        })
      })
  }

  close() {
    this.closeButton().click()
    this.element().should('not.be.visible')
  }

  selectTab(tabName) {
    cy.get(this.tabsSelector).find('div').contains(tabName).should('be.visible').click({ force: true })
    cy.get(this.tabsSelector)
      .find('div')
      .contains(tabName)
      .parent()
      .then(($selectedTab) => {
        cy.wrap($selectedTab).should('have.attr', 'aria-selected', 'true')
      })
  }

  expand(collapseText) {
    cy.get(this.elementSelector).find(this.collapseElementSelector).contains(collapseText).should('be.visible').click()
    cy.get(this.elementSelector)
      .find(this.collapseElementSelector)
      .contains(collapseText)
      .then(($collapse) => {
        cy.wrap($collapse).should('have.attr', 'aria-expanded', 'true')
      })
  }

  collapse(collapseText) {
    cy.get(this.elementSelector).find(this.collapseElementSelector).contains(collapseText).should('be.visible').click()
    cy.get(this.elementSelector)
      .find(this.collapseElementSelector)
      .contains(collapseText)
      .then(($collapse) => {
        cy.wrap($collapse).should('not.have.attr', 'aria-expanded')
      })
  }

  headerText() {
    return cy
      .get(this.elementSelector)
      .find(this.headerSelector)
      .find('span')
      .first()
      .then(($headerText) => {
        cy.wrap($headerText).then(($wrapped) => {
          $wrapped
        })
      })
  }

  // properties tab
  nodeLabel() {
    return cy
      .get(this.elementSelector)
      .find('div')
      .contains('Node Label')
      .find('input')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  nodeType() {
    return cy
      .get(this.elementSelector)
      .find('div')
      .contains('Node Type')
      .find('input')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  nodeDescription() {
    return cy
      .get(this.elementSelector)
      .find('div')
      .contains('Node Description')
      .find('textarea')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  // flow tab
  addFlowButton() {
    return cy
      .get(this.flowTabSelector)
      .find('.addButton')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  addFlow(flowName, flowType, flowDescription) {
    this.fillFlowForm(flowName, flowType, flowDescription)
    cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-btn-primary').click()
    cy.wait(300)
  }

  fillFlowForm(flowName, flowType, flowDescription) {
    this.addFlowButton().click()
    cy.get('.ant-popover').contains('Add New Flow Condition').should('be.visible')
    cy.get('.ant-popover').contains('Add New Flow Condition').next().find('input').type(flowName)
    cy.get('.ant-popover')
      .contains('Add New Flow Condition')
      .next()
      .find('[role="combobox"]')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(flowType)
              .click()
          })
      })
    cy.get('.ant-popover').contains('Add New Flow Condition').next().find('textarea').type(flowDescription)
    cy.wait(300)
  }

  // channel tab
  addChannelButton() {
    return cy
      .get(this.channelTabSelector)
      .find('.addButton')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  addChannel(channelName) {
    this.fillChannelForm(channelName)
    cy.get('.ant-popover').contains('Add Channel').next().find('.ant-btn-primary').click()
    cy.wait(300)
  }

  fillChannelForm(channelName) {
    this.addChannelButton().click()
    cy.get('.ant-popover').contains('Add Channel').should('be.visible')
    cy.get('.ant-popover')
      .contains('Add Channel')
      .next()
      .find('[role="combobox"]')
      .should('be.visible')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(channelName, { matchCase: false })
              .click()
          })
      })
    cy.wait(300)
  }

  // skill tab
  addSkillButton() {
    return cy
      .get(this.skillTabSelector)
      .find('.addButton')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  fillSkillForm(skillName) {
    this.addSkillButton().click()
    cy.get('.ant-popover').contains('Add Existing Skill').should('be.visible')
    cy.get('.ant-popover')
      .contains('Add Existing Skill')
      .next()
      .find('[role="combobox"]')
      .should('be.visible')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(skillName, { matchCase: false })
              .click()
          })
      })
    cy.wait(300)
  }

  addSkill(skillName) {
    this.fillSkillForm(skillName)
    cy.get('.ant-popover').contains('Add Existing Skill').next().find('.ant-btn-primary').click()
  }

  // output tab

  addOutputButton() {
    return cy
      .get(this.outputTabSelector)
      .find('.addButton')
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  fillOutputForm(outputLabel, outputContent, outputType, outputDescription) {
    this.addOutputPopOver().should('be.visible')
    this.addOutputPopOver().next().find('[placeholder="Enter a condition label."]').filter(':visible').type(outputLabel)
    this.addOutputPopOver()
      .next()
      .find('[placeholder="Enter Voice or Chat Prompt for User."]')
      .filter(':visible')
      .type(outputContent)
    this.addOutputPopOver()
      .next()
      .find('[role="combobox"]')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(outputType)
              .click()
          })
      })
    this.addOutputPopOver().next().find('[placeholder="Describe this for developers."]').type(outputDescription)
  }

  addOutput(outputLabel, outputContent, outputType, outputDescription) {
    this.addOutputButton().click()
    this.fillOutputForm(outputLabel, outputContent, outputType, outputDescription)
    this.addOutputPopOver().next().find('.ant-btn-primary').click()
    this.addOutputPopOver().should('not.be.visible')
  }

  editOutput(target, outputLabel, outputType, outputDescription) {
    cy.get('.contentHeader').contains(target).parent().find('button').scrollIntoView().click()
    cy.get('.ant-dropdown-menu-item').contains('Edit Condition Properties').should('be.visible').click()
    cy.get('.ant-popover').contains('Edit Output Condition').should('be.visible')
    cy.get('.ant-popover')
      .contains('Edit Output Condition')
      .next()
      .find('[placeholder="Enter a condition label."]')
      .filter(':visible')
      .clear()
      .type(outputLabel)
    cy.get('.ant-popover')
      .contains('Edit Output Condition')
      .next()
      .find('[role="combobox"]')
      .click()
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('li')
              .contains(outputType)
              .click()
          })
      })
    cy.get('.ant-popover')
      .contains('Edit Output Condition')
      .next()
      .find('[placeholder="Describe this for developers."]')
      .clear()
      .type(outputDescription)
    cy.get('.ant-popover').contains('Edit Output Condition').next().find('.ant-btn-primary').click()
    // cy.get('.ant-popover').contains('Edit Output Condition').should('not.be.visible') */
  }

  deleteOutput(outputLabel) {
    cy.get('.contentHeader').contains(outputLabel).parent().find('button').scrollIntoView().click()
    cy.get('.ant-dropdown-menu-item').contains('Delete Item').should('be.visible').click()
  }

  editNodeLabel(testData, errorMessage) {
    cy.get(this.nodeEdit).find('.contentEditButton').click()
    cy.get(this.nodeEdit).find('.ant-input').clear().type(testData)
    cy.get(this.nodeEdit).find('.contentEdit > .editControls > .saveItEditButton').click()
    cy.get('[style="color: red;"]').should('contain', errorMessage)
    cy.get(this.nodeEdit).find('.contentEdit > .editControls > .cancelEditButton').click()
  }
  
  editNodeLabelNoError(testData) {
    cy.get(this.nodeEdit).find('.contentEditButton').click()
    cy.get(this.nodeEdit).find('.ant-input').clear().type(testData)
    cy.get(this.nodeEdit).find('.contentEdit > .editControls > .saveItEditButton').click()
   }

  clickCancel() {
    cy.get(this.nodeEdit).find('.contentEdit > .editControls > .cancelEditButton').click()
  } 

  checkPreBridgeOutput(label) {
    cy.get('#OutputTab')
      .find('div')
      .contains('PRE')
      .next()
      .find('.outputListItem')
      .contains(label)
      .parent()
      .find('.itemConditionType')
      .contains(PRE_BRIDGE)
      .should('exist')
      .should('be.visible')
  }
}
