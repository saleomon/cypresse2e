export class DiagramBar {
  constructor(
    private dialogBarSel = '.toolbarWrapper',
    private closeBtnSel = dialogBarSel + ' .closeButton',
    private openBtnSel = dialogBarSel + ' .openButton',
    private deleteNodeBtnSel = dialogBarSel + ' [aria-label="Delete Outline icon"]',
    private cutLinkToParentBtnSel = dialogBarSel + ' [aria-label="Link Off icon"]',
    private revertToAutoLayoutBtnSel = dialogBarSel + ' [aria-label="Content Save Off Outline icon"]',
    private saveLayoutBtnSel = dialogBarSel + ' [aria-label="Content Save Outline icon"]',
    private entitiesViewerBtnSel = dialogBarSel + ' [aria-label="Database Plus Outline icon"]',
    private yesBtnSel = 'button[class="ant-btn ant-btn-primary ant-btn-sm"]',
    private noBtnSel = 'button[class="ant-btn ant-btn-sm"]',
    private userNameSel = (username: string) => cy.xpath(`//div[@role="gridcell" and contains(text(), "${username}")]`),
    private contentDuplicateIcon = '[aria-label="Content Duplicate icon"]',
    private duplicateContentBtnSel = dialogBarSel + ' [aria-label="Content Duplicate icon"]',
    private duplicateDialogSel = '//div[@class="ant-popover-title" and contains(text(), "Duplicate")]',
    private selectChannelDialogSel = '//div[@class="ant-popover-title" and contains(text(), "Select Channel")]',
    private duplicateToChannelSel = '//div[@class = "class-list" and contains(text(), "Duplicate To Channel")]',
    private channelSel = (channel: string) =>
      cy.xpath(`//div[@class="ant-popover-inner-content"]/*[text()="${channel}"]`),
    private warningMessageSel = 'div[class="ant-modal-content"]',
    private cancelBtnSel = '//span[text() = "Cancel"]',
    private duplicateBtnSel = '//span[text() = "Duplicate"]',
    private duplicateNodeSel = '//div[@class = "class-list" and contains(text(), "Duplicate Node")]',
    private toastMessageSel = 'div[class="ant-message-notice-content"]',
    private duplicateButtonSel = '//span[@class="material-design-icon content-duplicate-icon icons"]',
  ) {}

  clickOnYesBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.yesBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnNoBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.noBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnOpenBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.openBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnCloseBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.closeBtnSel).should('be.visible').click(opt)
  }

  clickOnDeleteNodeBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.deleteNodeBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnCutToParentBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.cutLinkToParentBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnRevertToAutoLayoutBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.revertToAutoLayoutBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnSaveLayoutBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.saveLayoutBtnSel).should('be.visible').click(opt)

    return this
  }

  clickOnEntitiesViewerBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.entitiesViewerBtnSel).should('be.visible').click(opt)

    return this
  }

  checkUserName(username: string, opt?: Partial<Cypress.ClickOptions>) {
    this.userNameSel(username).should('contain.text', username)
    this.userNameSel(username).should('be.visible').click()

    return this
  }

  clickContentDuplicateButton(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.contentDuplicateIcon).should('be.visible').click(opt)

    return this
  }

  clickOnDuplicateContentBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.duplicateContentBtnSel).should('be.visible').click(opt)

    return this
  }

  isDuplicateDialogDisplayed() {
    cy.xpath(this.duplicateDialogSel).should('be.visible')

    return this
  }

  isDuplicateDialogClosed() {
    cy.xpath(this.duplicateDialogSel).should('not.be.visible')

    return this
  }

  isDuplicateToChannelBtnDisplayed() {
    cy.xpath(this.duplicateToChannelSel).should('be.visible')

    return this
  }

  clickOnDuplicateToChannelBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.duplicateToChannelSel).should('be.visible').click(opt)

    return this
  }

  isDuplicateNodeBtnDisplayed() {
    cy.xpath(this.duplicateNodeSel).should('be.visible')

    return this
  }

  clickOnDuplicateNodeBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.duplicateNodeSel).should('be.visible').click(opt)

    return this
  }

  isSelectChannelDialogDisplayed() {
    cy.xpath(this.selectChannelDialogSel).should('be.visible')

    return this
  }

  isSelectChannelDialogClosed() {
    cy.xpath(this.selectChannelDialogSel).should('not.be.visible')

    return this
  }

  selectChannelFromDialog(channel: string, opt?: Partial<Cypress.ClickOptions>) {
    this.channelSel(channel).should('be.visible').click(opt)

    return this
  }

  isChannelDisplayed(channel: string) {
    this.channelSel(channel).should('be.visible').should('contain.text', channel)

    return this
  }

  isWarningMessageDisplayed(text: string) {
    cy.get(this.warningMessageSel).should('be.visible').should('contain.text', text)

    return this
  }

  isWarningMessageClosed() {
    cy.get(this.warningMessageSel).should('not.exist')

    return this
  }

  clickOnCancelBtn() {
    cy.xpath(this.cancelBtnSel).should('be.visible').click({force: true})

    return this
  }

  isCancelBtnDisplayed() {
    cy.xpath(this.cancelBtnSel).should('be.visible')

    return this
  }

  clickOnDuplicateBtn(opt: Partial<Cypress.ClickOptions> = {}) {
    opt.force = true

    cy.xpath(this.duplicateBtnSel).should('be.visible').click(opt)

    return this
  }

  isDuplicateBtnDisplayed() {
    cy.xpath(this.duplicateBtnSel).should('be.visible')

    return this
  }

  isToastMessageDisplayed(text: string) {
    cy.get(this.toastMessageSel).should('be.visible').should('contain.text', text)

    return this
  }
}

export default new DiagramBar()
