export class DuplicatePopUp {
  constructor(
    private duplicateChannelSel = `//div[contains(text(),'Duplicate To Channel')]`,
    private duplicateNodeSel = '//div[contains(text(),"Duplicate Node")]',

  ) {}

  checkDuplicateChannel() {
    cy.xpath(this.duplicateChannelSel).should('be.visible')

    return this
  }

  checkDuplicateNode() {
    cy.xpath(this.duplicateNodeSel).should('be.visible')

    return this
  }

  clickDuplicateChannel(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.duplicateChannelSel).should('be.visible').click(opt)

    return this
  }
}

export default new DuplicatePopUp()
