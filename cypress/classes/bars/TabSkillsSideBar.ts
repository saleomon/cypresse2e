import { GeneralTabSideBar } from './GeneralTabSideBar'
import { PropertiesPanelSel } from './PropertiesPanelSel'

class TabSkillsSideBar extends GeneralTabSideBar {
  constructor(
    private addButton = () => cy.get(PropertiesPanelSel.addButtonSel), // '.addButton'
  ) {
    super()
  }

  openAddExistingSkill() {
    this.addButton().should('be.visible').click()

    return this
  }

}

export default new TabSkillsSideBar()
