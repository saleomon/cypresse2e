export class UserSideBar {
  constructor(
    private projectControlsSel = `//div[@class='ant-collapse-header' and contains(text(),'Project Controls')]`,
    private deleteButtonSel = 'button[class="ant-btn ant-btn-danger"]',
    private yesButtonSel = 'button[class="ant-btn ant-btn-primary ant-btn-sm"]',
    private reenableButtonSel = 'button[class="ant-btn"]',
  ) {}

  clickProjectControl(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.projectControlsSel).should('be.visible').click(opt)

    return this
  }

  clickDeleteBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.deleteButtonSel).should('be.visible').click(opt)

    return this
  }

  clickYesBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.get(this.yesButtonSel)
      .should('be.visible')
      .click({ multiple: true, ...opt })

    return this
  }

  clickReEnableBtn() {
    cy.get(this.reenableButtonSel).should('be.visible').click()

    return this
  }

  checkEnableBtn() {
    cy.get(this.reenableButtonSel).should('be.visible')
  }

  isNotExistReEnableBtn() {
    cy.get(this.reenableButtonSel).should('not.exist')

    return this
  }
}

export default new UserSideBar()
