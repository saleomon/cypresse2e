export class WarningPopUp {
  constructor(
    protected okButtonSel = `//button[@class='ant-btn ant-btn-primary']//span[contains(text(),'OK')]`,
    protected cancelButtonSel = '//div[@class="ant-modal-confirm-btns"]//button[@class="ant-btn"]',
  ) {}

  clickOkBtn(opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true

    cy.xpath(this.okButtonSel).should('be.visible').click(opt)

    return this
  }

  checkOkButton(){
    cy.xpath(this.okButtonSel).should('be.visible')
  }

  checkCancelButton() {
    cy.xpath(this.cancelButtonSel).should('be.visible')
  }

  clickCancelButton(opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true

    cy.xpath(this.cancelButtonSel).should('be.visible').click(opt)
  }
}

export default new WarningPopUp()
