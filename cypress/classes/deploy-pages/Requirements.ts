export class Requirements {
  constructor(
    private tabRadioButton = () => cy.get('#radioGroup .ant-radio-button-wrapper'),
    public requirementsTableSelector = '.ag-theme-alpine',
    private removeGroupFromDropAreaButton = (groupName: string) =>
      cy.get('.ag-column-drop-cell').contains(groupName).parent().find('.ag-icon-cancel'),
    private dropZoneEmptyMessage = () =>
      cy.get('.ag-column-drop-empty:not(.ag-hidden) .ag-column-drop-horizontal-empty-message'),
  ) {}

  openTabByName(tab: string, opt?: Partial<Cypress.ClickOptions>) {
    this.tabRadioButton().contains(tab).click(opt)

    return this
  }

  // eslint-disable-next-line class-methods-use-this
  dragColumnAsFilter(column: Cypress.Chainable<JQuery<HTMLElement>>) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    column.drag('.ag-column-drop-wrapper', { force: true })
  }

  removeGroupFromDropArea(groupName: string, opt?: Partial<Cypress.ClickOptions>) {
    this.removeGroupFromDropAreaButton(groupName).click(opt)
  }

  dropZoneEmptyMessageIsVisible() {
    this.dropZoneEmptyMessage().should('be.visible').should('have.text', 'Drag here to set row groups')
  }
}
export default new Requirements()
