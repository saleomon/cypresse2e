import { DialogPopOver } from '../DialogPopOver'

export class AddProjectedOutcomePopover extends DialogPopOver {
  constructor(
    public outcomeNameLabel = () => cy.get('[title="Outcome Name"]'),
    public outcomePercentageLabel = () => cy.get('[title="Percentage"]'),
    public outcomeNameField = () => cy.get('[data-cy="outcomeLabelField"]'),
    public outcomeDescriptionField = () => cy.get('[placeholder="Describe this Outcome."]'),
    public outcomePercentage = () => cy.get('[role="spinbutton"]'),
    public outcomeValueSel = '[title="Value"]',
    public outcomeColorSel = '[title="Color"]',
    public addProjectedOutcomeBtn = () => cy.get('[data-cy="outcomeFormSubmit"]'),
    public addProjectedOutcomeBtnDataCy = () => cy.get('outcomeFormSubmit'),
    public increaseOutcomePercentage = () => cy.get('[aria-label="Increase Value"]'),
    public decreaseOutcomePercentage = () => cy.get('[aria-label="Decrease Value"]'),
    public outcomeNameValidationMsg = () => cy.get('.ant-form-explain'),
  ) {
    super()
  }

  fillOutcome(outcome: object) {
    this.outcomeNameField().filter(':visible').clear().type(outcome.projectedOutcomeLabel)
    if (outcome.outcomeDescription.length > 0) {
      this.outcomeDescriptionField().filter(':visible').clear().type(outcome.outcomeDescription)
    }
    this.outcomePercentage().filter(':visible').clear().type(outcome.outcomePercentage)
    cy.selectCustom(this.outcomeValueSel, outcome.outcomeValue)
    cy.selectCustom(this.outcomeColorSel, outcome.outcomeColor)
  }

  checkDefaultValue(select: string, value: string) {
    cy.get(select).parent().next().find('.ant-select-selection-selected-value').should('have.text', value)
  }
}

export default new AddProjectedOutcomePopover()
