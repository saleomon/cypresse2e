import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewOpenQuestionNodePopUp extends AddNewNodePopUp {
  constructor(
    protected templateSel = () => cy.get('div[class="ant-select ant-select-enabled ant-select-sm"]'),
    protected defaultOpenQuestionSel = () => cy.get('[placeholder="Enter default content"]'),
  ) {
    super()
  }

  isTemplateSelected(text: string) {
    this.templateSel().should('contain.text', text)

    return this
  }

  isDefaultOpenQuestionSet(text: string) {
    this.defaultOpenQuestionSel().should('contain.value', text)

    return this
  }

  setDefaultOpenQuestion(text: string) {
    this.defaultOpenQuestionSel().type(text)
    return this
  }

}

export default new AddNewOpenQuestionNodePopUp()
