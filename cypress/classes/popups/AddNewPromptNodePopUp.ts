import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewPromptNodePopUp extends AddNewNodePopUp {
  constructor(
    protected templateSel = () => cy.get('div[class="ant-select ant-select-enabled ant-select-sm"]'),
    protected defaultPromptSel = () => cy.get('[placeholder="Enter default content"]'),
  ) {
    super()
  }

  isTemplateSelected(text: string) {
    this.templateSel().should('contain.text', text)

    return this
  }

  isDefaultPromptSel(text: string) {
    this.defaultPromptSel().should('contain.value', text)

    return this
  }

  setDefaultPrompt(text: string) {
    this.defaultPromptSel().type(text)

    return this
  }
}

export default new AddNewPromptNodePopUp()
