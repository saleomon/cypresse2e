import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewSkillRouterNodePopUp extends AddNewNodePopUp {
  constructor(
    protected skillsSel = () => cy.xpath('(//span[@class="ant-form-item-children"])[3]'),
    protected chooseSkillSel = () =>
      cy.get('[class="ant-select-dropdown-menu-item ant-select-dropdown-menu-item-active"]'),
  ) {
    super()
  }

  setSkills() {
    this.skillsSel().should('be.visible').click()
    this.chooseSkillSel().should('be.visible').click()

    return this
  }

  isSkillSet(text: string) {
    this.skillsSel().should('have.text', text)

    return this
  }
}

export default new AddNewSkillRouterNodePopUp()
