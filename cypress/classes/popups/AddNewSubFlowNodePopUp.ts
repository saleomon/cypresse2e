import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewSubFlowNodePopUp extends AddNewNodePopUp {
  constructor(
    protected voipSel = () => cy.get('li[title="VoIP"]'),
    protected removeVoipSel = () => cy.xpath('(//span [@class="ant-select-selection__choice__remove"])[4]'),
    protected channelWarningMessageSel = () => cy.get('[style="color: orange; text-align: center;"]'),
  ) {
    super()
  }

  isVoipBtnDisplayed() {
    this.voipSel().should('be.visible')

    return this
  }

  clickOnRemoveVoipBtn() {
    this.removeVoipSel().should('be.visible').click()

    return this
  }

  isWarningMessageDisplayed(text: string) {
    this.channelWarningMessageSel().should('have.text', text)
    return this
  }
}

export default new AddNewSubFlowNodePopUp()
