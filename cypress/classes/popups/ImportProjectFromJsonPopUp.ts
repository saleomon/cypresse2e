export class ImportProjectFromJsonPopUp {
  constructor(
    protected submitButtonSel = (`button[class='ant-btn ant-btn-primary']`),
  ) {
  }

  clickSubmitBtn(opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true

    cy.get(this.submitButtonSel).should('be.visible').click(opt)

    return this
  }
}

export default new ImportProjectFromJsonPopUp()
