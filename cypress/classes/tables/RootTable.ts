export class RootTable {
  constructor(
    protected editBtnTableSel = '.editButton',
    protected leftContainerSel = '[ref="eLeftContainer"]',
    protected centerContainerSel = '[ref="eCenterContainer"]',
    protected row = '[role="row"]',
    protected deleteBtnTableSel = '*[class^="deleteButton"]',
  ) {}

  checkTableInfo(tableSelector: string, expectedRow: [object]) {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call
    cy.get(tableSelector)
      // @ts-ignore
      .getAgGridData()
      .then((fullTable: [object]) => {
        fullTable.forEach(function (row: object, index: number) {
          // @ts-ignore
          Object.keys(row).forEach((key: string) => expect(expectedRow[index][key]).equals(row[key]))
        })
      })

    return this
  }

  getDeleteBtn(table: string, name: string) {
    return cy.get(table).find(this.row).contains(name).next(this.deleteBtnTableSel)
  }

  getEditBtn(table: string, name: string) {
    // cy.get(table).find('[ref="eBodyViewport"]').scrollTo('bottom')
    return cy
      .get(table)
      .find(this.leftContainerSel)
      .find('[role="row"]')
      .contains(name)
      .parent()
      .children(this.editBtnTableSel)
  }
}
export default new RootTable()
