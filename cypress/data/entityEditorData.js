const contentsEntityEditor = {
    entityLabel:              'UserName',
    mockValue:                'John White',
    mockValueUpd:             'Jack Black',
    entityLabelUpd:           'EditedEntity',
    enitityDescription:       'Description',
    type:                     'Date',
    formatType:               'Short Date',
    typeUpd:                  ' String ',
    formatTypeUpd:            'Formal Name',
    enitityDescriptionUpd:    'Description Updated',
    dateFormatArray:          [' Short Date ', ' Long Date ', ' Hour '],
    numberFormatArray:        [' None ',' Currency ',' SSN ',' Telephone '],
    stringFormatArray:        [' None ',' Formal Name '],
    entityRow:                [ {"Entity Label":"UserName","Type":"date","PII":"true","Writable":"true","Group":"defaultEntityGroup","Formatter":"Short Date","Mock Value":"John White","Description":"Description"}],
    entityRowUpd:             [ {"Entity Label":"EditedEntity","Type":"string","PII":"false","Writable":"false","Group":"defaultEntityGroup","Formatter":"Formal Name","Mock Value":"Jack Black","Description":"Description Updated"}],
    
    entityGroupRow:           [ {"Entity Group":"PII","Entity List":"","Primary Key":"EntityForEntityGroup","Depends On":"EntityForEntityGroup","Shared Endpoint":"true","Namespace":"Test Automation Project/TestPattern EntityEditor/PII","Description":"Description"}], 
    entityGroupLabel:         'PII',
    enitityGroupDescription:  'Personal Identity Information',
    entityGroupLabelUpd:      'Location',
    enitityGroupDescriptionUpd: 'Location',
    entityGroupModal:         'Edit Outcome',
    entityGroupPrimaryKey:    'EntityForEntityGroup',
    entityGroupPrimaryKeyUpd: 'EntityUpdForEntityGroup',
    dependsOn:                ["EntityForEntityGroup"]
};

export {contentsEntityEditor}