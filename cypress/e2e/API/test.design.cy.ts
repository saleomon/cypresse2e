import { ProjectInfo } from '../../types/response/ProjectInfo'
import UsersPage from '../../classes/pages/UsersPage'
import CreateNewProjectPopUp from '../../classes/popups/CreateNewProjectPopUp'
import LeftNavigationBar from '../../classes/navigation/LeftNavigationBar'
import UserSideBar from '../../classes/bars/UserSideBar'
import ProjectsPage from '../../classes/control-panel/projectsPage'
import LoginPage from '../../classes/pages/LoginPage'
import NavigationBar from '../../classes/navigation/NavigationBar'
import DesignerPage from '../../classes/pages/DesignerPage'

it('[CMTS-17650] - Verify that Able to Create a new project with the Project Label same as the deleted Project Label', () => {
  cy.loginUI()
  UsersPage.clickCreateNewProject()
  CreateNewProjectPopUp.setProjectNameField('Alex')
    .setDescription('description')
    .selectTypeProject('POC')
    .clickAddButton()
  LeftNavigationBar.clickOnSettingBtn()
  UsersPage.clickNameProject('Alex')
  UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  let username = 'username_' + Date.now()
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'Alex',
        description: 'description',
        projectType: 'POC',
        type: 'POC',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).to.eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).to.eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).to.eql('Contained')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  UsersPage.checkDescriptionProject('description')
  ProjectsPage.checkProjectType('Alex', 'POC')
})

it('[CMTS-16321] - Verify that Error code is displayed in the response when the project lable field  in Json is blank', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      failOnStatusCode: false,
      body: {
        label: '',
        description: 'Test1',
        projectType: 'Test',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.body.name).eql('ValidationError')
      expect(resp2.body.message).eql('Validation Failed')
      expect(resp2.body.statusCode).eql(400)
      expect(resp2.body.error).eql('Bad Request')
      expect(resp2.body.details.body[0].message).to.eql(`"label" is not allowed to be empty`)
      expect(resp2.body.details.body[0].path).to.eql(['label'])
      expect(resp2.body.details.body[0].type).to.eql('string.empty')
      expect(resp2.body.details.body[0].context.label).to.eql('label')
      expect(resp2.body.details.body[0].context.value).to.eql('')
      expect(resp2.body.details.body[0].context.key).to.eql('label')
    })
  })
  LoginPage.open().setUsername('dhb-admin').setPassword('dhb@dm!n123').clickOnLoginButton()
  LeftNavigationBar.clickOnSettingBtn()
  UsersPage.isNotExitsProject('')
})

it('[CMTS-16463] - Verify that Error validation code should be displayed when Project Type is invalid', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      failOnStatusCode: false,
      body: {
        label: 'TestOne',
        description: 'TestOne',
        projectType: '',
        type: 'Test',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.body.name).eql('ValidationError')
      expect(resp2.body.message).eql('Validation Failed')
      expect(resp2.body.statusCode).eql(400)
      expect(resp2.body.error).eql('Bad Request')
      expect(resp2.body.details.body[0].message).to.eql(`"projectType" is not allowed to be empty`)
      expect(resp2.body.details.body[0].path).to.eql(['projectType'])
      expect(resp2.body.details.body[0].type).to.eql('string.empty')
      expect(resp2.body.details.body[0].context.label).to.eql('projectType')
      expect(resp2.body.details.body[0].context.value).to.eql('')
      expect(resp2.body.details.body[0].context.key).to.eql('projectType')
    })
  })
})

// it('[CMTS-16464] - Verify that Error validation code is displayed when Outcome Id  value is blank', () => {
//   cy.request({
//     method: 'POST',
//     url: Cypress.env('baseUrlAPI') + '/auth/login',
//     body: {
//       username: Cypress.env('username'),
//       password: Cypress.env('password'),
//     },
//   }).then((resp1) => {
//     expect(resp1.body.status).eql('success')
//     expect(resp1.body.code).eql(200)
//     expect(resp1.body.message).eql('successfully retrieved token')
//     expect(resp1.body.payload.id).a('string')
//     expect(resp1.body.payload.userName).eql('dhb-admin')
//     expect(resp1.body.payload.email).eql(null)
//     expect(resp1.body.payload.isAdmin).eql(true)
//     expect(resp1.body.payload.accessToken).a('string')
//     expect(resp1.body.payload.expiresIn).eql(600)
//     expect(resp1.body.payload.refreshToken).a('string')
//     expect(resp1.body.payload.refreshExpiresIn).eql(1800)
//
//     cy.request({
//       method: 'POST',
//       url: Cypress.env('baseUrlAPI') + '/api/projects/import',
//       headers: {
//         Authorization: 'Bearer ' + resp1.body.payload.accessToken,
//       },
//       failOnStatusCode: false,
//       body: {
//         label: 'TestOne',
//         description: 'Test Create new project',
//         projectType: 'Test',
//         type: 'Test',
//         projectObjectVersion: 1,
//         outcomesBaseline: [],
//         outcomes: [
//           {
//             label: 'HITL',
//             description: 'Human in the Loop',
//             type: 'outcome',
//             outcomeType: 'Project',
//             outcomeValue: 'Neutral',
//             color: '#8FBCFF',
//             percentage: 33,
//             reasons: [],
//           },
//           {
//             label: 'Abandon',
//             description: 'User exits system without completion point.',
//             type: 'outcome',
//             outcomeType: 'Project',
//             outcomeValue: 'Negative',
//             color: '#FFBFCB',
//             percentage: 33,
//             reasons: [],
//           },
//           {
//             label: 'Contained',
//             description: 'User completes task without Human Assistance, Golden Call.',
//             type: 'outcome',
//             outcomeType: 'Project',
//             outcomeValue: 'Positive',
//             color: '#FFED8F',
//             percentage: 33,
//             reasons: [],
//           },
//         ],
//         templates: {
//           defaultPromptTemplate: {
//             label: 'DefaultPromptTemplate',
//             description: 'This is the default prompt template.',
//             templateType: 'prompt',
//             id: 'defaultPromptTemplate',
//             hasPre: true,
//             hasPost: true,
//             hasRepair: true,
//             channelConditions: [
//               {
//                 channelId: 'IVR',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'RWC',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'SMS',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'App',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'rSMS',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'VoIP',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//             ],
//           },
//           defaultOpenTemplate: {
//             label: 'DefaultOpenTemplate',
//             description: 'This is the default open template.',
//             templateType: 'openQuestion',
//             id: 'defaultOpenTemplate',
//             hasPre: true,
//             hasPost: true,
//             hasRepair: true,
//             channelConditions: [
//               {
//                 channelId: 'IVR',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'RWC',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'SMS',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'App',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'rSMS',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'VoIP',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//             ],
//           },
//           defaultClosedTemplate: {
//             label: 'DefaultClosedTemplate',
//             description: 'This is the default closed template.',
//             templateType: 'closedQuestion',
//             id: 'defaultClosedTemplate',
//             hasPre: true,
//             hasPost: true,
//             hasRepair: true,
//             channelConditions: [
//               {
//                 channelId: 'IVR',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'RWC',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'SMS',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'App',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'rSMS',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//               {
//                 channelId: 'VoIP',
//                 output: [],
//                 input: {
//                   models: [],
//                 },
//                 flow: [],
//               },
//             ],
//           },
//         },
//         patterns: [],
//       },
//     }).then((resp2) => {
//       expect(resp2.body.name).eql('ValidationError')
//       expect(resp2.body.message).eql('Validation Failed')
//       expect(resp2.body.statusCode).eql(400)
//       expect(resp2.body.error).eql('Bad Request')
//       expect(resp2.body.details.body[0].message).to.eql(
//         `""outcomes.8c777d4f-4d31-477f-a526-d0937b5c94f7.id" is not allowed to be empty`,
//       )
//       expect(resp2.body.details.body[0].path[0]).to.eql(['outcomes'])
//       expect(resp2.body.details.body[0].path[1]).to.eql(['8c777d4f-4d31-477f-a526-d0937b5c94f7'])
//       expect(resp2.body.details.body[0].path[2]).to.eql(['id'])
//       expect(resp2.body.details.body[0].type).to.eql('string.empty')
//       expect(resp2.body.details.body[0].context.label).to.eql('outcomes.8c777d4f-4d31-477f-a526-d0937b5c94f7.id')
//       expect(resp2.body.details.body[0].context.value).to.eql('')
//       expect(resp2.body.details.body[0].context.key).to.eql('id')
//     })
//   })
// })

it('[CMTS-16322] - Verify that Error code is displayed when project label contains less than 2char and morethan32 char', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer' + resp1.body.payload.accessToken,
      },
      body: {
        label: 't',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
      failOnStatusCode: false,
    }).then((resp2) => {
      expect(resp2.status).to.eql(403)
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  UsersPage.isNotExitsProject('t')

  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'testtesttesttesttesttesttesttesttest12345',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: {
          '8c777d4f-4d31-477f-a526-d0937b5c94f7': {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19': {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          '51075149-9e23-4684-8813-ce7d17d86a32': {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        },
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
      failOnStatusCode: false,
    }).then((resp2) => {
      expect(resp2.status).to.eql(403)
    })
  })
  UsersPage.isNotExitsProject('testtesttesttesttesttesttesttesttest12345')
})
it('[CMTS-16323] - Verify that Error code is displayed, on trying to create duplicate project', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'Test@123',
        description: 'Test Create new project',
        projectType: 'Test',
        type: 'Test',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
      failOnStatusCode: false,
    }).then((resp2) => {
      expect(resp2.status).eql(403)
      // expect(resp2.message).eql('Project label must be unique')
      // expect(resp2.body.status).eql('fail')
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  UsersPage.isNotExitsProject('Test@123')
})
it('[CMTS-16324] - Verify that Able to create New Project by providing the special characters in project Label using API Services', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'Test@34%$^&&*',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).to.eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).to.eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).to.eql('Contained')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  UsersPage.checkNameProject('Test@34%$^&&*')
  LeftNavigationBar.clickOutcomecTab()
  NavigationBar.selectProject('Test@34%$^&&*')
  DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')

  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'Test@35%$^&&*',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp3) => {
      expect(resp3.status).eql(201)
      expect(resp3.body.projectObject.outcomes[0].label).to.eql('HITL')
      expect(resp3.body.projectObject.outcomes[1].label).to.eql('Abandon')
      expect(resp3.body.projectObject.outcomes[2].label).to.eql('Contained')
      expect(resp3.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
      expect(resp3.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
      expect(resp3.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
    })
    cy.loginUI()
    LeftNavigationBar.clickOnSettingBtn()
    UsersPage.checkNameProject('Test@35%$^&&*')
    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('Test@35%$^&&*')
    DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/auth/login',
      body: {
        username: Cypress.env('username'),
        password: Cypress.env('password'),
      },
    }).then((resp5) => {
      expect(resp5.body.status).eql('success')
      expect(resp5.body.code).eql(200)
      expect(resp5.body.message).eql('successfully retrieved token')
      expect(resp5.body.payload.id).a('string')
      expect(resp5.body.payload.userName).eql('dhb-admin')
      expect(resp5.body.payload.email).eql(null)
      expect(resp5.body.payload.isAdmin).eql(true)
      expect(resp5.body.payload.accessToken).a('string')
      expect(resp5.body.payload.expiresIn).eql(600)
      expect(resp5.body.payload.refreshToken).a('string')
      expect(resp5.body.payload.refreshExpiresIn).eql(1800)

      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/api/projects/import',
        headers: {
          Authorization: 'Bearer ' + resp1.body.payload.accessToken,
        },
        body: {
          label: 'Test@36%$^&&*',
          description: 'Test Create new project',
          projectType: 'Test.',
          type: 'project',
          projectObjectVersion: 1,
          outcomesBaseline: [],
          outcomes: [
            {
              id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
              label: 'HITL',
              description: 'Human in the Loop',
              type: 'outcome',
              outcomeType: 'Project',
              outcomeValue: 'Neutral',
              color: '#8FBCFF',
              percentage: 33,
              reasons: [],
            },
            {
              id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
              label: 'Abandon',
              description: 'User exits system without completion point.',
              type: 'outcome',
              outcomeType: 'Project',
              outcomeValue: 'Negative',
              color: '#FFBFCB',
              percentage: 33,
              reasons: [],
            },
            {
              id: '51075149-9e23-4684-8813-ce7d17d86a32',
              label: 'Contained',
              description: 'User completes task without Human Assistance, Golden Call.',
              type: 'outcome',
              outcomeType: 'Project',
              outcomeValue: 'Positive',
              color: '#FFED8F',
              percentage: 33,
              reasons: [],
            },
          ],
          templates: {
            defaultPromptTemplate: {
              label: 'DefaultPromptTemplate',
              description: 'This is the default prompt template.',
              templateType: 'prompt',
              id: 'defaultPromptTemplate',
              hasPre: true,
              hasPost: true,
              hasRepair: true,
              channelConditions: [
                {
                  channelId: 'IVR',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'RWC',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'SMS',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'App',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'rSMS',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'VoIP',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
              ],
            },
            defaultOpenTemplate: {
              label: 'DefaultOpenTemplate',
              description: 'This is the default open template.',
              templateType: 'openQuestion',
              id: 'defaultOpenTemplate',
              hasPre: true,
              hasPost: true,
              hasRepair: true,
              channelConditions: [
                {
                  channelId: 'IVR',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'RWC',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'SMS',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'App',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'rSMS',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'VoIP',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
              ],
            },
            defaultClosedTemplate: {
              label: 'DefaultClosedTemplate',
              description: 'This is the default closed template.',
              templateType: 'closedQuestion',
              id: 'defaultClosedTemplate',
              hasPre: true,
              hasPost: true,
              hasRepair: true,
              channelConditions: [
                {
                  channelId: 'IVR',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'RWC',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'SMS',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'App',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'rSMS',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
                {
                  channelId: 'VoIP',
                  output: [],
                  input: {
                    models: [],
                  },
                  flow: [],
                },
              ],
            },
          },
          patterns: [],
        },
      }).then((resp3) => {
        expect(resp3.status).eql(201)
        expect(resp3.body.projectObject.outcomes[0].label).to.eql('HITL')
        expect(resp3.body.projectObject.outcomes[1].label).to.eql('Abandon')
        expect(resp3.body.projectObject.outcomes[2].label).to.eql('Contained')
        expect(resp3.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
        expect(resp3.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
        expect(resp3.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
      })
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      UsersPage.checkNameProject('Test@36%$^&&*')
      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('Test@36%$^&&*')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
})
it('[CMTS-16461] - Verify that able to create new project by providing the valid "Project Type" using API', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'TestOne',
        description: 'Test Create new project',
        projectType: 'Test',
        type: 'Test',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).to.eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).to.eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).to.eql('Contained')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  ProjectsPage.clickProjectLabel().checkProjectType('TestOne', 'Test')
  LeftNavigationBar.clickOutcomecTab()
  NavigationBar.selectProject('TestOne')
  DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')

  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewTestTwo',
        description: 'Test Create new project',
        projectType: 'POC',
        type: 'POC',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).to.eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).to.eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).to.eql('Contained')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  ProjectsPage.clickProjectLabel().checkProjectType('NewTestTwo', 'POC')
  LeftNavigationBar.clickOutcomecTab()
  NavigationBar.selectProject('NewTestTwo')
  DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')

  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'newTestThree',
        description: 'Test Create new project',
        projectType: 'Client',
        type: 'Client',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).to.eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).to.eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).to.eql('Contained')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).to.eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).to.eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).to.eql('DefaultClosedTemplate')
    })
  })
  cy.loginUI()
  LeftNavigationBar.clickOnSettingBtn()
  ProjectsPage.checkProjectType('newTestThree', 'Client')
  LeftNavigationBar.clickOutcomecTab()
  NavigationBar.selectProject('TestThree')
  DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
})

it('[CMTS-16461] - Verify that able to create new project by providing the valid "Project Type" using API', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'FirstTestProject',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectType('FirstTestProject', 'Test.')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('FirstTestProject')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })

  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'SecondTestProject',
        description: 'Test Create new project',
        projectType: 'POC.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectType('SecondTestProject', 'POC.')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('SecondTestProject')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })

  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'ThirdTestProject',
        description: 'Test Create new project',
        projectType: 'Client.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectType('ThirdTestProject', 'Client.')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('ThirdTestProject')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
})

it('[CMTS-16462] - Verify that Error Validation code should be displayed when the Project Type value is blank', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'TestOne',
        description: 'Test Create new project',
        projectType: '',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
      failOnStatusCode: false,
    }).then((resp2) => {
      expect(resp2.body.name).eql('ValidationError')
      expect(resp2.body.message).eql('Validation Failed')
      expect(resp2.status).eql(400)
      expect(resp2.body.error).eql('Bad Request')
      expect(resp2.body.details.body[0].message).eql('"projectType" is not allowed to be empty')
    })
  })
})

it('[CMTS-16318] - Verify that By Default 3 Outcomes and 3 default templates are created when New Project created using API Services', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectOne',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectOne')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectOne')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectTwo',
        description: 'Test Create new project',
        projectType: 'POC.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectTwo')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectTwo')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectTwoThree',
        description: 'Test Create new project',
        projectType: 'Client.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectTwoThree')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectTwoThree')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
})

it('[CMTS-16319] - Verify that Able to create New Project  by Keeping the Description as blank using API Services', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectFour',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectFour')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectFour')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectFive',
        description: 'Test123456',
        projectType: 'POC.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectFive')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectFive')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectSix',
        description: 'Test Create new project',
        projectType: 'Client.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectSix')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectSix')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
})

it('[CMTS-16320] - Verify that Able to create New Project that contains label as alpha, numeric and Alpha-numeric values using API Services', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectSeven',
        description: 'Test Create new project',
        projectType: 'Test.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectSeven')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectSeven')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectEight',
        description: 'Test Create new project',
        projectType: 'POC.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectEight')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectEight')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  }).then((resp1) => {
    expect(resp1.body.status).eql('success')
    expect(resp1.body.code).eql(200)
    expect(resp1.body.message).eql('successfully retrieved token')
    expect(resp1.body.payload.id).a('string')
    expect(resp1.body.payload.userName).eql('dhb-admin')
    expect(resp1.body.payload.email).eql(null)
    expect(resp1.body.payload.isAdmin).eql(true)
    expect(resp1.body.payload.accessToken).a('string')
    expect(resp1.body.payload.expiresIn).eql(600)
    expect(resp1.body.payload.refreshToken).a('string')
    expect(resp1.body.payload.refreshExpiresIn).eql(1800)

    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      headers: {
        Authorization: 'Bearer ' + resp1.body.payload.accessToken,
      },
      body: {
        label: 'NewProjectNine',
        description: 'Test Create new project',
        projectType: 'Client.',
        type: 'project',
        projectObjectVersion: 1,
        outcomesBaseline: [],
        outcomes: [
          {
            id: '8c777d4f-4d31-477f-a526-d0937b5c94f7',
            label: 'HITL',
            description: 'Human in the Loop',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Neutral',
            color: '#8FBCFF',
            percentage: 33,
            reasons: [],
          },
          {
            id: '5ce47155-2d5f-49ae-a7eb-ecc14dff0c19',
            label: 'Abandon',
            description: 'User exits system without completion point.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Negative',
            color: '#FFBFCB',
            percentage: 33,
            reasons: [],
          },
          {
            id: '51075149-9e23-4684-8813-ce7d17d86a32',
            label: 'Contained',
            description: 'User completes task without Human Assistance, Golden Call.',
            type: 'outcome',
            outcomeType: 'Project',
            outcomeValue: 'Positive',
            color: '#FFED8F',
            percentage: 33,
            reasons: [],
          },
        ],
        templates: {
          defaultPromptTemplate: {
            label: 'DefaultPromptTemplate',
            description: 'This is the default prompt template.',
            templateType: 'prompt',
            id: 'defaultPromptTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultOpenTemplate: {
            label: 'DefaultOpenTemplate',
            description: 'This is the default open template.',
            templateType: 'openQuestion',
            id: 'defaultOpenTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
          defaultClosedTemplate: {
            label: 'DefaultClosedTemplate',
            description: 'This is the default closed template.',
            templateType: 'closedQuestion',
            id: 'defaultClosedTemplate',
            hasPre: true,
            hasPost: true,
            hasRepair: true,
            channelConditions: [
              {
                channelId: 'IVR',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'RWC',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'SMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'App',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'rSMS',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
              {
                channelId: 'VoIP',
                output: [],
                input: {
                  models: [],
                },
                flow: [],
              },
            ],
          },
        },
        patterns: [],
      },
    }).then((resp2) => {
      expect(resp2.status).eql(201)
      expect(resp2.body.projectObject.outcomes[0].label).eql('HITL')
      expect(resp2.body.projectObject.outcomes[1].label).eql('Abandon')
      expect(resp2.body.projectObject.outcomes[2].label).eql('Contained')

      expect(resp2.body.projectObject.templates.defaultOpenTemplate.label).eql('DefaultOpenTemplate')
      expect(resp2.body.projectObject.templates.defaultPromptTemplate.label).eql('DefaultPromptTemplate')
      expect(resp2.body.projectObject.templates.defaultClosedTemplate.label).eql('DefaultClosedTemplate')

      cy.loginAPI()
      cy.loginUI()
      LeftNavigationBar.clickOnSettingBtn()
      ProjectsPage.checkProjectExist('NewProjectNine')

      LeftNavigationBar.clickOutcomecTab()
      NavigationBar.selectProject('NewProjectNine')
      DesignerPage.checkOutcomes('Abandon').checkOutcomes('Contained').checkOutcomes('HITL')
    })
  })
})
