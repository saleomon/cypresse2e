import path from 'path'
import projectsPage, { ProjectsPage } from '../../../classes/control-panel/projectsPage'

import ProjectAction from '../../../classes/popovers/ProjectAction'
import { LeftSidebarHrefs } from '../../../fixtures/navigation-data/left-sidebar-hrefs'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import NavigationBar from '../../../classes/navigation/NavigationBar'

let projectName: string
let secondProjectName: string

describe('[CMTS-14500] Managing projects(Control Panel)', function () {
  beforeEach(() => {
    cy.fixture('projects-templates/add-node-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        projectName = projectInfo.label
      })
    })
  })

  it('CMTS-14319,CMTS-14321,CMTS-14322- Delete project and verify project properties', () => {
    cy.fixture('projects-templates/add-node-project.json').as('secondprojects')
    cy.get<ProjectInfo>('@secondprojects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        secondProjectName = projectInfo.label
        NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
        projectsPage.selectProject(projectName)
        ProjectAction.expandProjectControl('Project Controls')
        ProjectAction.deleteProject(projectName)
        ProjectAction.isDeleted()
        ProjectAction.actionTabIsClose()
        cy.reload()
        projectsPage.selectProject(secondProjectName)
        ProjectAction.actionTabHeaderLbl(secondProjectName)
        ProjectAction.expandProjectControl('Project Controls')
        ProjectAction.deleteProject(projectName)
      })
    })
  })

  it('CMTS-16058,CMTS-16408,CMTS-16409- Update field values-Label,Description and Type', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    projectsPage.selectProject(projectName)
    ProjectAction.clickEditProjectProperty()
    ProjectAction.expandProjectControl('Project Properties')
    ProjectAction.updateProjectLabel('edited')
    ProjectAction.updateProjectDescription('updateddescription')
    ProjectAction.updateProjectType('POC')
    ProjectAction.saveActions()
    projectsPage.getRowProjectInfo(projectName + 'edited', 'POC', 'updateddescription')
    NavigationBar.projectIsVisible(projectName + 'edited')
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    projectsPage.selectProject(projectName + 'edited')
    ProjectAction.expandProjectControl('Project Properties')
    ProjectAction.checkdescriptionUpdated('updateddescription')
    ProjectAction.checkLabelUpdated(projectName + 'edited')
    ProjectAction.checkProjectTypeUpdated('POC')
    ProjectAction.expandProjectControl('Project Controls')
    ProjectAction.deleteProject(projectName + 'edited')
  })

  it('CMTS-16060- Field updates made can be reset', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    projectsPage.selectProject(projectName)
    ProjectAction.clickEditProjectProperty()
    ProjectAction.expandProjectControl('Project Properties')
    ProjectAction.updateProjectLabel('edited')
    ProjectAction.updateProjectDescription('updateddescription')
    ProjectAction.updateProjectType('POC')
    ProjectAction.resetActions()
    ProjectAction.checkdescriptionUpdated('')
    ProjectAction.checkLabelUpdated(projectName)
    ProjectAction.checkProjectTypeUpdated('Test')
    ProjectAction.expandProjectControl('Project Controls')
    NavigationBar.projectNotVisible(projectName + 'edited')
    NavigationBar.projectIsVisible(projectName)
    ProjectAction.deleteProject(projectName)
  })

  it('CMTS-14323- project JSON export', () => {
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    projectsPage.selectProject(projectName)
    ProjectAction.expandProjectControl('Project Controls')
    ProjectAction.exportProject()
    const downloadsFolder = Cypress.config('downloadsFolder')
    cy.readFile(path.join(downloadsFolder, projectName + '.json')).should('exist')
    projectsPage.selectProject(projectName)
    ProjectAction.expandProjectControl('Project Controls')
    ProjectAction.deleteProject(projectName)
  })
})
