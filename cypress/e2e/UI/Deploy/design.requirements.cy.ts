import { ProjectInfo } from '../../../types/response/ProjectInfo'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import { LeftSidebarHrefs } from '../../../fixtures/navigation-data/left-sidebar-hrefs'
import { DeployTopBarHrefs } from '../../../fixtures/navigation-data/deploy-top-bar-hrefs'
import FilterBar from '../../../classes/bars/FilterBar'
import DesignRequirements from '../../../classes/deploy-pages/DesignRequirements'
import Requirements from '../../../classes/deploy-pages/Requirements'
import Table from '../../../classes/tables/Table'
import flowsForDesignRequirementsTest from '../../../fixtures/data/deploy-requirements-design-tab/subflow-promt-flows'
import dataForCollapsedFlows from '../../../fixtures/data/deploy-requirements-design-tab/table-data-for-collapsed-flows'

let expandedFlowInfo: [object]
let collapsedFlowInfo: object
const flowName: string = 'SubFlow1'

describe('[CMTS-11207] - Design Requirements page', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/qa_autotest.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        // @ts-ignore
        expandedFlowInfo = flowsForDesignRequirementsTest(json.label)
        collapsedFlowInfo = dataForCollapsedFlows(json.label)
      })
      NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.deploy)
      FilterBar.selectPattern('subflow-prompt')
      NavigationBar.clickTopBarButton(DeployTopBarHrefs.designRequirements)
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it.skip('User should be able to expand and collapse flow', () => {
    Table.expandTableRowByText(flowName)

    DesignRequirements.checkFlowDiagramState('be.visible').checkSelectedFlowTableState('be.visible')
    Table.collapseTableRowByText(flowName)
    DesignRequirements.checkFlowDiagramState('not.be.visible').checkSelectedFlowTableState('not.be.visible')
  })

  it('User should be able to expand and collapse details of selected flow', () => {
    Table.expandTableRowByText(flowName)

    Table.checkTableLength(DesignRequirements.selectedFlowTable(), 3)
    Table.expandTableRowByText('Start').checkTableLength(DesignRequirements.selectedFlowTable(), 4)
    Table.expandTableRowByText('Prompt1').checkTableLength(DesignRequirements.selectedFlowTable(), 9)
    Table.collapseTableRowByText('Prompt1').checkTableLength(DesignRequirements.selectedFlowTable(), 4)
  })

  it('User should be able to see full info about flow in table', () => {
    Table.expandTableRowByText(flowName)

    Table.expandTableRowByText('Start')
    Table.expandTableRowByText('Prompt1')
    Table.expandTableRowByText('UserAbandonment')
    Table.checkTableInfo(DesignRequirements.selectedFlowTable(), expandedFlowInfo)
  })

  it('User should be able to see diagram as image of selected flow', () => {
    Table.expandTableRowByText(flowName)

    DesignRequirements.checkFlowDiagramState('be.visible')
  })

  it('Check colors of flows in table', () => {
    Table.expandTableRowByText(flowName)

    Table.checkTableLength(DesignRequirements.selectedFlowTable(), 3)
    DesignRequirements.checkColorOfSelectedFlowRow('Start', '204, 238, 159')
    DesignRequirements.checkColorOfSelectedFlowRow('Prompt1', '234, 220, 147')
    DesignRequirements.checkColorOfSelectedFlowRow('UserAbandonment', '130, 180, 255')
  })

  it('Table should be updated after pattern changing', () => {
    Table.checkTableInfo(
      Requirements.requirementsTableSelector,
      // @ts-ignore
      collapsedFlowInfo['Pattern - subflow-prompt, SubFlow1'],
    )
    FilterBar.selectPattern('empty')
    // @ts-ignore
    Table.checkTableInfo(Requirements.requirementsTableSelector, collapsedFlowInfo['Pattern - empty, All Flows'])
  })

  it('Table should be updated after flow changing', () => {
    // @ts-ignore
    Table.checkTableInfo(
      Requirements.requirementsTableSelector,
      // @ts-ignore
      collapsedFlowInfo['Pattern - subflow-prompt, SubFlow1'],
    )
    FilterBar.selectFlow('All Flows')
    Table.checkTableInfo(
      Requirements.requirementsTableSelector,
      // @ts-ignore
      collapsedFlowInfo['Pattern - subflow-prompt, All Flows'],
    )
  })

  it('Table should reflect data from all flows if [All Flows] is selected', () => {
    FilterBar.selectPattern('subflow-prompt')
    FilterBar.selectFlow('All Flows')
    Table.checkTableInfo(
      Requirements.requirementsTableSelector,
      // @ts-ignore
      collapsedFlowInfo['Pattern - subflow-prompt, All Flows'],
    )
  })
})
