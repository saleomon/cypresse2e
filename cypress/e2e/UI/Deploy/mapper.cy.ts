import AddNewSubFlowNodePopUp from '../../../classes/popups/AddNewSubFlowNodePopUp'
import AddNewSkillRouterNodePopUp from '../../../classes/popups/AddNewSkillRouterNodePopUp'
import AddNewOpenQuestionNodePopUp from '../../../classes/popups/AddNewOpenQuestionNodePopUp'
import AddNewClosedQuestionNodePopUp from '../../../classes/popups/AddNewClosedQuestionNodePopUp'
import FlowConditionPopOver from '../../../classes/popovers/FlowConditionPopOver'
import PaletteBox from '../../../classes/PaletteBox'
import DiagramBar from '../../../classes/bars/DiagramBar'
import FilterBar from '../../../classes/bars/FilterBar'
import Mapper from '../../../classes/deploy-pages/Mapper'
import DiagramUtils from '../../../support/DiagramUtils'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import TabFlowSideBar from '../../../classes/bars/TabFlowSideBar'
import TabOutputSideBar from '../../../classes/bars/TabOutputSideBar'
import GeneralTabSideBar from '../../../classes/bars/GeneralTabSideBar'

import { DesignerTopBarHrefs } from '../../../fixtures/navigation-data/designer-top-bar-hrefs'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import {
  dataMapper,
  rowDataEndPoints,
  rowDataFlow,
  rowData9751,
  rowAllDataEndPoints,
  before9681,
  after9681,
  skillRouter9806Before,
  skillRouter9806After,
} from '../../../fixtures/data/deploy-mapper/mapperData'
import LoginPage from '../../../classes/pages/LoginPage'
import GenerateFlowsPopUp from '../../../classes/popups/GenerateFlowsPopUp'
import ConfigureHeartReachPopUp from '../../../classes/popups/ConfigureHeartReachPopUp'
import LeftNavigationBar from '../../../classes/navigation/LeftNavigationBar'

describe('Mapper page', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/mapper.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        Cypress.env('projectName', projectInfo.label) // to input project name as the part of variable 'tag description'
        NavigationBar.selectProject(projectInfo.label)
      })
    })
  })

  after(() => {
    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })
})

it.skip(
  'CMTS-9681-to-validate-the-edited-nodes-flow-is-displayed-correct-in-the-mapper-tabel',
  { tags: '@smoke' },
  () => {
    NavigationBar.openMapperPage() // open page Mapper
    FilterBar.selectPattern('PatternEditFlow')
    FilterBar.selectFlow('All Flows') // select all flows
    Mapper.chooseTab('All') // select all tags
    Mapper.checkRowValuesObject('[row-id="8"]', before9681) // value of rowID vary for diff scenarios
    NavigationBar.openFlowsPage() // open page Flows
    FilterBar.selectPattern('PatternEditFlow')
    DiagramUtils.findDiagram()
    DiagramUtils.click('PromptNode9681') // open content panel for node Prompt Node
    TabFlowSideBar.openFlowTab()
    TabFlowSideBar.flowActions('default')
    TabFlowSideBar.selectActionEditProperties()
    FlowConditionPopOver.setLabel('new')
    FlowConditionPopOver.clickOnSaveBtn() // rename flow
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('PatternEditFlow')
    FilterBar.selectFlow('All Flows') // select all flows
    Mapper.chooseTab('All') // select all tags
    Mapper.checkRowValuesObject('[row-id="8"]', after9681) // check if tagID is changes on page Mapper
  },
)

it(
  'CMTS-9884-to-validate-the-added-question-to-the-open-node-is-displayed-inside-the-outputs-tab-on-the',
  { tags: '@smoke' },
  () => {
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('Test Skill Name')
    DiagramUtils.findDiagram()
    DiagramUtils.click('OpenQ')
    // change output question from default  to new one
    GeneralTabSideBar.selectTabByName('Output')
    TabOutputSideBar.editOutput('Default', 'question')
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('Test Skill Name')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Output')
    Mapper.checkCellByTagId('O_OpenQ_Default', '[col-id="payload"]', 'question') // check if payload is renamed on tab output
  },
)

it(
  'CMTS-9886-to-validate-removed-outputs-are-not-displayed-inside-the-outputs-tab-on-the-mapper-page',
  { tags: '@smoke' },
  () => {
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('OpenQuestionDeleteOutput')
    DiagramUtils.findDiagram()
    DiagramUtils.click('OpenQ') // open content panel for node Open Question
    GeneralTabSideBar.selectTabByName('Output')
    TabOutputSideBar.deleteOutput('Default')
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('OpenQuestionDeleteOutput')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Output')
    Mapper.checkCellByTagId('O_OpenQ_Default', '[col-id="payload"]', '') // check if payload is empty on tab output
  },
)

it(
  'CMTS-9808-to-validate-the-added-outputs-are-displayed-inside-the-outputs-tab-on-the-mapper-page',
  { tags: '@smoke' },
  () => {
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('Pattern9808')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram('#closeNode', 'AddNode1') // add node Open Question // instead of let palleteBox = new PaletteBox()
    AddNewOpenQuestionNodePopUp.setLabel('OpenQ')
    AddNewOpenQuestionNodePopUp.setDefaultOpenQuestion('question')
    AddNewOpenQuestionNodePopUp.clickOnAddBtn()
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('Pattern9808')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Output')
    Mapper.checkCellByTagId('O_OpenQ_Default', '[col-id="payload"]', 'question') // check if payload is equal to default open question
  },
)

it(
  'CMTS-9809-to-validate-the-added-inputs-are-displayed-inside-the-inputs-tab-on-the-mapper-page',
  { tags: '@smoke' },
  () => {
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ClosedQuestionPattern')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram('#openNode', 'AddNode1') // add node Open Question
    AddNewClosedQuestionNodePopUp.setLabel('CloseQuestion')
    AddNewClosedQuestionNodePopUp.setDefaultClosedQuestion('question')
    AddNewClosedQuestionNodePopUp.clickOnAddBtn()
    NavigationBar.openMapperPage()
    // three tabs are displayed: Mapper, Requirements, Flow viewer
    cy.contains('.topLink.router-link-active', 'Mapper').should('be.visible')
    cy.contains('.topLink', 'Requirements').should('be.visible')
    cy.contains('.topLink', 'Design Requirements').should('be.visible')
    FilterBar.selectPattern('ClosedQuestionPattern')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Input')
    // check if node appear on tab output
    Mapper.checkCellByTagId('I_CloseQuestion_OfferParser', '[col-id="payload"]', 'Parser: OfferParser NLUModel') // check if payload is equal to default open question
  },
)

it(
  'CMTS-9751-to-validate-the-related-tagid-will-be-removed-from-the-mapper-tabel-after-deleting-the',
  { tags: '@smoke' },
  () => {
    NavigationBar.openMapperPage() // open page Mapper
    FilterBar.selectPattern('PatternDeleteNode')
    Cypress.env('patternName', 'PatternDeleteNode')
    FilterBar.selectFlow('All Flows') // select all flows
    Mapper.chooseTab('All') // select all tags
    NavigationBar.openPatternsPage() // open page Flows
    FilterBar.selectPattern('PatternDeleteNode')
    DiagramUtils.findDiagram()
    DiagramUtils.click('SubFlow9751')
    DiagramBar.clickOnDeleteNodeBtn()
    DiagramBar.clickOnYesBtn() // delete node
    NavigationBar.openMapperPage() // open page Mapper
    FilterBar.selectPattern('PatternDeleteNode')
    FilterBar.selectFlow('All Flows') // select all flows
    Mapper.chooseTab('All') // select all tags
    Mapper.checkRowValuesNew('[row-id="0"]', rowData9751, Cypress.env('projectName'))
  },
)

it(
  'CMTS-9873-to-validate-the-added-endpoints-are-displayed-inside-the-endpoint-tab-on-the-mapper-page',
  { tags: '@smoke' },
  () => {
    // open page Mapper
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('PatternEndPoint')
    Cypress.env('patternName', 'PatternEndPoint')
    // select all flows
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Endpoints')
    Mapper.checkRowValuesNew('[row-id="0"]', rowDataEndPoints[0]) // perhaps to check all table info it's better to use method checkTableAG but
    Mapper.checkRowValuesNew('[row-id="1"]', rowDataEndPoints[1])
    Mapper.checkRowValuesNew('[row-id="2"]', rowDataEndPoints[2])
    Mapper.checkRowValuesNew('[row-id="3"]', rowDataEndPoints[3])
    Mapper.checkRowValuesNew('[row-id="4"]', rowDataEndPoints[4])
    Mapper.checkRowValuesNew('[row-id="5"]', rowDataEndPoints[5])
    Mapper.checkRowValuesNew('[row-id="6"]', rowDataEndPoints[6])

    /*    // expand node details -  question - should check data with expanded details - take additional time to run autotest
     Mapper.expandNodeDetails()
     cy.contains(Mapper.columnGroupText, 'Node Details').next('[ref="agOpened"]').should('not.have.class', 'ag-hidden')
     // Mapper.checkHeadersWithNodeDetails()
     Mapper.checkFullRowValues('[row-id="0"]', rowAllDataEndPoints[0], Cypress.env('projectName'))
     Mapper.checkFullRowValues('[row-id="1"]', rowAllDataEndPoints[1], Cypress.env('projectName'))
     Mapper.checkFullRowValues('[row-id="2"]', rowAllDataEndPoints[2], Cypress.env('projectName'))
     Mapper.checkFullRowValues('[row-id="3"]', rowAllDataEndPoints[3], Cypress.env('projectName'))
     Mapper.checkFullRowValues('[row-id="4"]', rowAllDataEndPoints[4], Cypress.env('projectName'))
     Mapper.checkFullRowValues('[row-id="5"]', rowAllDataEndPoints[5], Cypress.env('projectName'))
     Mapper.checkFullRowValues('[row-id="6"]', rowAllDataEndPoints[6], Cypress.env('projectName'))
     cy.contains(Mapper.columnGroupText, 'Node Details').should('be.visible').dblclick()
     cy.contains(Mapper.columnGroupText, 'Node Details').next('[ref="agOpened"]').should('have.class', 'ag-hidden') */

    Mapper.chooseTab('Flow')
    Mapper.checkRowByTagId('F_ClosedQuestion9873_default', rowDataFlow[0])
    Mapper.checkRowByTagId('F_ClosedQuestion9873_exitone', rowDataFlow[1])
    Mapper.checkRowByTagId('F_ClosedQuestion9873_exittwo', rowDataFlow[2])
    Mapper.checkRowByTagId('F_ClosedQuestion9873_exitthree', rowDataFlow[3])
    Mapper.checkRowByTagId('F_ClosedQuestion9873_exitfour', rowDataFlow[4])
    Mapper.checkRowByTagId('F_ClosedQuestion9873_exitfive', rowDataFlow[5])
  },
)

it(
  'CMTS-9736-to-validate-the-payload-of-existing-node-will-be-changed-after-adding-the-new-node-below',
  { tags: '@smoke' },
  () => {
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    FilterBar.selectPattern('PatternAddNewNode')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram('#subflowNode', 'SubFlow')
    AddNewSubFlowNodePopUp.setLabel('SubFlowTwo')
    AddNewSubFlowNodePopUp.setLink('flow')
    AddNewSubFlowNodePopUp.clickOnAddBtn()
    NavigationBar.openMapperPage() // open page Mapper
    FilterBar.selectPattern('PatternAddNewNode')
    FilterBar.selectFlow('All Flows') // select all flows
    Mapper.chooseTab('Flow')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Flow')
    Mapper.checkCellByTagId('F_SubFlow_flow', '[col-id="payload"]', 'GOTO: SubFlowTwo')
  },
)

it.skip('CMTS-9806-to-validate-the-added-flows-are-displayed-inside-the-flows-tab-on-the-mapper-page', () => {
  NavigationBar.openPatternsPage()
  DiagramUtils.findDiagram()
  FilterBar.selectPattern(dataMapper.subFlowPattern) // choose pattern from created via POST request project
  DiagramUtils.findDiagram()
  DiagramUtils.addNodeInDiagram(PaletteBox.skillRouterNodeIdSelector, 'Start', true) // add skill router to pattern on diagram on page Patterns
  AddNewSkillRouterNodePopUp.setLabel(dataMapper.skillRouterName)
  AddNewSkillRouterNodePopUp.clickOnAddBtn()
  NavigationBar.openMapperPage()
  FilterBar.selectFlow('All Flows') // select all flows
  Mapper.chooseTab('All')
  Mapper.checkHeadersValues() // check headers
  // check table data - Skill Router
  Mapper.checkRowValues(
    '[row-id="0"]',
    'S_' + dataMapper.subFlowPattern + '_StartBot',
    'false',
    'Start',
    'BOT START: [object Object] | GOTO: ' + dataMapper.skillRouterName,
    'Start Flow: ' + dataMapper.subFlowPattern + Cypress.env('projectName') + '/' + dataMapper.subFlowPattern,
  )
  /*  Mapper.checkRowValues(
    '[row-id="1"]',
    'O_' + dataMapper.skillRouterName + '_default',
    'false',
    dataMapper.skillRouterName,
    'What can I do for you today?',
    '',
  ) */
  Mapper.checkRowValuesObject('[row-id="1"]', skillRouter9806Before[1])
  Mapper.checkRowValuesObject('[row-id="2"]', skillRouter9806Before[2])
  Mapper.checkRowValuesObject('[row-id="3"]', skillRouter9806Before[3])
  Mapper.checkRowValuesObject('[row-id="4"]', skillRouter9806Before[4])
  Mapper.checkRowValuesObject('[row-id="5"]', skillRouter9806Before[5])
  // add new 2 nodes SubFlow
  NavigationBar.openPatternsPage()
  DiagramUtils.findDiagram()
  DiagramUtils.addNodeInDiagram('#subflowNode', dataMapper.skillRouterName)
  AddNewSubFlowNodePopUp.setLabel(dataMapper.subFlow_1Name)
  AddNewSubFlowNodePopUp.setLink('default')
  AddNewSubFlowNodePopUp.clickOnAddBtn()

  DiagramUtils.findDiagram()
  DiagramUtils.addNodeInDiagram('#subflowNode', dataMapper.subFlow_1Name)
  AddNewSubFlowNodePopUp.setLabel(dataMapper.subFlow_2Name)
  AddNewSubFlowNodePopUp.setLink('link')
  AddNewSubFlowNodePopUp.clickOnAddBtn()
  NavigationBar.openMapperPage()
  FilterBar.selectPattern(dataMapper.subFlowPattern)

  FilterBar.selectFlow('All Flows') // check tab All
  Mapper.checkHeadersValues() // check headers
  // check table data - Skill Router
  Mapper.checkRowValues(
    '[row-id="0"]',
    'S_' + dataMapper.subFlowPattern + '_StartBot',
    'false',
    'Start',
    'BOT START: [object Object] | GOTO: ' + dataMapper.skillRouterName,
    'Start Flow: ' + dataMapper.subFlowPattern + Cypress.env('projectName') + '/' + dataMapper.subFlowPattern,
  )
  /*  Mapper.checkRowValues(
    '[row-id="1"]',
    'O_' + dataMapper.skillRouterName + '_default',
    'false',
    dataMapper.skillRouterName,
    'What can I do for you today?',
    '',
  ) */
  Mapper.checkRowValuesObject('[row-id="1"]', skillRouter9806After[1])
  Mapper.checkRowValuesObject('[row-id="2"]', skillRouter9806After[2])
  Mapper.checkRowValuesObject('[row-id="3"]', skillRouter9806After[3])
  Mapper.checkRowValuesObject('[row-id="4"]', skillRouter9806After[4])
  Mapper.checkRowValuesObject('[row-id="5"]', skillRouter9806After[5])
  // check table data - Sub Flow 1
  Mapper.checkRowValues(
    '[row-id="7"]',
    'F_' + dataMapper.subFlow_1Name + '_link',
    'false',
    dataMapper.subFlow_1Name,
    'GOTO: ' + dataMapper.subFlow_2Name,
    '',
  )
  Mapper.checkRowValues(
    '[row-id="8"]',
    'F_' + dataMapper.subFlow_1Name + '_UserAbandonment',
    'false',
    dataMapper.subFlow_1Name,
    'GOTO: ' + 'UserAbandonment',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="9"]',
    'F_' + dataMapper.subFlow_1Name + '_UserAbandonment',
    'false',
    dataMapper.subFlow_1Name,
    'GOTO: UserAbandonment',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="10"]',
    'G_UserAbandonment_' + dataMapper.subFlow_1Name,
    'false',
    'UserAbandonment',
    'Outcome: UserAbandonment type=outcomePoint fromNode=' + dataMapper.subFlow_1Name + ' condition=UserAbandonment',
    'Global User Abandonment',
  )
  Mapper.checkRowValues(
    '[row-id="11"]',
    'F_' + dataMapper.subFlow_1Name + '_Escalate',
    'false',
    dataMapper.subFlow_1Name,
    'GOTO: Escalate',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="12"]',
    'F_' + dataMapper.subFlow_1Name + '_Fallback',
    'false',
    dataMapper.subFlow_1Name,
    'GOTO: Fallback',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="13"]',
    'S_' + dataMapper.subFlow_1Name + '_StartSubFlow',
    'false',
    'Start',
    'START Flow: ' +
      Cypress.env('projectName') +
      '/' +
      dataMapper.subFlowPattern +
      '/' +
      dataMapper.subFlow_1Name +
      ' | ' +
      dataMapper.addPoint,
    'Start Flow: ' +
      dataMapper.subFlowPattern +
      Cypress.env('projectName') +
      '/' +
      dataMapper.subFlowPattern +
      '/' +
      dataMapper.subFlow_1Name,
  )
  // check table data - Sub Flow 2
  Mapper.checkRowValues(
    '[row-id="14"]',
    'F_' + dataMapper.subFlow_2Name + '_default',
    'false',
    dataMapper.subFlow_2Name,
    dataMapper.addPoint,
    '',
  )
  Mapper.checkRowValues(
    '[row-id="15"]',
    'F_' + dataMapper.subFlow_2Name + '_UserAbandonment',
    'false',
    dataMapper.subFlow_2Name,
    'GOTO: UserAbandonment',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="16"]',
    'G_UserAbandonment_' + dataMapper.subFlow_2Name,
    'false',
    'UserAbandonment',
    'Outcome: UserAbandonment type=outcomePoint fromNode=' + dataMapper.subFlow_2Name + ' condition=UserAbandonment',
    'Global User Abandonment',
  )
  Mapper.checkRowValues(
    '[row-id="17"]',
    'F_' + dataMapper.subFlow_2Name + '_Escalate',
    'false',
    dataMapper.subFlow_2Name,
    'GOTO: Escalate',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="18"]',
    'F_' + dataMapper.subFlow_2Name + '_Fallback',
    'false',
    dataMapper.subFlow_2Name,
    'GOTO: Fallback',
    '',
  )
  Mapper.checkRowValues(
    '[row-id="19"]',
    'S_' + dataMapper.subFlow_2Name + '_StartSubFlow',
    'false',
    'Start',
    'START Flow: ' +
      Cypress.env('projectName') +
      '/' +
      dataMapper.subFlowPattern +
      '/' +
      dataMapper.subFlow_2Name +
      ' | ' +
      dataMapper.addPoint,
    'Start Flow: ' +
      dataMapper.subFlowPattern +
      Cypress.env('projectName') +
      '/' +
      dataMapper.subFlowPattern +
      '/' +
      dataMapper.subFlow_2Name,
  )
})

describe('mapper export', { tags: '@smoke' }, () => {
  it('CMTS-9678-to-validate-it-is-possible-to-export-csv-document-from-mapper-page', () => {
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('PatternEndPoint')
    Cypress.env('patternName', 'PatternEndPoint')
    FilterBar.selectFlow('All Flows') // select all flows
    Mapper.chooseTab('Input')
    FilterBar.exportCSV()

    cy.readFile('cypress\\downloads\\export.csv')
      .should('exist')
      .and('contain', 'I_ClosedQuestion9873_OfferParser')
      .and('contain', 'false')
      .and('contain', 'ClosedQuestion9873')
      .and('contain', 'Parser: OfferParser NLUModel')
  })

  it('CMTS-9677-to-validate-it-is-possible-to-export-excel-document-from-mapper-page', () => {
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('PatternEndPoint')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Input')
    FilterBar.exportExcel() // export current table to Excel
    Mapper.checkExportToExcel('cypress/fixtures/data/deploy-mapper/exportXLSXCompare.json')
  })

  it('check export to excel mapping table with expanded node details', () => {
    NavigationBar.openMapperPage()
    FilterBar.selectPattern('PatternEndPoint')
    FilterBar.selectFlow('All Flows')
    Mapper.chooseTab('Input')
    Mapper.expandNodeDetails() // expand node details
    FilterBar.exportExcel() // export current table to Excel
    Mapper.checkExportToExcelNodeDetails('cypress/fixtures/data/deploy-mapper/exportExcelCompareNodeDetails.json')
  })

  it('[CMTS-15396] To validate the toast message is displayed after addingh new valid Vendor Environment Account', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('[CMTS-15397] To validate it is possible to Deploy the flow with new added valid Account', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('CMTS-15398 To validate all the fields are mandatory inside the Configure HeartReach with vendor pop up', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.checkPdeUrlField()
      .checkAccountField()
      .checkTokenField()
      .checkAddBtn()
      .checkCloseBtn()
      .clickAddBtn()
      .checkErrorMessage('Please provide PDE URL')
    ConfigureHeartReachPopUp.checkErrorMessage('Please input a value for Token')
    ConfigureHeartReachPopUp.checkErrorMessage('Please input a value for Account')
  })

  it('[CMTS-15399] Configure HeartReach with vendor pop up: To validate all the highlighted mandatory fields becomes not highligted after entering the valid data', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.checkPdeUrlField()
      .checkAccountField()
      .checkTokenField()
      .checkAddBtn()
      .checkCloseBtn()
      .clickAddBtn()
      .checkColorAccount(`rgb(245, 34, 45)`)
      .checkColorPde(`rgb(245, 34, 45)`)
      .checkColorToken(`rgb(245, 34, 45)`)
      .setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomationone')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .checkColorAccount(`rgb(217, 217, 217)`)
      .checkColorPde(`rgb(217, 217, 217)`)
      .checkColorToken(`rgb(217, 217, 217)`)
  })

  it('[CMTS-15400] Configure HeartReach with vendor pop up: To validate it is not possible to add new Account with already exist account name', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('[CMTS-15402] To validate the toast message is displayed after adding new Account', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.setPdeUrlField('https://discovery.deloitte-corp.api.onereach.ai')
      .setAccountField('TrueServe Templatestautomation')
      .setTokenField(
        'AAABogECAQB45rbZDpSPnVfntrh+EjeuF0mdDw45yrNMoRticvz3tloB5NQnNG7ZjFNITiyI0fxF8QAAAWgwggFkBgkqhkiG9w0BBwagggFVMIIBUQIBADCCAUoGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQM2DNw7DpC8tAYsW0tAgEQgIIBG1DhA4ExDjMYTN1f76OmNHfxDIXvIRepYrLq40iehUyGNqUE3Nz7jMJseuZbdMUqfzKUQpxAmp4SDGZNRkaWy+n/70e8H/lyOkImZBQaD3soX2/GNu1zJt6++t+O44uZLaJQYe24Grx1Z4ObYtDKZ1nuggue0fOQBOSDYP9FsYYSh5YjVdm2sO4bQpnRbsJ//FkUIL87FpOunZF2e6bDfZ41mVSQd5V/t+jiF4BjDzMDZgfiXD58m2GlYLfbPQWaedUJjSwJEXK/f3TNa9PafwN/flQnffbUuD/naNjmObu3FERCoKAp6it+7gk6SRv+AxZM1UR3SOIEAa2N4kpHb+tM2zaqwU/wd2aCHrj/BPItlQeHcH2377wsj00AAAFEZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhZMk52ZFc1MFNXUWlPaUl3Tnpaa00yRTJZUzAwTXpBeExUUTRZV0l0WWpBNE5TMDJPVFpqTnpneFlUQXhNV0lpTENKMWMyVnlTV1FpT2lJM01HTmpObVE0TWkwMk5qTTRMVFJtTmpVdE9EUmxZaTAxTVdWaU1UYzNNMlF5WkRRaUxDSnliMnhsSWpvaVZWTkZVaUlzSW1GMWRHaFViMnRsYmlJNklqYzBaV1JpTm1WbExXVTVOV0V0TkRCbVlpMWhaalkxTFdRMk56WXlNbUppWmpnd1lpSXNJbWxoZENJNk1UWTNNems0TlRJeU5uMC5YbFRJT2M4V0VmQTBwQzloSEsyVkZneDBEajVFb1dJeUVHWWNqNlVJckJz',
      )
      .clickAddBtn()
      .checkMessageInvalidToken('Invalid Token')
  })

  it('[CMTS-15401] Configure HeartReach with vendor pop up: To validate it is possible to close pop up after click on the any screen area', () => {
    LoginPage.open().setUsername('dhb-admin')
    LoginPage.setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnDeploytBtn()
    FilterBar.clickGenerateFlow()
    GenerateFlowsPopUp.clickOnSettingsIconBtn()
    ConfigureHeartReachPopUp.checkAccountField().checkTokenField().checkPdeUrlField().checkAddBtn().checkCloseBtn()
    Mapper.clickOnOutput()
  })
})
