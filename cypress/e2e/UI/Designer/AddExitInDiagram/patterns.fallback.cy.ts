import { dataObject } from '../../../../fixtures/data/data-spec-projects'
import PaletteBox from '../../../../classes/PaletteBox'
import DiagramUtils from '../../../../support/DiagramUtils'
import AddNewGlobalFallbackOutcomePopUp from '../../../../classes/popups/AddNewGlobalFallbackOutcomePopUp'
import NodePropertiesSideBar from '../../../../classes/bars/NodePropertiesSideBar'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'
import LinkNodeSideFallback from '../../../../fixtures/provider/LinkNodeSideFallback'
import LabelWithCharactersFallback from '../../../../fixtures/provider/LabelWithCharactersFallback'
import LabelWithUnderscoreFallback from '../../../../fixtures/provider/LabelWithUnderscoreFallback'
import LabelWithErrorFallback from '../../../../fixtures/provider/LabelWithErrorFallback'
import LabelWithSymbolFallback from '../../../../fixtures/provider/LabelWithSymbolFallback'
import LabelWithReasonFallback from '../../../../fixtures/provider/LabelWithReasonFallback'

beforeEach(function () {
  cy.restoreLocalStorage()
})

afterEach(function () {
  // preserve local storage after each hook
  cy.saveLocalStorage()
})

describe.skip('work with global fallback', { tags: '@exit' }, () => {
  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  LinkNodeSideFallback.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, dataObject.pattern_diagram.subflow1.nodeLabel)
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()

      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      NodePropertiesSideBar.openFlow()
        .isSideNode(input.linkLabel, input.sideText)
        .isSideNodeConditionSel(input.linkLabel, input.conditionText)
    })
  })

  it('[CMTS-13970] Global Fallback outcomes cannot be added with the same link labels', () => {
    cy.precondition('13970')

    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    PaletteBox.expandCollapseExits().click()
    DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'SubFlow1')
    AddNewGlobalFallbackOutcomePopUp.setEndpointLink('Custom').selectLinkType(' Normal (down) ').clickOnAddBtn()

    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow().isSideNode('Custom', 'down')
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'SubFlow1')
    AddNewGlobalFallbackOutcomePopUp.setEndpointLink('Custom')
      .selectLinkType(' Normal (down) ')
      .clickOnAddBtn()
      .checkErrorMessage('Link label is not unique!')
  })

  LabelWithCharactersFallback.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()

      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      NodePropertiesSideBar.openFlow()
        .isSideNode(input.linkLabel, input.sideText)
        .isSideNodeConditionSel(input.linkLabel, input.conditionText)
    })
  })

  LabelWithReasonFallback.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel)
        .selectLinkType(input.linkType)
        .setReason(input.reason)
        .clickOnAddBtn()

      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      NodePropertiesSideBar.openFlow()
        .isSideNode(input.linkLabel, input.sideText)
        .isSideNodeConditionSel(input.linkLabel, input.conditionText)
        .isSideNodeReasonSel(input.reason)
    })
  })

  LabelWithErrorFallback.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel)
        .selectLinkType(input.linkType)
        .clickOnAddBtn()
        .checkErrorMessage(input.error)
    })
  })

  it('[CMTS-13995] Global Fallback outcomes with different link types are attached to one parent node according to the selected link type', () => {
    cy.precondition('13995')

    DiagramUtils.findDiagram()
    DiagramUtils.click('AddNode1')
    PaletteBox.expandCollapseExits().click()
    DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'AddNode1')
    AddNewGlobalFallbackOutcomePopUp.setEndpointLink('down').clickOnAddBtn()
    DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
    NodePropertiesSideBar.openFlow().isSideNode('down', 'down').isSideNodeConditionSel('down', 'Fallback')
  })

  LabelWithUnderscoreFallback.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()
    })
  })

  LabelWithSymbolFallback.forEach((input) => {
    it(input.title, () => {
      cy.precondition(input.preconditionId)

      DiagramUtils.findDiagram()
      PaletteBox.expandCollapseExits().click()
      DiagramUtils.addNodeInDiagram(PaletteBox.globalFallbackSelector, 'AddNode1')
      AddNewGlobalFallbackOutcomePopUp.setEndpointLink(input.linkLabel).selectLinkType(input.linkType).clickOnAddBtn()
    })
  })
})
