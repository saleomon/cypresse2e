import { AddNewSubflowNodeDialog } from '../../../../classes/DialogModal'

import FilterBar from '../../../../classes/bars/FilterBar'
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import PalleteBox from '../../../../classes/PaletteBox'
import NodeValidation from '../../../../classes/validation/NodeLabel'
import DiagramUtils from '../../../../support/DiagramUtils'
import NodeLabel from '../../../../fixtures/data/validation/validationNodeLabel'
import LinkLabel from '../../../../fixtures/data/validation/validationLinkLabel'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'

before(() => {
  cy.fixture('projects-templates/add-node-project.json').as('projects')

  cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
    cy.loginAPI()
    cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
      Cypress.env('projectId', projectInfo.id)
      cy.loginUI()
      NavigationBar.selectProject(projectInfo.label)
    })
    NavigationBar.openPatternsPage()
    FilterBar.selectPattern('general')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.subflowNodeIdSelector, 'nodeLabel')
  })
})

after(() => {
  cy.removeProject(Cypress.env('projectId'))
})

describe('Add Subflow Node', { tags: '@validation' }, () => {
  NodeLabel.forEach((input) => {
    it.skip(input.title, () => {
      NodeValidation.nodeLabel(input)
    })
  })

  LinkLabel.forEach((input) => {
    it.skip(input.title, () => {
      NodeValidation.linkLabel(input)
    })
  })

  it('Empty channels', () => {
    NodeValidation.channelsEmpty()
  })

  it('Type not existing channel', () => {
    NodeValidation.multiselectNotExist('Channels', 'ABC')
  }) 
})
