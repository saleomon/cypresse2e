import FilterBar from '../../../../classes/bars/FilterBar'
import NodeDetailsPanel from '../../../../classes/NodeDetailsPanel' // should be changed on TabFlowSideBar, TabOutputSideBar ...
import NavigationBar from '../../../../classes/navigation/NavigationBar'
import DiagramUtils from '../../../../support/DiagramUtils'
import FlowConditionPopOver from '../../../../classes/popovers/FlowConditionPopOver'
import AddNewOutputCondition from '../../../../classes/popovers/AddNewOutputCondition'
import TabFlowSideBar from '../../../../classes/bars/TabFlowSideBar'
import TabOutputSideBar from '../../../../classes/bars/TabOutputSideBar'
import { ProjectInfo } from '../../../../types/response/ProjectInfo'

import OutputLabelData from '../../../../fixtures/data/validation/outputLabel'

// should add assertion cy.checkToastMesage() for all tests
let createdProjectInfo = {}
let detailsPanel = new NodeDetailsPanel()
const nodeName = 'closedq'

// output condition types
const PRE_BRIDGE = 'preBridge'
const POST_BRIDGE = 'postBridge'
const QUESTION = 'primary' // previous value was question
const REPAIR = 'repair'

beforeEach(function () {
  // restore localstorage
  cy.restoreLocalStorage()
})

describe('Edit closedq node using Details panel', () => {
  before(() => {
    cy.fixture('projects-templates/edit-close-node-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        NavigationBar.openFlowsPage()
        FilterBar.selectPattern('flowNodes')
        DiagramUtils.findDiagram()
      })
    })
  })

  afterEach(() => {
    cy.saveLocalStorage()
    cy.removeProject(Cypress.env('projectId'))
  })

  describe.skip('Properties Tab', () => {
    it('Edit Description', () => {
      let assertionText = 'edited description'
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
      detailsPanel.selectTab('Properties')
      cy.get('[placeholder="Enter Node Description"]')
        .parent()
        .find('.contentEditButton')
        .click()
        .then(() => {
          cy.get('div').contains('Node Description').find('textarea').click().clear().type(assertionText)
          cy.get('div').contains('Node Description').find('.editControls .saveItEditButton').click()
          detailsPanel.nodeDescription().then(($textarea) => {
            expect($textarea.val()).contains(assertionText)
          })
        })
      cy.findNodeForKey(nodeName).then(($node) => {
        expect($node.data.description).equals(assertionText)
      })
    })
  })

  describe('Output Tab', () => {
    before(function () {
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
      detailsPanel.selectTab('Output')
    })

    describe('Output: Input validation (add output condition popover)', { tags: '@validation' }, () => {
      before(() => {
        detailsPanel.addOutputButton().click()
        detailsPanel.addOutputPopOver().should('be.visible')
      })
      after(() => {
        detailsPanel.addOutputButton().click() // to hide popover
      })

      OutputLabelData.forEach((input) => {
        it(input.title, () => {
          AddNewOutputCondition.outputLabelValidation(input)
        })
      })
    })

    it('Output: add/delete preBridge condition (CMTS-9327, CMTS-9353)', () => {
      let label = 'e2e-pre'
      let content = 'e2e-pre-content'
      let description = 'e2e-pre-descr'
      let actualNumber
      // add
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.addOutput(label, content, PRE_BRIDGE, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber + 1)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(PRE_BRIDGE)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .should('not.exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber - 1)
        })
    })

    it('Output: add/delete postBridge condition (CMTS-9328, CMTS-9354)', () => {
      let label = 'e2e-post'
      let content = 'e2e-post-content'
      let description = 'e2e-post-descr'
      let actualNumber
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.addOutput(label, content, POST_BRIDGE, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber + 1)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(POST_BRIDGE)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab').find('div').contains('POST').next().find('.outputList').contains(label).should('not.exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber - 1)
        })
    })

    it('Output: add/delete question condition (CMTS-9326, CMTS-9352)', () => {
      let label = 'e2e-question'
      let content = 'e2e-quiestion-content'
      let description = 'e2e-question-descr'
      let actualNumber
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.addOutput(label, content, QUESTION, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber + 1)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .next()
        .find('.outputList')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(QUESTION)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .next()
        .find('.outputList')
        .contains(label)
        .should('not.exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('CLOSED QUESTION')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber - 1)
        })
    })

    it('Output: add/delete repair condition (CMTS-9329, CMTS-9355)', () => {
      let label = 'e2e-repair'
      let content = 'e2e-repair-content'
      let description = 'e2e-repair-descr'
      let actualNumber = 6
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.addOutput(label, content, REPAIR, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(7)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .next()
        .find('.outputList')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(REPAIR)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab').find('div').contains('REPAIR').next().find('.outputList').contains(label).should('not.exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('REPAIR')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber - 1)
        })
    })

    describe('Output: EDIT cases', () => {
      it('Output: Edit Condition Type, label and description of a condition output (CMTS-9346, CMTS-9347, CMTS-9348, CMTS-9349)', () => {
        let targetLabel = 'condition-question'
        let content = 'c'
        let description = 'd'
        let actualNumber
        detailsPanel.addOutputButton().click() // to hide popover
        cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
        detailsPanel.addOutput(targetLabel, content, QUESTION, description)
        // assertions
        cy.wait('@addOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })

        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            actualNumber = Number($number)
          })
        cy.intercept('PUT', 'api/patterns/*/*').as('editOutput')
        detailsPanel.editOutput(targetLabel, 'condition-pre', PRE_BRIDGE, description)
        cy.wait('@editOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            expect(Number($number)).equals(actualNumber + 1)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .should('exist')
          .scrollIntoView()
          .should('be.visible')
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .parent()
          .find('.itemConditionType')
          .contains(PRE_BRIDGE)
          .should('exist')
          .should('be.visible')
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .parent()
          .find('.itemDescription')
          .contains(description)
          .should('exist')
          .should('be.visible')
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .parent()
          .next()
          .find('.contentView')
          .contains(content)
          .should('exist')
          .should('be.visible')

        cy.get('#OutputTab')
          .find('div')
          .contains('POST')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            actualNumber = Number($number)
          })
        detailsPanel.editOutput('condition-pre', 'condition-post', POST_BRIDGE, description)
        cy.get('#OutputTab')
          .find('div')
          .contains('POST')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            expect(Number($number)).equals(actualNumber + 1)
          })

        cy.get('#OutputTab')
          .find('div')
          .contains('REPAIR')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            actualNumber = Number($number)
          })
        detailsPanel.editOutput('condition-post', 'condition-repair', REPAIR, description)
        cy.get('#OutputTab')
          .find('div')
          .contains('REPAIR')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            expect(Number($number)).equals(actualNumber + 1)
          })
      })

      it('Output: Edit Content(prompt) text of an output condition item (CMTS-9351)', () => {
        let targetLabel = 'content-editable'
        let content = 'content prompt'
        let description = 'd'

        cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
        detailsPanel.addOutput(targetLabel, content, QUESTION, description)
        // assertions
        cy.wait('@addOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('CLOSED QUESTION')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .parent()
          .find('.contentFooter')
          .find('.contentEditButton')
          .scrollIntoView()
          .click()
        cy.get('#OutputTab')
          .find('div')
          .contains('CLOSED QUESTION')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .parent()
          .find('.contentEdit')
          .find('textarea')
          .clear()
          .type('Edited prompt')
        cy.intercept('PUT', 'api/patterns/*/*').as('editOutput')
        cy.get('#OutputTab')
          .find('div')
          .contains('CLOSED QUESTION')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .parent()
          .find('.contentEdit')
          .find('.editControls')
          .find('.saveItEditButton')
          .click()
        cy.wait('@editOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('CLOSED QUESTION')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .next()
          .find('.contentView')
          .contains('Edited prompt')
          .should('exist')
          .scrollIntoView()
          .should('be.visible')
      })
    })
  })

  describe('Flows Tab', () => {
    let flowName = 'named'
    let flowNewName = 'renamed'
    let flowType = 'Normal'
    let flowDescription = 'flow descr'
    let flowNewDescription = 'new flow descr'
    before(() => {
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
    })
    it('Flow - Add - (positive)', { tags: '@smoke' }, () => {
      TabFlowSideBar.selectTabByName('Flow')
      cy.intercept('PUT', 'api/patterns/*/*').as('addFlow')
      detailsPanel.addFlow(flowName, flowType, flowDescription)
      cy.wait('@addFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('.itemHeader').should('contain', flowName)
      // cy.get('.itemConditionType').should('contain', flowType) //disabled due to CMTS-9178
      cy.get('.descriptionSpan').should('contain', flowDescription)
      // assertions on diagram object
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(5)
        expect($links[3].text).equals(flowName)
      })
    })

    it('Flow - Edit - CMTS-9236', { tags: '@smoke' }, () => {
      TabFlowSideBar.selectTabByName('Flow')
      cy.get('.itemHeader').contains('flowToEdit').find('button').click()
      TabFlowSideBar.clickMenuItem('Edit Properties')
      cy.intercept('PUT', 'api/patterns/*/*').as('editFlow')
      FlowConditionPopOver.fillEditFlowCondition(flowNewName, flowNewDescription)
      FlowConditionPopOver.clickOnSaveBtn()
      cy.wait('@editFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })

      // aseertion on front
      cy.contains('.itemHeader', flowNewName).should('exist')
      cy.contains('.descriptionSpan', flowNewDescription).should('exist')
      // assertions on diagram object
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(5)
        expect($links[3].text).equals(flowNewName)
      })
    })

    it('Flow - Delete created flow - (CMTS-9243)', { tags: '@smoke' }, () => {
      TabFlowSideBar.selectTabByName('Flow')
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('.itemHeader').contains('flowToDelete').find('button').click()
      TabFlowSideBar.clickMenuItem('Delete Flow')
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('not.contain', 'flowToDelete')
      // assertions on diagram
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(4)
        // expect($links[0].text).equals('default')
      })
    })

    it('Flow - Delete REPAIR flow', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      TabFlowSideBar.selectTabByName('Flow')
      cy.get('#flowTab').find('.itemHeader').contains('Repair').find('button').should('not.exist')
     })

    it('Flow - Delete default flow (CMTS-9244)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      TabFlowSideBar.selectTabByName('Flow')
      cy.get('#flowTab').find('.itemHeader').contains('default').find('button').click()
      TabFlowSideBar.clickMenuItem('Delete Flow')
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('#flowTab').find('.itemHeader').should('have.length', 3)
      // assertions on diagram
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(3)
      })
    })

    it(
      'Flow - Input validation (CMTS-9227, CMTS-9228, CMTS-9229, CMTS-9230, CMTS-9231, CMTS-9232, CMTS-9233)',
      { tags: '@validation' },
      () => {
        detailsPanel.selectTab('Flow')
        detailsPanel.addFlowButton().click() // to show popover

        detailsPanel.flowNameField().type('a') // one characher
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('morethan32morethan32morethan32mor') // 33 charactes
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('space in it') // space inside label
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('  ') // two spaces only
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('underscore_label') // underscore in label
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.flowValidationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('aa') // two charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('32chars32chars32chars32chars32ch') // 32 charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        cy.get('.ant-popover').contains('Add New Flow Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.flowNameField().clear()
        detailsPanel.addFlowButton().click() // to hide popover
      },
    )

    it('Flow - Reset validation (CMTS-9237)', { tags: '@reset' }, () => {
      TabFlowSideBar.selectTabByName('Flow')
      FlowConditionPopOver.openPopOver()
      FlowConditionPopOver.fillAddFlowCondition('reset', 'Normal', 'reset')
      FlowConditionPopOver.clickOnResetBtn()
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('input').should('contain', '')
      cy.get('.ant-popover')
        .contains('Add New Flow Condition')
        .next()
        .find('[role="combobox"]')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li[aria-selected="true"]')
                .should('have.text', ' Normal (down) ')
            })
        })
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('textarea').should('contain', '')
    })
  })
})
