import FilterBar from '../../../../classes/bars/FilterBar.ts'
import NodeDetailsPanel from '../../../../classes/NodeDetailsPanel'
import NavigationBar from '../../../../classes/navigation/NavigationBar.ts'
import DiagramUtils from '../../../../support/DiagramUtils.ts'

let createdProjectInfo = {}
let detailsPanel = new NodeDetailsPanel()
let nodeName = 'prompt'
// output condition types
const PRE_BRIDGE = 'preBridge'
const POST_BRIDGE = 'postBridge'
const PROMPT = 'primary' // prev value was prompt

before(function () {
  cy.fixture('projects-templates/edit-node-project.json').then(($json) => {
    cy.createProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), $json).then(
      ($projectInfo) => {
        cy.UILogin(Cypress.env('username'), Cypress.env('password'))
        NavigationBar.selectProject($projectInfo.label)
        cy.get('div').contains($projectInfo.label).should('be.visible')
        NavigationBar.openFlowsPage()
        FilterBar.selectPattern('flowNodes')
        createdProjectInfo = $projectInfo
      },
    )
  })
})

beforeEach(function () {
  // restore localstorage
  cy.restoreLocalStorage()
})

describe('Edit prompt node using Details panel', () => {
  describe.skip('Properties Tab', () => {
    it('Edit Description', () => {
      let assertionText = 'edited description'
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
      detailsPanel.selectTab('Properties')
      cy.get('[placeholder="Enter Node Description"]')
        .parent()
        .find('.contentEditButton')
        .click()
        .then(() => {
          cy.get('div').contains('Node Description').find('textarea').click().clear().type(assertionText)
          cy.get('div').contains('Node Description').find('.editControls .saveItEditButton').click()
          detailsPanel.nodeDescription().then(($textarea) => {
            expect($textarea.val()).contains(assertionText)
          })
        })
      cy.findNodeForKey(nodeName).then(($node) => {
        expect($node.data.description).equals(assertionText)
      })
    })
  })

  describe.skip('Output Tab', () => {
    before(function () {
      cy.waitForDiagram('goDiagram')
      cy.clickNode(nodeName)
      detailsPanel.selectTab('Output')
    })
    describe('Output: Input validation (add output condition popover)', { tags: '@validation' }, () => {
      before(function () {
        cy.waitForDiagram('goDiagram')
        cy.clickNode(nodeName)
        detailsPanel.selectTab('Output')
        detailsPanel.addOutputButton().click()
        cy.get('.ant-popover').contains('Add New Output Condition').should('be.visible')
      })
      it('1 char - CMTS-9332', () => {
        detailsPanel.outputNameField().type('a')
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover')
          .contains('Add New Output Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Length should be 2 to 32 chars')
        detailsPanel.outputNameField().clear()
      })

      it('33 characters - CMTS-9333', () => {
        detailsPanel.outputNameField().type('morethan32morethan32morethan32mor') // 33 charactes
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover')
          .contains('Add New Output Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Length should be 2 to 32 chars')
        detailsPanel.outputNameField().clear()
      })

      it('space in label - CMTS-9334', () => {
        detailsPanel.outputNameField().type('space in it') // space inside label
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover')
          .contains('Add New Output Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.outputNameField().clear()
      })

      it('undersore_in_label - CMTS-9335', () => {
        detailsPanel.outputNameField().type('underscore_label')
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover')
          .contains('Add New Output Condition')
          .next()
          .find('.ant-form-explain')
          .should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.outputNameField().clear()
      })

      it('2 chars - CMTS-9336', () => {
        detailsPanel.outputNameField().type('aa')
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover').contains('Add New Output Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.outputNameField().clear()
      })

      it('32 chars - CMTS-9337', () => {
        detailsPanel.outputNameField().type('32chars32chars32chars32chars32ch')
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover').contains('Add New Output Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.outputNameField().clear()
      })

      it('special symbols - CMTS-9338', () => {
        detailsPanel.outputNameField().type('label"with&special//chars')
        detailsPanel.outputNameField().blur()
        cy.get('.ant-popover').contains('Add New Output Condition').next().find('.ant-form-explain').should('not.exist')
        detailsPanel.outputNameField().clear()
      })

      after(function () {
        detailsPanel.addOutputButton().click() // to hide popover
      })
    })

    it('Output: add/delete preBridge condition (CMTS-9327, CMTS-9353)', () => {
      let label = 'e2e-pre'
      let content = 'e2e-pre-content'
      let description = 'e2e-pre-descr'
      // add
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      detailsPanel.addOutput(label, content, PRE_BRIDGE, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .find('.extraHeaderElements')
        .find('div')
        .contains('1')
        .should('exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(PRE_BRIDGE)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .next()
        .find('.outputListItem')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('PRE')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(1)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab').find('div').contains('PRE').should('not.exist')
    })

    it('Output: add/delete postBridge condition (CMTS-9328, CMTS-9354)', () => {
      let label = 'e2e-post'
      let content = 'e2e-post-content'
      let description = 'e2e-post-descr'
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      detailsPanel.addOutput(label, content, POST_BRIDGE, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .find('.extraHeaderElements')
        .find('div')
        .contains('1')
        .should('exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(POST_BRIDGE)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('POST')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(1)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab').find('div').contains('POST').should('not.exist')
    })

    it('Output: add/delete prompt condition (CMTS-9326, CMTS-9352)', () => {
      let label = 'e2e-prompt'
      let content = 'e2e-prompt-content'
      let description = 'e2e-prompt-descr'
      let actualNumber
      cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.addOutput(label, content, PROMPT, description)
      // assertions
      cy.wait('@addOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber + 1)
        })
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .next()
        .find('.outputList')
        .contains(label)
        .should('exist')
        .scrollIntoView()
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemConditionType')
        .contains(PROMPT)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .find('.itemDescription')
        .contains(description)
        .should('exist')
        .should('be.visible')
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .next()
        .find('.outputList')
        .contains(label)
        .parent()
        .next()
        .find('.contentView')
        .contains(content)
        .should('exist')
        .should('be.visible')

      // delete
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteOutput')
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          actualNumber = Number($number)
        })
      detailsPanel.deleteOutput(label)
      cy.wait('@deleteOutput') // request should be sent
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('#OutputTab').find('div').contains('PROMPT').next().find('.outputList').contains(label).should('not.exist')
      cy.get('#OutputTab')
        .find('div')
        .contains('PROMPT')
        .find('.extraHeaderElements')
        .find('div')
        .invoke('text')
        .then(($number) => {
          expect(Number($number)).equals(actualNumber - 1)
        })
    })

    describe('Output: EDIT cases', () => {
      before(function () {
        cy.waitForDiagram('goDiagram')
        cy.clickNode(nodeName)
        detailsPanel.selectTab('Output')
      })

      it('Output: Edit Condition Type, label and description of a condition output (CMTS-9346, CMTS-9347, CMTS-9348, CMTS-9349)', () => {
        let targetLabel = 'condition-prompt'
        let content = 'c'
        let description = 'd'
        let actualNumber

        cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
        detailsPanel.addOutput(targetLabel, content, PROMPT, description)
        // assertions
        cy.wait('@addOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })

        cy.intercept('PUT', 'api/patterns/*/*').as('editOutput')
        detailsPanel.editOutput(targetLabel, 'condition-pre', PRE_BRIDGE, description)
        cy.wait('@editOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            expect(Number($number)).equals(1)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .should('exist')
          .scrollIntoView()
          .should('be.visible')
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .parent()
          .find('.itemConditionType')
          .contains(PRE_BRIDGE)
          .should('exist')
          .should('be.visible')
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .parent()
          .find('.itemDescription')
          .contains(description)
          .should('exist')
          .should('be.visible')
        cy.get('#OutputTab')
          .find('div')
          .contains('PRE')
          .next()
          .find('.outputListItem')
          .contains('condition-pre')
          .parent()
          .next()
          .find('.contentView')
          .contains(content)
          .should('exist')
          .should('be.visible')

        detailsPanel.editOutput('condition-pre', 'condition-post', POST_BRIDGE, description)
        cy.get('#OutputTab')
          .find('div')
          .contains('POST')
          .find('.extraHeaderElements')
          .find('div')
          .invoke('text')
          .then(($number) => {
            expect(Number($number)).equals(1)
          })
      })

      it('Output: Edit Content(prompt) text of an output condition item (CMTS-9351)', () => {
        let targetLabel = 'content-editable'
        let content = 'content prompt'
        let description = 'd'

        cy.intercept('PUT', 'api/patterns/*/*').as('addOutput')
        detailsPanel.addOutput(targetLabel, content, PROMPT, description)
        // assertions
        cy.wait('@addOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('PROMPT')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .parent()
          .find('.contentFooter')
          .find('.contentEditButton')
          .scrollIntoView()
          .click()
        cy.get('#OutputTab')
          .find('div')
          .contains('PROMPT')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .parent()
          .find('.contentEdit')
          .find('textarea')
          .clear()
          .type('Edited prompt')
        cy.intercept('PUT', 'api/patterns/*/*').as('editOutput')
        cy.get('#OutputTab')
          .find('div')
          .contains('PROMPT')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .parent()
          .find('.contentEdit')
          .find('.editControls')
          .find('.saveItEditButton')
          .click()
        cy.wait('@editOutput') // request should be sent
          .its('response')
          .then((response) => {
            expect(response).to.have.property('statusCode', 201)
          })
        cy.get('#OutputTab')
          .find('div')
          .contains('PROMPT')
          .next()
          .find('.outputList')
          .contains(targetLabel)
          .parent()
          .next()
          .find('.contentView')
          .contains('Edited prompt')
          .should('exist')
          .scrollIntoView()
          .should('be.visible')
      })
    })
  })

  describe.skip('Flows Tab', () => {
    let flowName = 'named'
    let flowNewName = 'renamed'
    let flowType = 'Normal'
    let flowDescription = 'flow descr'
    let flowNewDescription = 'new flow descr'

    it('Flow - Add - (positive)', () => {
      DiagramUtils.findDiagram()
      DiagramUtils.click(nodeName)
      detailsPanel.selectTab('Flow')
      cy.intercept('PUT', 'api/patterns/*/*').as('addFlow')
      detailsPanel.addFlow(flowName, flowType, flowDescription)
      cy.wait('@addFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      cy.get('.itemHeader').should('contain', flowName)
      // cy.get('.itemConditionType').should('contain', flowType) //disabled due to CMTS-9178
      cy.get('.descriptionSpan').should('contain', flowDescription)
      // assertions on diagram object
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowName)
      })
    })

    it('Flow - Edit - CMTS-9236', () => {
      detailsPanel.selectTab('Flow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.wait(500)
      cy.get('li').contains('Edit Properties').should('exist').should('be.visible').click()
      cy.intercept('PUT', 'api/patterns/*/*').as('editFlow')
      cy.get('.ant-popover').contains('Edit Existing Flow Condition').should('be.visible')
      cy.get('.ant-popover-placement-left').find('input').clear().type(flowNewName)

      cy.get('.ant-popover-placement-left').find('textarea').clear().type(flowNewDescription)
      cy.get('.ant-popover-placement-left').find('.ant-btn-primary').click()
      cy.wait('@editFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })

      // aseertion on front
      cy.get('.itemHeader').should('contain', flowNewName)
      cy.get('.itemHeader').should('contain', flowNewDescription)
      // assertions on diagram object
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(2)
        expect($links[1].text).equals(flowNewName)
      })
    })

    it('Flow - Delete created flow - (CMTS-9243)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('.itemHeader').contains(flowName).find('button').click()
      cy.wait(500)
      cy.get('li').contains('Delete Flow').should('exist').should('be.visible').click()
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('not.contain', flowNewName)
      cy.get('.itemHeader').should('not.contain', flowNewDescription)
      // assertions on diagram
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(1)
        expect($links[0].text).equals('default')
      })
    })

    it('Flow - Delete default flow (CMTS-9244)', () => {
      cy.intercept('PUT', 'api/patterns/*/*').as('deleteFlow')
      cy.get('#flowTab').find('.itemHeader').find('button').click()
      cy.wait(500)
      cy.get('.ant-dropdown-menu-item').contains('Delete Flow').should('exist').should('be.visible').click()
      cy.wait('@deleteFlow')
        .its('response')
        .then((response) => {
          expect(response).to.have.property('statusCode', 201)
        })
      // assertions on front
      cy.get('.itemHeader').should('have.length', 0)
      // assertions on diagram
      cy.findLinksOutOf(nodeName).then(($links) => {
        expect($links.length).equals(0)
      })
    })

    it('Flow - Input validation (CMTS-9227, CMTS-9228, CMTS-9229, CMTS-9230, CMTS-9231, CMTS-9232, CMTS-9233)',
      { tags: '@validation' },
      () => {
        detailsPanel.selectTab('Flow')
        detailsPanel.addFlowButton().click() // to show popover

        detailsPanel.flowNameField().type('a') // one characher
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('morethan32morethan32morethan32mor') // 33 charactes
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('contain', 'Length be 2 to 32 chars')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('space in it') // space inside label
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('  ') // two spaces only
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('underscore_label') // underscore in label
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('contain', 'Label must not contain spaces or underscores.')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('aa') // two charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('not.exist')
        detailsPanel.flowNameField().clear()

        detailsPanel.flowNameField().type('32chars32chars32chars32chars32ch') // 32 charachers
        detailsPanel.flowNameField().blur() // blur triggers validation
        detailsPanel.validationMessage().should('not.exist')
        detailsPanel.flowNameField().clear()
        detailsPanel.addFlowButton().click() // to hide popover
      },
    )

    it('Flow - Reset validation (CMTS-9237)', () => {
      detailsPanel.selectTab('Flow')
      detailsPanel.fillFlowForm('reset', 'Normal', 'reset')
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('span').contains('Reset').click()
      detailsPanel.flowNameField().should('contain', '')
      cy.get('.ant-popover')
        .contains('Add New Flow Condition')
        .next()
        .find('[role="combobox"]')
        .then(($combobox) => {
          cy.wrap($combobox)
            .attribute('aria-controls')
            .then(($id) => {
              cy.get('#' + $id)
                .find('ul>li')
                .not('[aria-selected="true"]')
                .should('have.length', 7)
            })
        })
      cy.get('.ant-popover').contains('Add New Flow Condition').next().find('textarea').should('contain', '')
    })
  })
})

afterEach(function () {
  // preserve local storage after each hook
  cy.saveLocalStorage()
})

after(function () {
  cy.deleteProject(Cypress.env('baseUrlAPI'), Cypress.env('username'), Cypress.env('password'), createdProjectInfo.id)
  cy.window().then(($win) => {
    $win.localStorage.clear()
  })
})
