import { dataObject } from '../../../fixtures/data/data-spec-projects'
import DialogModal from '../../../classes/popups/DialogPopUp'
import PaletteBox from '../../../classes/PaletteBox'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'
import NodePropertiesSideBar from '../../../classes/bars/NodePropertiesSideBar'
import AddNewPromptNodePopUp from '../../../classes/popups/AddNewPromptNodePopUp'
import AddNewClosedQuestionNodePopUp from '../../../classes/popups/AddNewClosedQuestionNodePopUp'
import AddNewOpenQuestionNodePopUp from '../../../classes/popups/AddNewOpenQuestionNodePopUp'
import AddNewGoLinkNodePopUp from '../../../classes/popups/AddNewGoLinkNodePopUp'
import TabFlowSideBar from '../../../classes/bars/TabFlowSideBar'
import TabPropertiesSideBar from '../../../classes/bars/TabPropertiesSideBar'
import { DesignerTopBarHrefs } from '../../../fixtures/navigation-data/designer-top-bar-hrefs'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import FilterBar from '../../../classes/bars/FilterBar'
import NodeDetailsPanel from '../../../classes/NodeDetailsPanel'

describe('node operations', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })
  
  describe('reset fields', () => {
    it('[CMTS-10454] All input data inside Add New Node pop-up are removed after Clicking Reset button (prompt node)', () => {
      cy.precondition('10454')

      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })
      NodePropertiesSideBar.openChannels().isChannelChosen('IVR ')
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)

      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.promptNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.flow_diagram.prompt1)
      DialogModal.clickOnResetBtn()
      AddNewPromptNodePopUp.clickOnResetBtn()
        .isLabelSet('')
        .isNodeLabelErrorMessageNotDisplayed()
        .isDescriptionSet('')
        .isLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
        .isTemplateSelected('DefaultPromptTemplate')
        .isDefaultPromptSel('')
      DialogModal.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode1')
      })
    })

    it('[CMTS-12913] To validate the reset button removes the data on the Closed question node Add New Node pop up', () => {
      cy.precondition('12913')

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.closedQNodeIdSelector, 'Start', true)
      AddNewClosedQuestionNodePopUp.setLabel('ClosedQuestion1')
        .setDescription('This is first ClosedQuestion description')
        .setDefaultClosedQuestion('This is test question')
        .setLink('closedquestion1')
        .setLinkDescription('This is first ClosedQuestion link description')
      AddNewClosedQuestionNodePopUp.clickOnResetBtn()
        .isLabelSet('')
        .isNodeLabelErrorMessageNotDisplayed()
        .isDescriptionSet('')
        .isTemplateSelected('DefaultClosedTemplate')
        .isDefaultClosedQuestionSet('')
        .isLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
      DialogModal.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode1')
      })
    })

    it('[CMTS-9655] To validate the reset button removes the data on the Open question node Add New Node pop up', () => {
      cy.precondition('9655')

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)

      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.openQNodeIdSelector, 'Start', true)
      cy.fillTagPopUp(dataObject.flow_diagram.openq1)
      AddNewOpenQuestionNodePopUp.clickOnResetBtn()
        .isLabelSet('')
        .isNodeLabelErrorMessageNotDisplayed()
        .isDescriptionSet('')
        .isTemplateSelected('DefaultOpenTemplate')
        .isDefaultOpenQuestionSet('')
        .isLinkSet('default')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
      DialogModal.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode1')
      })
    })

    it('[CMTS-10690] All input data in Add Goto Link pop-up are removed after clicking Reset button', () => {
      cy.precondition('10690')

      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()
      DiagramUtils.addNodeInDiagram(PaletteBox.goToNodeIdSelector, dataObject.flow_diagram.openq2.nodeLabel)
      AddNewGoLinkNodePopUp.setLink('gotoLink')
        .setLinkDescription('This is GoToNode description.')
        .selectLinkType('Escalate to HitL (right)')
        .selectLinkToNode('OpenQuestion1')
      AddNewGoLinkNodePopUp.clickOnResetBtn()
        .isLinkSet('')
        .isLinkLabelErrorMessageNotDisplayed()
        .isLinkDescriptionSet('')
        .isLinkTypeSet('Normal (down)')
        .isLinkToNodeSet('')
        .isLinkToNodeErrorMessageNotDisplayed()
      AddNewGoLinkNodePopUp.clickOnCloseBtn()
      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq2.nodeLabel, 'default').then(($el) => {
        expect($el.data.key).equals('AddNode2')
      })
    })
  })

  describe('duplicate node', () => {
    it('[CMTS-11978] It is not possible to duplicate parent node with its child', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
      TabPropertiesSideBar.isNodeLabelSet('Tag1')
        .isNodeLabelSet('Tag1')
        // .isNodeLabelEditBtnDisplayed()
        .clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- Tag1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- Tag1')
        .isToastMessageDisplayed('Duplicate Node "Copy1- Tag1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
    })

    it('[CMTS-11979] Duplicated node has red frame', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()

      DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
      TabPropertiesSideBar.isTitleSet('Tag1')
        .isNodeLabelSet('Tag1')
        .isNodeLabelEditBtnDisplayed()
        .clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- Tag1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- Tag1')
        .isToastMessageDisplayed('Duplicate Node "Copy1- Tag1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
    })

    it('[CMTS-11980] Duplicated node has the same flows as original one', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
      TabPropertiesSideBar.openProperties()
        .isTitleSet('ClosedQuestion2')
        .isNodeLabelSet(dataObject.flow_diagram.closedq2.nodeLabel)

      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion3').then(
        ($el) => {
          expect($el.data.key).equals('ClosedQuestion3')
        },
      )
      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion4').then(
        ($el) => {
          expect($el.data.key).equals('ClosedQuestion4')
        },
      )
      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'Repair').then(($el) => {
        expect($el.data.key).equals(dataObject.flow_diagram.closedq2.nodeLabel)
      })

      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- ClosedQuestion2')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
        .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion3').then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion4').then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'Repair').then(($el) => {
        expect($el.data.key).equals('Copy1- ClosedQuestion2')
      })
    })

    it('[CMTS-11982] Copy - prefixes count the number of duplicates', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
      TabPropertiesSideBar.isTitleSet('ClosedQuestion2')
        .isNodeLabelSet('ClosedQuestion2')
        .clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- ClosedQuestion2')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')
      DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy2- ClosedQuestion2')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('Label = Copy2- ClosedQuestion2')
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isToastMessageDisplayed('Duplicate Node "Copy2- ClosedQuestion2" has an invalid label and must be changed')
    })

    it('[CMTS-14485] Duplicated nodes line can be scrolled', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)

      DiagramUtils.findDiagram()
      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel)
        .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
        .isNodeLabelEditBtnDisplayed()

      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- Prompt1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy2- Prompt1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy2- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy2- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy2- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode7')
      })
      DiagramUtils.findChildNodeByLinkText('Copy2- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode8')
      })

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick('Copy3- Prompt1') // need to scroll to click on this node
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy3- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy3- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy3- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode9')
      })
      DiagramUtils.findChildNodeByLinkText('Copy3- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode10')
      })

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick('Copy4- Prompt1') // need to scroll to click on this node
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy4- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy4- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy4- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode11')
      })
      DiagramUtils.findChildNodeByLinkText('Copy4- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode12')
      })

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick('Copy5- Prompt1') // need to scroll to click on this node
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy5- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy5- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy5- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode13')
      })
      DiagramUtils.findChildNodeByLinkText('Copy5- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode14')
      })

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick('Copy6- Prompt1') // need to scroll to click on this node
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy6- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy6- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy6- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode15')
      })
      DiagramUtils.findChildNodeByLinkText('Copy6- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode16')
      })

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick('Copy7- Prompt1') // need to scroll to click on this node
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy7- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy7- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy7- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode17')
      })
      DiagramUtils.findChildNodeByLinkText('Copy7- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode18')
      })

      TabPropertiesSideBar.clickOnCloseBtn()
    })

    it('[CMTS-14486] Duplicated node can be renamed', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel)
        .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
        .isNodeLabelEditBtnDisplayed()
        .clickOnDuplicateBtn({ force: true })

      DiagramUtils.click('Copy1- Prompt1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })

      TabPropertiesSideBar.setNodeLabel('editedcopy').isNodeLabelSet('editedcopy')
    })

    it('[CMTS-14487] The description can be added to the Duplicated node', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel)
        .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
        .isNodeLabelEditBtnDisplayed()
        .clickOnDuplicateBtn({ force: true })

      DiagramUtils.click('Copy1- Prompt1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')

      DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', 'default').then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })

      TabPropertiesSideBar.setNodeLabel('editedcopy')
        .isNodeLabelSet('editedcopy')
        .setDescription('this is description for duplicated node')
        .isDescriptionSet('this is description for duplicated node')
    })

    it('[CMTS-14488] Duplicated node is not attached to the flow (nodes tree)', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()

      DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
      TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel).isNodeLabelSet(
        dataObject.flow_diagram.prompt1.nodeLabel,
      )
      TabFlowSideBar.openFlow()

      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'default').then(($el) => {
        expect($el.data.key).equals('endpointNode2')
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.flow_diagram.prompt1.nodeLabel,
        dataObject.flow_diagram.tag2.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.flow_diagram.tag2.nodeLabel)
      })

      TabPropertiesSideBar.openProperties().clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- Prompt1')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
        .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')
    })

    it('[CMTS-14489] Duplicated node flows have Addpoints in copied flows', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()

      DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
      TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.closedq2.nodeLabel).isNodeLabelSet(
        dataObject.flow_diagram.closedq2.nodeLabel,
      )
      TabFlowSideBar.openFlow()

      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'Repair').then(($el) => {
        expect($el.data.key).equals(dataObject.flow_diagram.closedq2.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion3').then(
        ($el) => {
          expect($el.data.key).equals('ClosedQuestion3')
        },
      )
      DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion4').then(
        ($el) => {
          expect($el.data.key).equals('ClosedQuestion4')
        },
      )

      TabPropertiesSideBar.openProperties().clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- ClosedQuestion2')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
        .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')
      TabFlowSideBar.openFlow()

      DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'Repair').then(($el) => {
        expect($el.data.key).equals('Copy1- ClosedQuestion2')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion3').then(($el) => {
        expect($el.data.key).equals('AddNode5')
      })
      DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion4').then(($el) => {
        expect($el.data.key).equals('AddNode6')
      })
    })

    it('[CMTS-14490] Duplicated node flow diagram displayed works the same as for original node', () => {
      cy.precondition('duplicated-node', 'tag-closedquestion-prompt')
      DiagramUtils.findDiagram()

      DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
      DiagramUtils.findDiagram()

      DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
      TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.closedq2.nodeLabel)
        .isNodeLabelSet(dataObject.flow_diagram.closedq2.nodeLabel)
        .clickOnDuplicateBtn({ force: true })
      DiagramUtils.click('Copy1- ClosedQuestion2')
      TabPropertiesSideBar.clickOnDatamodelListBtn()
        .isDatamodelHumanParameterSet('isNotConnected = true')
        .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
        .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')
        .isDescriptionBtnFocused()
        .clickOnOutputBtn()
        .clickOnBothBtn()
    })
  })
})

describe('Header on the Flows Tab', () => {
  beforeEach(() => {
    cy.fixture('projects-templates/empty-project.json').as('projects')
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
        cy.restoreLocalStorage()
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[ CMTS-14021 ] On Flows Tab , Pattern and Channels are not displayed when no patterns are added to the project ', () => {
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    cy.log('Validate Pattern dropdown on flows tab when no pattern is aligned')
    FilterBar.checkSelectedPattern('NO PATTERN SELECTED')
    FilterBar.checkSelectedFlow('All Flows')
    cy.log('Validate Pattern dropdown on flows tab when no  is aligned')
    FilterBar.checkSelectedChannel('NO CHANNEL SELECTED')
    DiagramUtils.checkChannelText('No Channel')
    cy.log('Validate text and header color ')
    DiagramUtils.checkBackgroundHeaderColor(200, 0, 0, 0.3)
    cy.log('Validate if the warning message is displayed')
    DiagramUtils.checkAlertMessage('Could not find any selected flow.')
  })
})

describe('Header is displayed when Properties panel is opened', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[ CMTS-14063 ] On Flows tab , channel label is visible when the node properties panel is open', () => {
    let nodeDetails = new NodeDetailsPanel()
    cy.precondition('empty')
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    DiagramUtils.findDiagram()
    cy.log('Validate if the properties panel is open')
    NodePropertiesSideBar.checkPropertiesPanelVisibility('be.visible')
    cy.log('Validate the text and header color')
    DiagramUtils.checkTextHeaderColor('IVR', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(24, 144, 255, 0.3)
    cy.log('Validate the text and header color when the properties panel is closed')
    nodeDetails.close()
    NodePropertiesSideBar.checkPropertiesPanelVisibility('not.be.visible')
    DiagramUtils.checkTextHeaderColor('IVR', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(24, 144, 255, 0.3)
  })
})

describe('Flow without empty data', () => {
  beforeEach(() => {
    cy.fixture('entity-templates/nodeSwitch.json').as('projects')
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
      })
    })
  })

  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[ CMTS-14041 ] Flows tab: channels displayed in the Channel drop-down can be switched', () => {
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    NodePropertiesSideBar.checkPropertiesPanelVisibility('be.visible')
    cy.log('Validate IVR')
    FilterBar.selectChannel('IVR')
    DiagramUtils.checkChannelText('IVR')
    DiagramUtils.checkTextHeaderColor('IVR', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(24, 144, 255, 0.3)
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Prompt1')

    cy.log('Validate RWC')
    FilterBar.selectChannel('RWC')
    DiagramUtils.checkChannelText('RWC')
    DiagramUtils.checkTextHeaderColor('RWC', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(107, 151, 48, 0.3)
    DiagramUtils.findDiagram()
    DiagramUtils.click('Closed1')

    cy.log('Validate SMS')
    FilterBar.selectChannel('SMS')
    DiagramUtils.checkChannelText('SMS')
    DiagramUtils.checkTextHeaderColor('SMS', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(234, 220, 147, 0.4)
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Open1')

    cy.log('Validate App')
    FilterBar.selectChannel('App')
    DiagramUtils.checkChannelText('App')
    DiagramUtils.checkTextHeaderColor('App', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(73, 191, 203, 0.3)
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Tag1')

    cy.log('Validate rSMS')
    FilterBar.selectChannel('rSMS')
    DiagramUtils.checkChannelText('rSMS')
    DiagramUtils.checkTextHeaderColor('rSMS', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(255, 191, 203, 0.3)
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Prompt2')

    cy.log('Validate VoIP')
    FilterBar.selectChannel('VoIP')
    DiagramUtils.checkChannelText('VoIP')
    DiagramUtils.checkTextHeaderColor('VoIP', 255, 255, 255, 0.8)
    DiagramUtils.checkBackgroundHeaderColor(235, 191, 255, 0.3)
    DiagramUtils.findDiagram()
    DiagramUtils.findNodeByKey('Closed2')
  })
})
