import { dataObject } from '../../../fixtures/data/data-spec-projects'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import FilterBar from '../../../classes/bars/FilterBar'

describe('nodes tree changes when switching', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it(
    '[CMTS-15574] - Nodes tree changes when switching between patterns on the Patterns tab',
    { tags: '@smoke' },
    () => {
      cy.precondition('node-tree', 'GlobalPattern')

      FilterBar.checkSelectedPattern('GlobalPattern')

      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.skillsrouter1.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.skillsrouter1.nodeLabel,
        dataObject.pattern_diagram.subflow2.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'custom').then(($el) => {
        expect($el.data.key).equals('outcomeNode1')
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'contained').then(($el) => {
        expect($el.data.key).equals('outcomeNode2')
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'abandon').then(($el) => {
        expect($el.data.key).equals('outcomeNode3')
      })

      FilterBar.selectPattern('OutboundPattern').checkSelectedPattern('OutboundPattern')
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.tag1.linkLabel).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.tag1.nodeLabel,
        dataObject.pattern_diagram.subflow1.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.subflow2.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'contained').then(($el) => {
        expect($el.data.key).equals('outcomeNode1')
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'negative').then(($el) => {
        expect($el.data.key).equals('outcomeNode2')
      })

      FilterBar.selectPattern('EscalatePattern').checkSelectedPattern('EscalatePattern')
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.subflow2.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'subflow3').then(($el) => {
        expect($el.data.key).equals('SubFlow3')
      })
      DiagramUtils.findChildNodeByLinkText('SubFlow3', 'contained').then(($el) => {
        expect($el.data.key).equals('outcomeNode1')
      })
      DiagramUtils.findChildNodeByLinkText('SubFlow3', 'escalate').then(($el) => {
        expect($el.data.key).equals('outcomeNode2')
      })
      DiagramUtils.findChildNodeByLinkText('SubFlow3', 'fallback').then(($el) => {
        expect($el.data.key).equals('outcomeNode3')
      })

      FilterBar.selectPattern('FallbackPattern').checkSelectedPattern('FallbackPattern')
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.tag1.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.tag1.nodeLabel,
        dataObject.pattern_diagram.skillsrouter1.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'abandon').then(($el) => {
        expect($el.data.key).equals('outcomeNode1')
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.skillsrouter1.nodeLabel,
        dataObject.pattern_diagram.subflow2.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'fallback').then(($el) => {
        expect($el.data.key).equals('outcomeNode2')
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'hitl').then(($el) => {
        expect($el.data.key).equals('outcomeNode3')
      })

      FilterBar.selectPattern('SkillPattern').checkSelectedPattern('SkillPattern')
      DiagramUtils.findDiagram()
      DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.subflow1.nodeLabel,
        dataObject.pattern_diagram.tag1.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.tag1.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.tag1.nodeLabel, 'escalate').then(($el) => {
        expect($el.data.key).equals('outcomeNode1')
      })
      DiagramUtils.findChildNodeByLinkText(
        dataObject.pattern_diagram.tag1.nodeLabel,
        dataObject.pattern_diagram.subflow2.linkLabel,
      ).then(($el) => {
        expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
      })
      DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'skilloutcome').then(
        ($el) => {
          expect($el.data.key).equals('outcomeNode2')
        },
      )
    },
  )

  it('[CMTS-15624] Nodes tree changes when switching between patterns on the Flows tab', { tags: '@smoke' }, () => {
    cy.precondition('node-tree', 'GlobalPattern')
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    FilterBar.checkSelectedPattern('GlobalPattern').checkSelectedFlow('SubFlow1').checkSelectedChannel('IVR')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.openq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })

    FilterBar.selectPattern('OutboundPattern')
      .checkSelectedPattern('OutboundPattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'subflow2').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })

    FilterBar.selectPattern('EscalatePattern')
      .checkSelectedPattern('EscalatePattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.openq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'subflow2').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })

    FilterBar.selectPattern('FallbackPattern')
      .checkSelectedPattern('FallbackPattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'tagnode1').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })

    FilterBar.selectPattern('SkillPattern')
      .checkSelectedPattern('SkillPattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'tagnode1').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
  })

  it('[CMTS-15665] - Nodes tree changes when switching between flows on the Flows tab', { tags: '@smoke' }, () => {
    cy.precondition('node-tree', 'GlobalPattern')
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    FilterBar.checkSelectedPattern('GlobalPattern').checkSelectedFlow('SubFlow1').checkSelectedChannel('IVR')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.openq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    FilterBar.selectFlow('SubFlow2')
      .checkSelectedPattern('GlobalPattern')
      .checkSelectedFlow('SubFlow2')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.closedq2.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'custom').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'contained').then(($el) => {
      expect($el.data.key).equals('endpointNode3')
    })

    FilterBar.selectPattern('EscalatePattern')
      .checkSelectedPattern('EscalatePattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.openq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'subflow2').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    FilterBar.selectFlow('SubFlow2')
      .checkSelectedPattern('EscalatePattern')
      .checkSelectedFlow('SubFlow2')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'subflow3').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    FilterBar.selectFlow('SubFlow3')
      .checkSelectedPattern('EscalatePattern')
      .checkSelectedFlow('SubFlow3')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'contained').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'fallback').then(($el) => {
      expect($el.data.key).equals('endpointNode3')
    })

    FilterBar.selectPattern('SkillPattern')
      .checkSelectedPattern('SkillPattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'tagnode1').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    FilterBar.selectFlow('SubFlow2')
      .checkSelectedPattern('SkillPattern')
      .checkSelectedFlow('SubFlow2')
      .checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'skilloutcomes').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
  })

  it('[CMTS-16009] - Nodes tree changes when switching projects on the Patterns tab', { tags: '@smoke' }, () => {
    cy.precondition('Project1', 'Project1Pattern')

    FilterBar.checkSelectedProject('Project1').checkSelectedPattern('Project1Pattern')

    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('outcomeNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow1.nodeLabel, 'custom').then(($el) => {
      expect($el.data.key).equals('outcomeNode2')
    })

    cy.precondition('Project2', 'Project2Pattern')
    FilterBar.checkSelectedProject('Project2').checkSelectedPattern('Project2Pattern')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.pattern_diagram.subflow1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow1.nodeLabel,
      dataObject.pattern_diagram.subflow2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.subflow2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.pattern_diagram.subflow2.nodeLabel,
      dataObject.pattern_diagram.skillsrouter1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.pattern_diagram.skillsrouter1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.skillsrouter1.nodeLabel, 'subflow4').then(($el) => {
      expect($el.data.key).equals('SubFlow4')
    })
    DiagramUtils.findChildNodeByLinkText('SubFlow4', 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText('SubFlow4', 'No').then(($el) => {
      expect($el.data.key).equals('outcomeNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.pattern_diagram.subflow2.nodeLabel, 'subflow3').then(($el) => {
      expect($el.data.key).equals('SubFlow3')
    })
    DiagramUtils.findChildNodeByLinkText('SubFlow3', 'default').then(($el) => {
      expect($el.data.key).equals('outcomeNode1')
    })
    DiagramUtils.findChildNodeByLinkText('SubFlow3', 'subflow5').then(($el) => {
      expect($el.data.key).equals('SubFlow5')
    })
    DiagramUtils.findChildNodeByLinkText('SubFlow5', 'RegisterUser').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    cy.newProject('empty-project')
    NavigationBar.clickPatternBtn()
    DiagramUtils.findDiagram()
    FilterBar.checkSelectedProject('emprtyProject-test-postman')
      .isEmptyProjectToastMessageDisplayed('Could not find any selected flow.')
      .checkSelectedPattern('NO PATTERN SELECTED')
    DiagramUtils.isDiagramEmpty()
  })

  it('[CMTS-16015] - Nodes tree changes when switching projects on the Flows tab', () => {
    cy.precondition('Project1', 'Project1Pattern')
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    FilterBar.checkSelectedProject('Project1')
      .checkSelectedPattern('Project1Pattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')

    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })

    cy.precondition('Project2', 'Project2Pattern')
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    FilterBar.checkSelectedProject('Project2')
      .checkSelectedPattern('Project2Pattern')
      .checkSelectedFlow('SubFlow1')
      .checkSelectedChannel('IVR')

    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    cy.newProject('empty-project')
    NavigationBar.clickFlowBtn()
    DiagramUtils.findDiagram()
    FilterBar.isEmptyProjectToastMessageDisplayed('Could not find any selected flow.')
      .checkSelectedProject('emprtyProject-test-postman')
      .checkSelectedPattern('NO PATTERN SELECTED')
      .checkSelectedFlow('All Flows')
      .checkSelectedChannel('NO CHANNEL SELECTED')
    DiagramUtils.isDiagramEmpty()
  })
})
