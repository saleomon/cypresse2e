import NavigationBar from '../../../classes/navigation/NavigationBar'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'
import { dataObject } from '../../../fixtures/data/data-spec-projects'
import FilterBar from '../../../classes/bars/FilterBar'
import PaletteBox from '../../../classes/PaletteBox'
import AddNewSubFlowNodePopUp from '../../../classes/popups/AddNewSubFlowNodePopUp'
import AddNewClosedQuestionNodePopUp from '../../../classes/popups/AddNewClosedQuestionNodePopUp'
import AddNewPromptNodePopUp from '../../../classes/popups/AddNewPromptNodePopUp'
import AddNewGoLinkNodePopUp from '../../../classes/popups/AddNewGoLinkNodePopUp'
import AddNewOutcome from '../../../classes/popups/AddNewOutcome'
import NodeDetails from '../../../classes/NodeDetails'
import AddNewTagNodePopUp from '../../../classes/popups/AddNewTagNodePopUp'
import TabFlowSideBar from '../../../classes/bars/TabFlowSideBar'

describe('Complex diagram test', () => {
  beforeEach(() => {
    cy.fixture('projects-templates/add-node-project.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        Cypress.env('projectId', projectInfo.id)
        cy.loginUI()
        NavigationBar.selectProject(projectInfo.label)
      })
      NavigationBar.openPatternsPage()
      FilterBar.selectPattern('general')
      DiagramUtils.findDiagram()
      DiagramUtils.doubleClick('subflow12')
      NodeDetails.closeNodeDetails()
      DiagramUtils.findDiagram()
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('CMTS-62-Scenario1', () => {
    DiagramUtils.addNodeInDiagram(PaletteBox.tagNodeIdSelector, 'Start', true)
    AddNewTagNodePopUp.setLabel('UpdateEmailRouting')
      .setDescription('Routing based on intent')
      .setLink('default')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
      expect($el.data.key).equals('UpdateEmailRouting')
    })

    DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'UpdateEmailRouting')
    AddNewSubFlowNodePopUp.setLabel('AddEmail').setLink('Add').setDescription('Add new email address').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('UpdateEmailRouting', 'Add').then(($el) => {
      expect($el.data.key).equals('AddEmail')
    })

    DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'UpdateEmailRouting')
    AddNewSubFlowNodePopUp.setLabel('EditDeleteEmail')
      .setLink('Edit/Delete')
      .setDescription('Edit or delete an email address')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('UpdateEmailRouting', 'Edit/Delete').then(($el) => {
      expect($el.data.key).equals('EditDeleteEmail')
    })

    DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'UpdateEmailRouting')
    AddNewSubFlowNodePopUp.setLabel('SetTypeEmail')
      .setLink('SetType')
      .setDescription('Determine the type of email provided')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('UpdateEmailRouting', 'SetType').then(($el) => {
      expect($el.data.key).equals('SetTypeEmail')
    })

    DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'UpdateEmailRouting')
    AddNewSubFlowNodePopUp.setLabel('SetPrimaryEmail')
      .setLink('SetPrimary')
      .setDescription('Set an email address to be your primary email')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('UpdateEmailRouting', 'SetPrimary').then(($el) => {
      expect($el.data.key).equals('SetPrimaryEmail')
    })

    PaletteBox.expand('Exits')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddEmail')
    AddNewOutcome.setLinkLabel('SetType').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('AddEmail', 'SetType').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from UpdateEmailRouting')
    DiagramUtils.doubleClick('UpdateEmailRouting')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'EditDeleteEmail')
    AddNewOutcome.setLinkLabel('SetType').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('EditDeleteEmail', 'SetType').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from AddEmail')
    DiagramUtils.doubleClick('AddEmail')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'EditDeleteEmail')
    AddNewOutcome.setLinkLabel('SetPrimary').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('EditDeleteEmail', 'SetPrimary').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from EditDeleteEmail')
    DiagramUtils.doubleClick('EditDeleteEmail')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'EditDeleteEmail')
    AddNewOutcome.setLinkLabel('AnythingElse').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('EditDeleteEmail', 'AnythingElse').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.doubleClick('SetTypeEmail')
    NodeDetails.closeNodeDetails()
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'SetTypeEmail')
    AddNewOutcome.setLinkLabel('AnythingElse').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('SetTypeEmail', 'AnythingElse').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from SetTypeEmail')
    DiagramUtils.doubleClick('SetTypeEmail')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'SetPrimaryEmail')
    AddNewOutcome.setLinkLabel('AnythingElse').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('SetPrimaryEmail', 'AnythingElse').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from SetPrimaryEmail')
    DiagramUtils.doubleClick('SetPrimaryEmail')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()
    DiagramUtils.findDiagram()
  })

  it('CMTS-62-Scenario2', () => {
    DiagramUtils.addNodeInDiagram(PaletteBox.closedQNodeIdSelector, 'Start', true)
    AddNewClosedQuestionNodePopUp.setLabel('QGetAddress')
      .setDescription('This is first ClosedQuestion description')
      .setDefaultClosedQuestion('This is test question')
      .setLink('default')
      .setLinkDescription('This is first ClosedQuestion link description')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('Start', 'default').then(($el) => {
      expect($el.data.key).equals('QGetAddress')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.promptNodeIdSelector, 'QGetAddress')
    AddNewPromptNodePopUp.setLabel('ValidAddressFormat').setLink('default').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('QGetAddress', 'default').then(($el) => {
      expect($el.data.key).equals('ValidAddressFormat')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.closedQNodeIdSelector, 'ValidAddressFormat')
    AddNewClosedQuestionNodePopUp.setLabel('QConfirmAddress')
      .setDescription('This is second ClosedQuestion description')
      .setDefaultClosedQuestion('This is test question')
      .setLink('default')
      .setLinkDescription('This is second ClosedQuestion link description')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('ValidAddressFormat', 'default').then(($el) => {
      expect($el.data.key).equals('QConfirmAddress')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.promptNodeIdSelector, 'QConfirmAddress')
    AddNewPromptNodePopUp.setLabel('AddressConfirmed')
      .setDescription('Thats great')
      .setLink('Yes')
      .setLinkDescription('This is AddressConfirmedPromptNode link description')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('QConfirmAddress', 'Yes').then(($el) => {
      expect($el.data.key).equals('AddressConfirmed')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.promptNodeIdSelector, 'QConfirmAddress')
    AddNewPromptNodePopUp.setLabel('AddressNotConfirmed')
      .setDescription('Thats sorry about that')
      .setLink('No')
      .setLinkDescription('This is AddressNotConfirmedPromptNode link description')
      .clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('QConfirmAddress', 'No').then(($el) => {
      expect($el.data.key).equals('AddressNotConfirmed')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowNodeIdSelector, 'AddressNotConfirmed')
    AddNewSubFlowNodePopUp.setLabel('OfferSMS').setLink('default').setDescription('Offer the SMS').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('AddressNotConfirmed', 'default').then(($el) => {
      expect($el.data.key).equals('OfferSMS')
    })

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.goToNodeIdSelector, 'OfferSMS')
    AddNewGoLinkNodePopUp.setLink('Confirmed').selectLinkToNode('QConfirmAddress').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('OfferSMS', 'Confirmed').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })

    PaletteBox.expand('Exits')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.subflowDefaultIdSelecor, 'AddressConfirmed')
    AddNewOutcome.setLinkLabel('SetTypeAddress').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('AddressConfirmed', 'SetTypeAddress').then(($el) => {
      expect($el.data.key).contains('endpointNode')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from QConfirmAddress')
    DiagramUtils.doubleClick('QConfirmAddress')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from QConfirmAddress')
    DiagramUtils.doubleClick('AddressConfirmed')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()

    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PaletteBox.goToNodeIdSelector, 'OfferSMS')
    AddNewGoLinkNodePopUp.setLink('SMSDeclined').selectLinkToNode('QGetAddress').clickOnAddBtn()
    DiagramUtils.findChildNodeByLinkText('OfferSMS', 'SMSDeclined').then(($el) => {
      expect($el.data.key).equals('gotoNode2')
    })

    DiagramUtils.findDiagram()
    cy.log('Deleting extra nodes from QConfirmAddress')
    DiagramUtils.doubleClick('OfferSMS')
    NodeDetails.selectTabByName('Flow')
    TabFlowSideBar.flowActions('default')
    NodeDetails.selectMenuItemByName('Delete Flow')
    NodeDetails.closeNodeDetails()
    DiagramUtils.findDiagram()
  })
})
