import { dataObject } from '../../../fixtures/data/data-spec-projects'
import { ProjectInfo } from '../../../types/response/ProjectInfo'
import DiagramUtils from '../../../support/DiagramUtils'
import NodeDetails from '../../../classes/NodeDetails'
import FilterBar from '../../../classes/bars/FilterBar'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import TabPropertiesSideBar from '../../../classes/bars/TabPropertiesSideBar'
import { DesignerTopBarHrefs } from '../../../fixtures/navigation-data/designer-top-bar-hrefs'
import PaletteBox from '../../../classes/PaletteBox'

describe('Sub-flow node actions', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.fixture('projects-templates/qa_autotest.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        cy.selectProjectAndPattern(projectInfo.label, 'subflow-prompt')
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  it('[CMTS-54] Double clicking sub-flow node leads to flows page', () => {
    NavigationBar.openPatternsPage()
    FilterBar.selectPattern(dataObject.flow_diagram.prompt1.pattern)
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    FilterBar.checkSelectedFlow(dataObject.flow_diagram.prompt1.flow)
    DiagramUtils.findDiagram()
    FilterBar.checkSelectedChannel('IVR')
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.setDescription(dataObject.flow_diagram.prompt1.nodeDescription)
  })

  it('[ CMTS-14117 ] For Condition label , update exit name after changing Flow Label', () => {
    const updatedLabel = 'TestingConditionLabel'
    cy.log('Validate the Subflow label is default')
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    DiagramUtils.findDiagram()
    FilterBar.selectPattern('subflow-prompt')
    PaletteBox.expandCollapseExits().click()
    cy.get('.circleLabel').should('contain', 'default')
    cy.log('Goto Patterns Tab and edit the label')
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.patterns)
    FilterBar.selectPattern('subflow-prompt')
    DiagramUtils.findDiagram()
    DiagramUtils.click('SubFlow1')
    NodeDetails.selectTabByName('Flow')
    NodeDetails.moreOptions().click()
    cy.log('Edit the Condition label')
    NodeDetails.selectMenuItemByName('Edit Properties')
    NodeDetails.conditionLabel().click().clear().type(updatedLabel)
    NodeDetails.saveButton().click()
    cy.log('Validate the updated Condition label')
    NavigationBar.clickTopBarButton(DesignerTopBarHrefs.flows)
    DiagramUtils.findDiagram()
    PaletteBox.expandCollapseExits().click()
    PaletteBox.checkSubNodeLabel('SubFlow1 (1) Exits', updatedLabel)
  })
})
