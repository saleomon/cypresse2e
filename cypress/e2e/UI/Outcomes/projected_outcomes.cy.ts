import PaletteBox from '../../../classes/PaletteBox'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import ProjectedOutcomeTable from '../../../classes/outcomes/ProjectedOutcomeTable'
import GlobalInteractionPattern from '../../../classes/outcomes/GlobalInteractionPatternTable'
import FilterBar from '../../../classes/bars/FilterBar'
import AddProjectedOutcomePopover from '../../../classes/popovers/outcomes/AddProjectedOutcomePopOver'
import AddNewGlobalPatternPopover from '../../../classes/popovers/outcomes/AddGlobalPatternPopOver'
import { OutcomesPageElements } from '../../../classes/outcomesPage'
import { projectedOutcomesData } from '../../../fixtures/data/outcomes_data/projectedOutcomesData'
import { contentsOutcomes } from '../../../fixtures/data/outcomes_data/outcomesTestData'
import { ProjectInfo } from '../../../types/response/ProjectInfo'

describe('table Projected Outcomes', function () {
  beforeEach(() => {
    cy.fixture('metrics-templates/projected-outcomes.json').as('project')

    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        Cypress.env('projectName', projectInfo.label)
        Cypress.env('projectId', projectInfo.id)
        NavigationBar.selectProject(projectInfo.label)
        NavigationBar.openOutcomesPage()
      })
    })
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })
  /* beforeEach(function () {
    //restore localstorage
    cy.restoreLocalStorage()
  }) 
 */

  it('CMTS-6-create-new-projected-outcome', { tags: ['@smoke', '@ui'] }, () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    AddProjectedOutcomePopover.fillOutcome(projectedOutcomesData.projectedOutcomeNew)
    cy.get(OutcomesPageElements.validationMsg).should('not.exist')
    cy.get(OutcomesPageElements.addProjectedOutcomeBtn).click()
    ProjectedOutcomeTable.checkOutcomeFields(projectedOutcomesData.projectedOutcomeNew)

    // check if new projected outcome appears in select Outcomes on modal Create New Global Pattern
    GlobalInteractionPattern.openCreateNewGlobalPattern()
    cy.get(OutcomesPageElements.globalPatternOutcomesSelect).click()
    cy.get('li.ant-select-dropdown-menu-item').contains(contentsOutcomes.projectedOutcomeLbl).should('exist')
    cy.get('.ant-select-search__field').last().blur()
    cy.get(OutcomesPageElements.openCreateNewGlobalPatternBtn).click()

    // add outcome to pattern
    GlobalInteractionPattern.editRow('PatternToTestOutcomes')
    cy.wait(2000)
    cy.multiselect(OutcomesPageElements.patternOutcomesLbl, [contentsOutcomes.projectedOutcomeLbl, 'HITL'])
    AddNewGlobalPatternPopover.clickOnSaveBtn()
    NavigationBar.openPatternsPage()
    // check if new projected outcome appears on page Patterns
    FilterBar.selectPattern('PatternToTestOutcomes')
    PaletteBox.expandCollapseExits().click()
    PaletteBox.checkPatternOutcome(contentsOutcomes.projectedOutcomeLbl)
    NavigationBar.openOutcomesPage()
  })

  it('CMTS-9-edit-projected-outcome', { tags: '@smoke' }, () => {
    ProjectedOutcomeTable.editRow('ProjectedOutcomeToEdit')

    AddProjectedOutcomePopover.fillOutcome(projectedOutcomesData.projectedOutcomeUpdated)
    AddProjectedOutcomePopover.clickOnSaveBtn()

    // check if new outcome is updated
    ProjectedOutcomeTable.checkOutcomeFields(projectedOutcomesData.projectedOutcomeUpdated)
  })

  it('CMTS-100-delete-projected-outcome', { tags: '@smoke' }, () => {
    ProjectedOutcomeTable.deleteRow('ProjectedOutcomeToDelete')
    cy.checkToastMessage('Deleted outcome') // check if message Deleted outcome appears
    ProjectedOutcomeTable.checkAbsenceProjectedOutcome('ProjectedOutcomeToDelete') // check if outcome is deleted
  })

  it('CMTS-101-reset-functionality-while-outcomes-adding', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // fill pop-up Create New Projected Outcome
    AddProjectedOutcomePopover.fillOutcome(projectedOutcomesData.projectedOutcomeNew)
    cy.contains('Reset').should('be.visible').click() // reset fields on pop-up add projected outcome
    AddProjectedOutcomePopover.outcomeNameField().should('have.value', '')
    AddProjectedOutcomePopover.outcomeDescriptionField().should('have.value', '')
    AddProjectedOutcomePopover.outcomePercentage().should('have.value', '33%')
    AddProjectedOutcomePopover.checkDefaultValue(
      AddProjectedOutcomePopover.outcomeValueSel,
      contentsOutcomes.outcomeDefaultValue,
    )
    AddProjectedOutcomePopover.checkDefaultValue(
      AddProjectedOutcomePopover.outcomeColorSel,
      contentsOutcomes.outcomeDefaultColor,
    )
    cy.get(OutcomesPageElements.openCreateNewProjectedOutcomeBtn).should('be.visible').click()
  })

  it('CMTS-8819-to-validate-it-is-possible-to-create-the-outcome-with-empty-description', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    AddProjectedOutcomePopover.fillOutcome(projectedOutcomesData.projectedOutcomeNoDescription)
    AddProjectedOutcomePopover.clickOnAddBtn()
    ProjectedOutcomeTable.checkOutcomeFields(projectedOutcomesData.projectedOutcomeNoDescription)
  })

  it('CMTS-9584-check-change-outcomes-colors', () => {
    ProjectedOutcomeTable.editRow('HITL')
    cy.selectCustom(AddProjectedOutcomePopover.outcomeColorSel, ' #EB4627 ')
    AddProjectedOutcomePopover.clickOnSaveBtn()
    NavigationBar.openPatternsPage()
    FilterBar.selectPattern('PatternToTestOutcomes')
    PaletteBox.expandCollapseExits().click()
    PaletteBox.checkExitColor('HITL').should('have.attr', 'style', 'border-color: rgb(143, 188, 255);')
  })

  it.skip('CMTS-9521 newly added outcome appears on modal Create New Global Pattern', () => {
    NavigationBar.openOutcomesPage()
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    cy.fillOutcome(
      'ProjectedOutcome9521',
      contentsOutcomes.outcomeDescription,
      contentsOutcomes.outcomePercentage,
      contentsOutcomes.outcomeValueNeutral,
      contentsOutcomes.outcomeColor,
    )
    cy.get(OutcomesPageElements.addProjectedOutcomeBtn).click()
    cy.get(OutcomesPageElements.openCreateNewGlobalPatternBtn).click()
    cy.get('[data-cy="patternOutcomesField"]').should('be.visible').click()
    cy.get(OutcomesPageElements.patternOutcomesLbl)
      .parent()
      .next()
      .find('[role="combobox"]')
      .filter(':visible')
      .then(($combobox) => {
        cy.wrap($combobox)
          .attribute('aria-controls')
          .then(($id) => {
            cy.get('#' + $id)
              .find('ul>li')
              .should('contain.text', 'ProjectedOutcome9521')
          })
      })
  })

  /*  afterEach(function () {
    //preserve local storage after each hook
    cy.saveLocalStorage()
  }) */
})

describe('CMTS-8414-test-pack-for-projected-outcomes-validations', { tags: '@validation' }, () => {
  beforeEach(() => {
    cy.fixture('metrics-templates/projected-outcomes.json').as('projects')

    cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
      cy.loginAPI()
      cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
        cy.loginUI()
        Cypress.env('projectName', projectInfo.label)
        Cypress.env('projectId', projectInfo.id)
        NavigationBar.selectProject(projectInfo.label)
        NavigationBar.openOutcomesPage()
      })
    })
  })

  after(() => {
    cy.removeProject(Cypress.env('projectId'))
  })

  it('CMTS-8428-to-validate-it-is-not-possible-for-user-to-create-the-fallback-outcome-cause-it-is-default', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.notUniqueErrMsg,
      OutcomesPageElements.outcomeNameField,
      'Fallback',
      true,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('check if all required color values are present in select Color', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    // check if all required color values are present in select Color
    cy.get(OutcomesPageElements.outcomeColorSel).filter(':visible').parent().next().find('[role="combobox"]').click()
    cy.get('.ant-select-dropdown-menu-root')
      .filter(':visible')
      .find('li')
      .each(($item, i) => {
        expect($item.text()).to.equal(contentsOutcomes.colorSet[i])
      })
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check if message Please provide a unique condition label after clicking on button Add', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    // check if message 'Please provide a unique condition label' after clicking on button 'Add'
    cy.get(OutcomesPageElements.addProjectedOutcomeBtn).click()
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.emptyFieldErrMsg,
      OutcomesPageElements.outcomeNameField,
      '',
      true,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('CMTS-8424-to-validate-the-name-with-33-characters-is-not-valid-outcome-s-name', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.lengthLimitErrMsg,
      OutcomesPageElements.outcomeNameField,
      contentsOutcomes.name33Letters,
      true,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('CMTS-8421-to-validate-the-name-with-32-characters-including-special-characters-inside-is-a-valid', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.lengthLimitErrMsg,
      OutcomesPageElements.outcomeNameField,
      'abcdfghABCDFGHIJKLMNOPQRSTUVWZYZ',
      false,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('CMTS-8415-to-validate-the-name-with-2-characters-is-a-valid-outcome-s-name', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.lengthLimitErrMsg,
      OutcomesPageElements.outcomeNameField,
      'ab',
      false,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('CMTS-8427-to-validate-it-is-not-possible-to-create-the-outcome-with-the-default-outcomes-name-which', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.notUniqueErrMsg,
      OutcomesPageElements.outcomeNameField,
      'HITL',
      true,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('CMTS-8423-to-validate-the-name-with-1-character-is-not-valid-outcome-s-name', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.lengthLimitErrMsg,
      OutcomesPageElements.outcomeNameField,
      contentsOutcomes.oneLetter,
      true,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('CMTS-8421-to-validate-the-name-with-32-characters-including-special-characters-inside-is-a-valid', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.lengthLimitErrMsg,
      OutcomesPageElements.outcomeNameField,
      contentsOutcomes.projectedOutcomeNameSpecSymbols,
      false,
    )
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('check if all messages diappear after clicking on the button Reset', () => {
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // open modal Create New Projected Outcome
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.emptyFieldErrMsg,
      OutcomesPageElements.outcomeNameField,
      '',
      true,
    )
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomePercentageLabel,
      contentsOutcomes.percentageRequiredErrMsg,
      OutcomesPageElements.outcomePercentage,
      '',
      true,
    )
    // check if all messages diappear after clicking on the button 'Reset'
    AddProjectedOutcomePopover.clickOnResetBtn()
    cy.get(OutcomesPageElements.redHighlighted).should('not.exist')
    ProjectedOutcomeTable.openCreateNewProjectedOutcome() // close modal Create New Projected Outcome
  })

  it('check if message Please provide a unique condition label after clicking in field Outcome Name and then clicl outside of field Outcome Name', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.emptyFieldErrMsg,
      OutcomesPageElements.outcomeNameField,
      '',
      true,
    )
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check if message Label must not contain spaces or underscores. appears if input space into outcome name', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.containSpacesErrMsg,
      OutcomesPageElements.outcomeNameField,
      'a b',
      true,
    )
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check if message Label must not contain spaces or underscores. appears if input underscore into outcome name', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomeNameLabel,
      contentsOutcomes.containSpacesErrMsg,
      OutcomesPageElements.outcomeNameField,
      'a_b',
      true,
    )
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check if message percentage is required appears if input letter into field Percentage', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomePercentageLabel,
      contentsOutcomes.percentageRequiredErrMsg,
      OutcomesPageElements.outcomePercentage,
      '',
      true,
    )
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check if message percentage is required appears if make empty field Percentage', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()

    AddProjectedOutcomePopover.checkValidationError(
      OutcomesPageElements.outcomePercentageLabel,
      contentsOutcomes.percentageRequiredErrMsg,
      OutcomesPageElements.outcomePercentage,
      'a',
      true,
    )
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check that can not input value more than 100 for field Percentage ', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()

    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').clear()
    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').type('101')
    cy.get('.ant-popover-title').filter(':visible').click()
    cy.get(OutcomesPageElements.outcomePercentage).should('have.attr', 'aria-valuenow', '100')
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check that can not input value less than 0 for field Percentage  ', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()

    cy.get(OutcomesPageElements.outcomePercentage).clear()
    cy.get(OutcomesPageElements.outcomePercentage).type('-1')
    cy.get('.ant-popover-title').filter(':visible').click()
    cy.get(OutcomesPageElements.outcomePercentage).should('have.attr', 'aria-valuenow', '0')
    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check that can not set value more than 100 for field Percentage with arrow up', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()

    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').clear()
    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').type('99')
    cy.get(OutcomesPageElements.increaseOutcomePercentage)
      .invoke('show')
      .click()
      .should('have.attr', 'aria-disabled', 'true')

    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  it('check that can not set value less than 0 for field Percentage with arrow down', () => {
    // open modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()

    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').clear()
    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').type('1')
    cy.get(OutcomesPageElements.decreaseOutcomePercentage)
      .invoke('show')
      .invoke('show')
      .click()
      .should('have.attr', 'aria-disabled', 'true')

    // close modal Create New Projected Outcome
    ProjectedOutcomeTable.openCreateNewProjectedOutcome()
  })

  /*
  // check required fields Create Projected Outcome - low priority - WIP
  cy.get('label[title="Outcome Name"]').should('be.visible').and('have.class', '.ant-form-item-required')
  cy.get('.ant-form-item-label').find('label').contains('Outcome Name:').should('be.visible').and('have.class', '.ant-form-item-required')
  cy.get('label').contains('Outcome Name').should('be.visible').and('have.class', '.ant-form-item-required')
  cy.get('label[title="Value"]').should('be.visible').and('not.have.class', '.ant-form-item-required')
  cy.get('label[title="Color"]').should('be.visible').and('have.class', '.ant-form-item-required') */
})
