import { contentsProject } from '../../../fixtures/data/outcomes_data/outcomesTestData'
import { LeftSidebarHrefs } from '../../../fixtures/navigation-data/left-sidebar-hrefs'
import NavigationBar from '../../../classes/navigation/NavigationBar'
import ProjectsPage from '../../../classes/control-panel/ProjectsPage'
import DialogPopOver from '../../../classes/popovers/DialogPopOver'
import CreateNewProjectPopOver from '../../../classes/popovers/CreateNewProjectPopOver'

import { ProjectInfo } from '../../../types/response/ProjectInfo'

describe('[CMTS-5] To verify that new project is created successfully', function () {
  beforeEach(() => {
    cy.loginUI()
    cy.loginAPI()
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(Cypress.env('projectIdtoDelete'))
    })
  })

  it('Create new project - fill in all fields', { tags: '@smoke' }, () => {
    cy.intercept('POST', '/api/projects').as('current-project')
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()
    const projectName = contentsProject.projectName + id
    cy.fillProject(projectName, contentsProject.projectDescription, contentsProject.projectType)
    DialogPopOver.clickOnAddBtn()
    cy.wait('@current-project')
      .its('response')
      .then((json) => {
        expect(json).to.have.property('statusCode', 201)
        Cypress.env('projectIdtoDelete', json.body.id)
        cy.log(Cypress.env('projectIdtoDelete'))
      })
    cy.checkToastMessage('Project created')
    NavigationBar.projectsDropDown().click()
    ProjectsPage.checkProjectExist(contentsProject.projectName)
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    ProjectsPage.getRowProjectInfo(projectName, contentsProject.projectType, contentsProject.projectDescription)
  })

  it('Create new project - only project name is filled', { tags: '@smoke' }, () => {
    cy.intercept('POST', '/api/projects').as('current-project')
    const uuid = () => Cypress._.random(0, 1e6)
    const id = uuid()
    const projectName = contentsProject.projectName + id
    CreateNewProjectPopOver.createProjectBtn().click()
    CreateNewProjectPopOver.fillProjectName(projectName)
    DialogPopOver.clickOnAddBtn()
    NavigationBar.clickLeftSidebarButton(LeftSidebarHrefs.controlPanel)
    ProjectsPage.getRowProjectLabel(projectName, 'Non')
    cy.wait('@current-project')
      .its('response')
      .then((json) => {
        expect(json).to.have.property('statusCode', 201)
        Cypress.env('projectIdtoDelete', json.body.id)
        cy.log(Cypress.env('projectIdtoDelete'))
      })
    cy.checkToastMessage('Project created')
    NavigationBar.projectsDropDown().click()
    ProjectsPage.checkProjectExist(contentsProject.projectName)
    NavigationBar.openOutcomesPage()
  })
})

describe('create new project - validation / reset fields', function () {
  before(() => {
    cy.loginUI()
    NavigationBar.openOutcomesPage()
  })

  it('Create new project with existing project name', { tags: ['@validation'] }, () => {
    CreateNewProjectPopOver.createProjectBtn().click()
    CreateNewProjectPopOver.fillProjectName('Test project name')
    DialogPopOver.clickOnAddBtn()
    CreateNewProjectPopOver.formExplainMessage('Please provide a unique project label')
  })

  it('Create new project - check placeholders after reset', () => {
    CreateNewProjectPopOver.createProjectBtn().click()
    cy.fillProject(contentsProject.projectName, contentsProject.projectDescription, contentsProject.projectType)
    DialogPopOver.clickOnResetBtn()
    CreateNewProjectPopOver.checkFieldDefault()
  })

  it('Check placeholders default value', () => {
    CreateNewProjectPopOver.createProjectBtn().click()
    CreateNewProjectPopOver.checkFieldDefault()
  })
})
