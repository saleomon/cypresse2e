import { ProjectInfo } from '../../types/response/ProjectInfo'
import { dataObject } from '../../fixtures/data/data-spec-projects'
import NavigationBar from '../../classes/navigation/NavigationBar'
import ControlPanelPage from '../../classes/pages/UsersPage'
import DiagramUtils from '../../support/DiagramUtils'
import FilterBar from '../../classes/bars/FilterBar'
import TabFlowSideBar from '../../classes/bars/TabFlowSideBar'
import NodePropertiesSideBar from '../../classes/bars/NodePropertiesSideBar'
import TabPropertiesSideBar from '../../classes/bars/TabPropertiesSideBar'
import ProjectsPage from '../../classes/control-panel/projectsPage'
import UsersPage from '../../classes/pages/UsersPage'
import DiagramBar from '../../classes/bars/DiagramBar'
import LeftNavigationBar from '../../classes/navigation/LeftNavigationBar'
import UserSideBar from '../../classes/bars/UserSideBar'
import GeneralTabSideBar from '../../classes/bars/GeneralTabSideBar'
import ImportProjectFromJsonPopUp from '../../classes/popups/ImportProjectFromJsonPopUp'

describe('Duplicate to Channel', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()
    cy.loginAPI()
    cy.loginUI()
  })

  it('[CMTS-16795] - Duplicate to Channel: clicking Cancel button in the warning message cancels the duplication operation', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openPatternsPage()

    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('SubFlow1')
    FilterBar.selectChannel('IVR').checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('SMS')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('SMS')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of SMS channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnCancelBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.openq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-16798] - Duplicate to Channel: clicking Duplicate button in the warning message duplicates the nodes tree to a selected channel', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openPatternsPage()

    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('SubFlow1')
    FilterBar.selectChannel('IVR').checkSelectedChannel('IVR')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('SMS')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('SMS')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of SMS channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-16811] - A duplicate tree flows (label, type, description) are the same as in the duplicated one', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openPatternsPage()

    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    FilterBar.checkSelectedChannel('IVR')

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.click('Start')
    NodePropertiesSideBar.openFlow().isSideNodeConditionSel('prompt', dataObject.flow_diagram.prompt1.nodeLabel)
    NodePropertiesSideBar.isSideNodeDescription('prompt1', dataObject.flow_diagram.prompt1.linkDescription)

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    NodePropertiesSideBar.openFlow()
      .isSideNodeConditionSel('closedquestion1', dataObject.flow_diagram.closedq1.nodeLabel)
      .isSideNode('closedquestion1', 'down')
      .isSideNodeDescription('closedquestion1', dataObject.flow_diagram.closedq1.linkDescription)

      .isSideNodeConditionSel('tag1', dataObject.flow_diagram.tag1.nodeLabel)
      .isSideNode('tag1', 'down')
      .isSideNodeDescription('tag1', dataObject.flow_diagram.tag1.linkDescription)

    DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
    NodePropertiesSideBar.openFlow()
      .isSideNodeConditionSel('openquestion1', dataObject.flow_diagram.openq1.nodeLabel)
      .isSideNode('openquestion1', 'down')
      .isSideNodeDescription('openquestion1', dataObject.flow_diagram.openq1.linkDescription)

      .isSideNodeConditionSel('OpenQuestion1', dataObject.flow_diagram.openq1.nodeLabel)
      .isSideNode('OpenQuestion1', 'right')
      .isSideNodeDescription('OpenQuestion1', '')

      .isSideNodeConditionSel('no', 'addPoint')
      .isSideNode('no', 'down')
      .isSideNodeDescription('no', 'no')

      .isSideNodeConditionSel('Repair', dataObject.flow_diagram.closedq1.nodeLabel)
      .isSideNode('Repair', 'loop')
      .isSideNodeDescription('Repair', '')

    DiagramUtils.click(dataObject.flow_diagram.openq1.nodeLabel)
    NodePropertiesSideBar.openFlow()
      .isSideNodeConditionSel('abandon', 'Fallback')
      .isSideNode('abandon', 'left')
      .isSideNodeDescription('abandon', '')

      .isSideNodeConditionSel('default', 'default')
      .isSideNode('default', 'down')
      .isSideNodeDescription('default', '')

      .isSideNodeConditionSel('escalate', 'Escalate')
      .isSideNode('escalate', 'right')
      .isSideNodeDescription('escalate', '')

    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    NodePropertiesSideBar.openFlow()
      .isSideNodeConditionSel('default', 'default')
      .isSideNode('default', 'down')
      .isSideNodeDescription('default', '')

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('SMS')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('RWC')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of RWC channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnCancelBtn()

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-16818] - A duplicate tree nodes (label, type, description) are the same as in the duplicated one', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openPatternsPage()

    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    FilterBar.checkSelectedChannel('IVR')

    DiagramUtils.click('Start')
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet('Start')
      .isNodeTypeSet('startPoint')
      .isDescriptionSet('Pattern Flow Start')

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.prompt1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.prompt1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.closedq1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.closedq1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.closedq1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.openq1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.openq1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.openq1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.openq1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.tag1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.tag1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.tag1.nodeDescription)

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('SMS')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('RWC')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of RWC channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('RWC').checkSelectedChannel('RWC')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.click('Start')
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet('Start')
      .isNodeTypeSet('startPoint')
      .isDescriptionSet('Pattern Flow Start')

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.prompt1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.prompt1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.closedq1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.closedq1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.closedq1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.openq1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.openq1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.openq1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.openq1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.tag1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.tag1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.tag1.nodeDescription)

    DiagramUtils.click('Start')
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet('Start')
      .isNodeTypeSet('startPoint')
      .isDescriptionSet('Pattern Flow Start')

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.prompt1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.prompt1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.closedq1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.closedq1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.closedq1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.closedq1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.openq1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.openq1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.openq1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.openq1.nodeDescription)

    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isNodeLabelSet(dataObject.flow_diagram.tag1.nodeLabel)
      .isNodeTypeSet(dataObject.flow_diagram.tag1.nodeType)
      .isDescriptionSet(dataObject.flow_diagram.tag1.nodeDescription)

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17068] - Duplicating a channel without a nodes tree to a channel with a nodes tree deletes the existing nodes tree', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openPatternsPage()

    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.openq1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })

    FilterBar.selectChannel('IVR').checkSelectedChannel('IVR')

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('SMS')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('SMS')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of SMS channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'OpenQuestion1').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17069] - A nodes tree can be duplicated to a selected channel from the Patterns tab', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('RWC')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of RWC channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('RWC').checkSelectedChannel('RWC')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17110] - Nodes IDs change on the duplicated channel', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.checkNodesId(dataObject.flow_diagram.prompt1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '1').should('exist')

    DiagramUtils.checkNodesId('Tag1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.tag1.nodeLabel, '0').should('exist')

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('MM')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of MM channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('MM').checkSelectedChannel('MM')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.checkNodesId(dataObject.flow_diagram.prompt1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '1').should('exist')

    DiagramUtils.checkNodesId('Tag1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.tag1.nodeLabel, '0').should('exist')

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17112] - Nodes IDs on the original channel do not change after duplicating the nodes tree', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.checkNodesId(dataObject.flow_diagram.closedq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '3').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '3').should('exist')

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('IVR')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of IVR channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('IVR').checkSelectedChannel('IVR')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.checkNodesId(dataObject.flow_diagram.closedq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.closedq1.nodeLabel, '3').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '3').should('exist')

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17114] - Node flow IDs change on the duplicated channel', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()

    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.checkNodesId(dataObject.flow_diagram.prompt1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '1').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '3').should('exist')

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('RWC')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of RWC channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('RWC').checkSelectedChannel('RWC')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })

    DiagramUtils.checkNodesId(dataObject.flow_diagram.prompt1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '1').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '3').should('exist')

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17122] - Node flow IDs on the original channel do not change after duplicating the nodes tree', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramUtils.checkNodesId(dataObject.flow_diagram.prompt1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '1').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '1').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '2').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.openq1.nodeLabel, '3').should('exist')
    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('MM')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of MM channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('MM').checkSelectedChannel('MM')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramUtils.checkNodesId(dataObject.flow_diagram.prompt1.nodeLabel).should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesFlowId(dataObject.flow_diagram.prompt1.nodeLabel, '1').should('exist')

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17123] - Node output and input conditions IDs change on the duplicated channel', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramUtils.checkNodesId(dataObject.flow_diagram.closedq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesInputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('IVR')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of IVR channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('IVR').checkSelectedChannel('IVR')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramUtils.checkNodesId(dataObject.flow_diagram.closedq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesInputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')
    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })

  it('[CMTS-17124] - Node output and input conditions IDs on the original channel do not change after duplicating the nodes tree', () => {
    LeftNavigationBar.clickOnSettingBtn()
    ProjectsPage.clickImportBtProjectS()

    cy.readFile('cypress/fixtures/projects-templates/DuplicateToChannel.json').then((projectJSON) => {
      ProjectsPage.setProjectJson(JSON.stringify(projectJSON))
      ImportProjectFromJsonPopUp.clickSubmitBtn()
    })

    LeftNavigationBar.clickOutcomecTab()
    NavigationBar.selectProject('DuplicateToChannel')
    NavigationBar.openFlowsPage()
    FilterBar.selectPattern('ChannelFromSMS')

    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramUtils.checkNodesId(dataObject.flow_diagram.closedq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesInputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')

    DiagramBar.clickOnDuplicateContentBtn()
      .isDuplicateDialogDisplayed()
      .isDuplicateToChannelBtnDisplayed()
      .clickOnDuplicateToChannelBtn()
      .isDuplicateDialogDisplayed()
      .isSelectChannelDialogDisplayed()
      .isChannelDisplayed('IVR')
      .isChannelDisplayed('RWC')
      .isChannelDisplayed('MM')
      .selectChannelFromDialog('IVR')
      .isWarningMessageDisplayed(
        'Warning !This will override all the contents (except the start node) of IVR channel for all the subflows.',
      )
      .isCancelBtnDisplayed()
      .isDuplicateBtnDisplayed()
      .clickOnDuplicateBtn()
      .isDuplicateDialogClosed()
      .isSelectChannelDialogClosed()
      .isWarningMessageClosed()
      .isToastMessageDisplayed('Layout saved successfully.')

    FilterBar.selectChannel('IVR').checkSelectedChannel('IVR')
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    FilterBar.selectChannel('SMS').checkSelectedChannel('SMS')
    DiagramUtils.findDiagram()
    DiagramUtils.findChildNodeByLinkText('Start', dataObject.flow_diagram.prompt1.linkLabel).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.prompt1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.closedq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.tag1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode1')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.closedq1.nodeLabel,
      dataObject.flow_diagram.openq1.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.openq1.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('gotoNode1')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq1.nodeLabel, 'no').then(($el) => {
      expect($el.data.key).equals('AddNode2')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'abandon').then(($el) => {
      expect($el.data.key).equals('outcomeNode3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.openq1.nodeLabel, 'escalate').then(($el) => {
      expect($el.data.key).equals('outcomeNode5')
    })
    DiagramUtils.checkNodesId(dataObject.flow_diagram.closedq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesInputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.closedq1.nodeLabel, '0').should('exist')

    DiagramUtils.checkNodesId(dataObject.flow_diagram.openq1.nodeLabel).should('exist')
    DiagramUtils.checkNodesOutputId(dataObject.flow_diagram.openq1.nodeLabel, '0').should('exist')

    LeftNavigationBar.clickOnSettingBtn()
    UserSideBar.clickProjectControl().clickDeleteBtn().clickYesBtn()
  })
})
