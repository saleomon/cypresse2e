import LoginPage from '../../classes/pages/LoginPage'
import ChangePassword from '../../classes/popups/ChangePassword'
import NavigationBar from '../../classes/navigation/NavigationBar'

describe('check login [UI]', () => {
  let userValidCreds: { title: string; username: string; password: string }[] = [
    {
      title: '[CMTS-10871] Login page. Check login with valid data',
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
    {
      title: 'login with another username register and correct password',
      username: Cypress.env<string>('username').toUpperCase(),
      password: Cypress.env('password'),
    },
  ]

  let userInvalidCreds: { title: string; username: string; password: string; errAlert: string }[] = [
    {
      title: '[CMTS-10873] Login page. Check login with invalid data',
      username: 'db-admin',
      password: 'db@dm!n123',
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
    {
      title: '[CMTS-10873] Login page. Check login with incorrect username',
      username: 'db-admin',
      password: Cypress.env('password'),
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
    {
      title: '[CMTS-10873] Login page. Check login with incorrect password',
      username: Cypress.env('username'),
      password: 'db@dm!n123',
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
    {
      title: 'login with reverse credentials [username to password and password to username]',
      username: Cypress.env('password'),
      password: Cypress.env('username'),
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
    {
      title: 'login with username spaces and correct password',
      username: '         ',
      password: Cypress.env('password'),
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
    {
      title: 'login with correct username and another password register',
      username: Cypress.env('username'),
      password: Cypress.env<string>('password').toUpperCase(),
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
    {
      title: 'login with xss and correct password',
      username: '<script>alert(123)</script>',
      password: 'dhb-admin',
      errAlert: 'Unable to log in, please try againFailed to log in',
    },
  ]

  let userEmptyCreds: { title: string; username: string; password: string }[] = [
    {
      title: '[CMTS-10870] Login page. Check login with empty "Username" field',
      username: '',
      password: Cypress.env('password'),
    },
    {
      title: '[CMTS-10872] Login page. Check login with empty "Password" field',
      username: Cypress.env('username'),
      password: '',
    },
  ]

  beforeEach(() => {
    cy.restoreLocalStorage()
  })

  userValidCreds.forEach((input) => {
    it(input.title, () => {
      LoginPage.open().setUsername(input.username).setPassword(input.password).clickOnLoginButton()

      cy.url().should('contain', '/designer/metrics')
    })
  })

  userInvalidCreds.forEach((input) => {
    it(input.title, () => {
      LoginPage.open()
        .setUsername(input.username)
        .setPassword(input.password)
        .clickOnLoginButton()
        .isErrAlert(input.errAlert)
    })
  })

  userEmptyCreds.forEach((input) => {
    it(input.title, () => {
      LoginPage.open().setUsername(input.username).setPassword(input.password).isNotClickableLoginButton()
    })
  })

  it('[CMTS-10874] Login page. Check max quantity of exceptions in case multiple attempt of login', () => {
    LoginPage.open().setUsername('123456').setPassword('123456')

    for (let i = 0; i <= 4; i++) {
      LoginPage.clickOnLoginButton()
    }

    LoginPage.isErrAlertCount(4)
  })

  it('[CMTS-10875] Login page. Check that placeholders is not shown for "Username" field in case field is not empty', () => {
    LoginPage.open().isUsernamePlaceholderDisplayed('Username')
    LoginPage.setUsername('123').isUsernameSet('123')
  })

  it.skip('[CMTS-11015] Login page. Check login with changed password', () => { // skipped - changing password is not required
    let username = 'username_' + Date.now()
    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/auth/login',
      body: {
        username: Cypress.env('username'),
        password: Cypress.env('password'),
      },
    }).then((resp1) => {
      expect(resp1.body.status).eql('success')
      expect(resp1.body.code).eql(200)
      expect(resp1.body.message).eql('successfully retrieved token')
      expect(resp1.body.payload.id).a('string')
      expect(resp1.body.payload.userName).eql('dhb-admin')
      expect(resp1.body.payload.email).eql(null)
      expect(resp1.body.payload.isAdmin).eql(true)
      expect(resp1.body.payload.accessToken).a('string')
      expect(resp1.body.payload.expiresIn).eql(600)
      expect(resp1.body.payload.refreshToken).a('string')
      expect(resp1.body.payload.refreshExpiresIn).eql(1800)

      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/admin/users',
        headers: {
          Authorization: 'Bearer ' + resp1.body.payload.accessToken,
        },
        body: [
          {
            username,
            firstName: 'testuser',
            lastName: 'testuser',
            email: username + '@deloitte.ua',
            emailVerified: false,
            enabled: true,
            credentials: [
              {
                temporary: false,
                type: 'password',
                value: '12345678',
              },
            ],
            realmRoles: ['user'],
            clientRoles: {
              'heartbeat-app': [
                'project-viewers',
                'project-creators',
                'project-editors',
                'project-delete',
                'heartreach-viewers',
              ],
            },
            attributes: {
              organization_id: 'a58dcd34-45fd-4798-b2e7-493955bd08ff',
            },
          },
        ],
      }).then((resp2) => {
        expect(resp2.body.createdUsers[0].username).eql(username)
        expect(resp2.body.createdUsers[0].firstName).eql('testuser')
        expect(resp2.body.createdUsers[0].lastName).eql('testuser')
        expect(resp2.body.createdUsers[0].email).eql(username + '@deloitte.ua')
        expect(resp2.body.createdUsers[0].emailVerified).eql(false)
        expect(resp2.body.createdUsers[0].enabled).eql(true)
        expect(resp2.body.createdUsers[0].credentials[0].temporary).eql(false)
        expect(resp2.body.createdUsers[0].credentials[0].type).eql('password')
        expect(resp2.body.createdUsers[0].credentials[0].value).eql('12345678')
        expect(resp2.body.createdUsers[0].realmRoles[0]).eql('user')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][0]).eql('project-viewers')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][1]).eql('project-creators')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][2]).eql('project-editors')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][3]).eql('project-delete')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][4]).eql('heartreach-viewers')
        expect(resp2.body.createdUsers[0].attributes.organization_id).a('string')
      })
    })
    LoginPage.open().setUsername(username).setPassword('12345678910').clickOnLoginButton()
    NavigationBar.clickUserAvatarButton().clickOnChangePassword()
    ChangePassword.setOldPassword('12345678')
      .setNewPassword('12345678910')
      .setRepeatPassword('12345678910')
      .clickOnSubmitBtn()

    LoginPage.open().setUsername(username).isUsernameSet(username)
    LoginPage.setPassword('12345678910').clickOnLoginButton()
    NavigationBar.clickUserAvatarButton()
  })

  it('[CMTS-11016] Login page. Check login with credentials of deleted user', () => {
    let username = 'username_' + Date.now()
    cy.request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/auth/login',
      body: {
        username: Cypress.env('username'),
        password: Cypress.env('password'),
      },
    }).then((resp1) => {
      expect(resp1.body.status).eql('success')
      expect(resp1.body.code).eql(200)
      expect(resp1.body.message).eql('successfully retrieved token')
      expect(resp1.body.payload.id).a('string')
      expect(resp1.body.payload.userName).eql('dhb-admin')
      expect(resp1.body.payload.email).eql(null)
      expect(resp1.body.payload.isAdmin).eql(true)
      expect(resp1.body.payload.accessToken).a('string')
      expect(resp1.body.payload.expiresIn).eql(600)
      expect(resp1.body.payload.refreshToken).a('string')
      expect(resp1.body.payload.refreshExpiresIn).eql(1800)

      cy.request({
        method: 'POST',
        url: Cypress.env('baseUrlAPI') + '/admin/users',
        headers: {
          Authorization: 'Bearer ' + resp1.body.payload.accessToken,
        },
        body: [
          {
            username,
            firstName: 'testuser',
            lastName: 'testuser',
            email: username + '@deloitte.ua',
            emailVerified: false,
            enabled: true,
            credentials: [
              {
                temporary: false,
                type: 'password',
                value: '12345678',
              },
            ],
            realmRoles: ['user'],
            clientRoles: {
              'heartbeat-app': [
                'project-viewers',
                'project-creators',
                'project-editors',
                'project-delete',
                'heartreach-viewers',
              ],
            },
            attributes: {
              organization_id: 'a58dcd34-45fd-4798-b2e7-493955bd08ff',
            },
          },
        ],
      }).then((resp2) => {
        expect(resp2.body.createdUsers[0].username).eql(username)
        expect(resp2.body.createdUsers[0].firstName).eql('testuser')
        expect(resp2.body.createdUsers[0].lastName).eql('testuser')
        expect(resp2.body.createdUsers[0].email).eql(username + '@deloitte.ua')
        expect(resp2.body.createdUsers[0].emailVerified).eql(false)
        expect(resp2.body.createdUsers[0].enabled).eql(true)
        expect(resp2.body.createdUsers[0].credentials[0].temporary).eql(false)
        expect(resp2.body.createdUsers[0].credentials[0].type).eql('password')
        expect(resp2.body.createdUsers[0].credentials[0].value).eql('12345678')
        expect(resp2.body.createdUsers[0].realmRoles[0]).eql('user')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][0]).eql('project-viewers')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][1]).eql('project-creators')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][2]).eql('project-editors')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][3]).eql('project-delete')
        expect(resp2.body.createdUsers[0].clientRoles['heartbeat-app'][4]).eql('heartreach-viewers')
        expect(resp2.body.createdUsers[0].attributes.organization_id).a('string')

        cy.request({
          method: 'DELETE',
          url: Cypress.env('baseUrlAPI') + '/admin/users/' + username,
          headers: {
            Authorization: 'Bearer ' + resp1.body.payload.accessToken,
          },
          body: {
            username,
            password: '12345678',
          },
        }).then((resp3) => {
          expect(resp3.body.status).eql('success')
          expect(resp3.body.code).eql(200)
          expect(resp3.body.message).eql('User successfully deleted')
        })
      })
    })

    LoginPage.open().setUsername(username).isUsernameSet(username)
    LoginPage.setPassword('12345678').clickOnLoginButton()
    LoginPage.isErrAlertCount(1)
  })
})
