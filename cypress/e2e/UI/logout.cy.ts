import NavigationBar from '../../classes/navigation/NavigationBar'

before(function () {
  cy.loginUI()
})

describe('Log Out test', { tags: '@ui' }, () => {
  it('Log Out', () => {
    NavigationBar.clickUserAvatarButton()
    cy.get('[role="menuitem"]')
      .contains('Log Out')
      .then(($logoutButton) => {
        $logoutButton.click()
        cy.get('h1').contains('Log In to Continue').should('be.visible')
        cy.reload().then(() => {
          cy.get('h1').contains('Log In to Continue').should('be.visible')
          cy.visit('/designer/patterns').then(() => {
            cy.get('h1').contains('Log In to Continue').should('be.visible')
          })
        })
      })
    // cy.checkToastMessage('Logging out')
  })
})
