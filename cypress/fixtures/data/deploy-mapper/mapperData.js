export const dataMapper = {
  // patterns
  pattern9873: 'PatternEndPoint',
  pattern9751: 'PatternDeleteNode',

  // node names
  subFlow: 'SubFlow',
  skillRouterName: 'SkillRouter9806',
  subFlow_1Name: 'SubFlow98061',
  subFlow_2Name: 'SubFlow98062',
  subFlowPattern: 'SubFlowPattern',
  addPoint: 'GOTO: addPoint',
  promptNode9681: 'PromptNode9681',
  userAbanonment: 'UserAbandonment',
  // has Mapping
  hasNotMapping: 'false',
  hasMapping: 'true',
  // 9873
  closedQuestion9873: 'ClosedQuestion9873',
  exitDefault: 'default',
  exitOne: 'one',
  exitTwo: 'two',
  exitThree: 'three',
  exitFour: 'four',
  exitFive: 'five',
  // prefixes
  prefixOutput: 'O_',
  prefixInput: 'I_',
  prefixFlow: 'F_',
  prefixFlowStarts: 'S',
  prefixEndPoint: 'E',
  prefixOutcome: 'G_',
  // element types
  endPoint: 'endPoint',
  flowCondition: 'flowCondition',
  inputParser: 'inputParser',
  outcomePoint: 'outcomePoint',
  outputCondition: 'outputCondition',
  startPoint: 'startPoint',
  // node types
  closedQuestion: 'closedQuestion',
  subFlowNodeType: 'subFlow',
  // channels
  channelIVR: 'IVR',

  // template labels
  defaultClosedTemplate: 'DefaultClosedTemplate',
}

export const rowDataEndPoints = [
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitDefault + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitDefault,
    payload:
      'Endpoint: ' +
      dataMapper.exitDefault +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      dataMapper.exitDefault,
    description: '',
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitOne + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitOne,
    payload:
      'Endpoint: ' +
      dataMapper.exitOne +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitOne,
    description: '',
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitTwo + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitTwo,
    payload:
      'Endpoint: ' +
      dataMapper.exitTwo +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitTwo,
    description: '',
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitThree + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitThree,
    payload:
      'Endpoint: ' +
      dataMapper.exitThree +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitThree,
    description: '',
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitFour + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitFour,
    payload:
      'Endpoint: ' +
      dataMapper.exitFour +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitFour,
    description: '',
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitFive + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitFive,
    payload:
      'Endpoint: ' +
      dataMapper.exitFive +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitFive,
    description: '',
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.userAbanonment + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.userAbanonment,
    payload:
      'Endpoint: ' +
      dataMapper.userAbanonment +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      dataMapper.userAbanonment,
    description: '',
  },
]

export const rowAllDataEndPoints = [
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitDefault + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitDefault,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: dataMapper.exitDefault,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.exitDefault +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      dataMapper.exitDefault,
    description: '',

    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitOne + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitOne,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: 'exit' + dataMapper.exitOne,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.exitOne +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitOne,
    description: '',

    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitTwo + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitTwo,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: 'exit' + dataMapper.exitTwo,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.exitTwo +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitTwo,
    description: '',

    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitThree + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitThree,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: 'exit' + dataMapper.exitThree,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.exitThree +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitThree,
    description: '',

    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitFour + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitFour,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: 'exit' + dataMapper.exitFour,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.exitFour +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitFour,
    description: '',
    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.exitFive + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.exitFive,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: 'exit' + dataMapper.exitFive,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.exitFive +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      'exit' +
      dataMapper.exitFive,
    description: '',
    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
  {
    tagID: dataMapper.prefixEndPoint + '_' + dataMapper.userAbanonment + '_' + dataMapper.closedQuestion9873,
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.userAbanonment,
    elementType: dataMapper.endPoint,
    nodeType: dataMapper.endPoint,
    channel: dataMapper.channelIVR,
    templateLabel: dataMapper.defaultClosedTemplate,
    condition: dataMapper.userAbanonment,
    conditionType: '',
    goto: '',
    pii: '',
    payload:
      'Endpoint: ' +
      dataMapper.userAbanonment +
      ' fromNode=' +
      dataMapper.closedQuestion9873 +
      ' condition=' +
      dataMapper.userAbanonment,
    description: '',
    nodeGoTo: dataMapper.closedQuestion9873,
    pattern: dataMapper.pattern9873,
  },
]

export const rowDataFlow = [
  {
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.closedQuestion9873,
    payload: 'GOTO: ' + dataMapper.exitDefault,
    description: dataMapper.exitDefault,
  },
  {
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.closedQuestion9873,
    payload: 'GOTO: ' + dataMapper.exitOne,
    description: 'exit' + dataMapper.exitOne,
  },
  {
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.closedQuestion9873,
    payload: 'GOTO: ' + dataMapper.exitTwo,
    description: 'exit' + dataMapper.exitTwo,
  },
  {
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.closedQuestion9873,
    payload: 'GOTO: ' + dataMapper.exitThree,
    description: 'exit' + dataMapper.exitThree,
  },
  {
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.closedQuestion9873,
    payload: 'GOTO: ' + dataMapper.exitFour,
    description: 'exit' + dataMapper.exitFour,
  },
  {
    hasMapping: dataMapper.hasNotMapping,
    node: dataMapper.closedQuestion9873,
    payload: 'GOTO: ' + dataMapper.exitFive,
    description: 'exit' + dataMapper.exitFive,
  },
]

export const rowData9751 = {
  tagID: dataMapper.prefixFlowStarts + '_' + 'PatternDeleteNode' + '_StartBot',
  hasMapping: dataMapper.hasNotMapping,
  node: 'Start',
  payload: 'BOT START: [object Object] | ' + dataMapper.addPoint,
  pattern: dataMapper.pattern9751,
  project: Cypress.env('projectName'),
  // "description":  "Start Flow: " + "PatternDeleteNode" + data.projectName + "/" + "PatternDeleteNode"
}

export const before9681 = {
  tagID: 'F_' + 'PromptNode9681' + '_default',
  hasMapping: 'false',
  node: 'PromptNode9681',
  payload: dataMapper.addPoint,
  description: '',
}

export const after9681 = {
  tagID: 'F_' + 'PromptNode9681' + '_new',
  hasMapping: 'false',
  node: 'PromptNode9681',
  payload: dataMapper.addPoint,
  description: '',
}

export const skillRouter9806Before = [
  {
    tagID: 'S_' + dataMapper.subFlowPattern + '_StartBot',
    hasMapping: 'false',
    node: 'Start',
    payload: 'BOT START: [object Object] | GOTO: ' + dataMapper.skillRouterName,
    description:
      'Start Flow: ' + dataMapper.subFlowPattern + Cypress.env('projectName') + '/' + dataMapper.subFlowPattern,
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_default',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: dataMapper.addPoint,
    description: '',
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_UserAbandonment',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: UserAbandonment',
    description: '',
  },
  {
    tagID: 'G_UserAbandonment_' + dataMapper.skillRouterName,
    hasMapping: 'false',
    node: 'UserAbandonment',
    payload:
      'Outcome: UserAbandonment type=outcomePoint fromNode=' +
      dataMapper.skillRouterName +
      ' condition=UserAbandonment',
    description: 'Global User Abandonment',
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_Escalate',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: Escalate',
    description: '',
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_Fallback',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: Fallback',
    description: '',
  },
]

export const skillRouter9806After = [
  {
    tagID: 'S_' + dataMapper.subFlowPattern + '_StartBot',
    hasMapping: 'false',
    node: 'Start',
    payload: 'BOT START: [object Object] | GOTO: ' + dataMapper.skillRouterName,
    description:
      'Start Flow: ' + dataMapper.subFlowPattern + Cypress.env('projectName') + '/' + dataMapper.subFlowPattern,
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_default',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: ' + dataMapper.subFlow_1Name,
    description: '',
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_UserAbandonment',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: UserAbandonment',
    description: '',
  },
  {
    tagID: 'G_UserAbandonment_' + dataMapper.skillRouterName,
    hasMapping: 'false',
    node: 'UserAbandonment',
    payload:
      'Outcome: UserAbandonment type=outcomePoint fromNode=' +
      dataMapper.skillRouterName +
      ' condition=UserAbandonment',
    description: 'Global User Abandonment',
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_Escalate',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: Escalate',
    description: '',
  },
  {
    tagID: 'F_' + dataMapper.skillRouterName + '_Fallback',
    hasMapping: 'false',
    node: dataMapper.skillRouterName,
    payload: 'GOTO: Fallback',
    description: '',
  },
]
export const csvExport = {
  // "Tag ID","Has Mapping","Node","Payload","Description"\r\n"I_ClosedQuestion9873_OfferParser","false","ClosedQuestion9873","Parser: OfferParser NLUModel",""
}
