// eslint-disable-next-line import/prefer-default-export

export default function dataForCollapsedFlows(projectName: string) {
  return {
    'Pattern - empty, All Flows': [
      {
        'Flow Name': 'empty',
        Type: 'pattern',
        'Flow Path': `${projectName}/empty`,
        Description: '',
      },
    ],
    'Pattern - subflow-prompt, SubFlow1': [
      {
        'Flow Name': 'SubFlow1',
        Type: 'node',
        'Flow Path': `${projectName}/subflowprompt/SubFlow1`,
        Description: 'This is first SubFlow description',
      },
    ],
    'Pattern - subflow-prompt, All Flows': [
      {
        'Flow Name': 'subflowprompt',
        Type: '',
        'Flow Path': `${projectName}/subflowprompt`,
        Description: '',
      },
      {
        'Flow Name': 'SubFlow1',
        Type: 'node',
        'Flow Path': `${projectName}/subflowprompt/SubFlow1`,
        Description: 'This is first SubFlow description',
      },
    ],
  }
}
