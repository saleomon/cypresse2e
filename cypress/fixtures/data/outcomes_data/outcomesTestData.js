const contentsOutcomes = {
  // projected outcomes test data
  projectedOutcomeLbl: 'TestProjectedOutcome',
  projectedOutcomeUpdated: 'NewProjectedOutcome',
  outcomeDescription: 'Description',
  outcomeDescriptionUpdated: 'NewDescription',
  outcomePercentage: '50',
  outcomePercentageUpdated: '35',
  outcomeValueNeutral: 'Negative',
  outcomeValueUpdated: 'Positive',
  outcomeColor: '#005E00',
  outcomeColorUpdated: '#FFDF66',
  outcomeDefaultValue: ' Neutral ',
  outcomeDefaultColor: ' #8FBCFF ',
  colorSet: [
    ' #FFED8F ',
    ' #FFDF66 ',
    ' #005E00 ',
    ' #3ADC49 ',
    ' #FFBFCB ',
    ' #B22929 ',
    ' #EB4627 ',
    ' #F2A787 ',
    ' #8FBCFF ',
    ' #0095C9 ',
    ' #70B9E8 ',
    ' #003378 ',
    ' #C4C4C4 ',
  ],

  // projected outcomes test data - negative scenarios
  oneLetter: 'a',
  twoSlashes: '//',
  twoBackslashes: 'a\\',
  name33Letters: 'abcdefghijklmnopqrstuwzwzTestOutc',
  projectedOutcomeNameSpecSymbols: '`~!@#$%^&*()+=[]{}:;"<>?/|,.',
  lengthLimitErrMsg: 'Length should be 2 to 32 chars',
  emptyFieldErrMsg: 'Please provide a unique condition label',
  notUniqueErrMsg: 'Label is not unique!',
  percentageRequiredErrMsg: 'percentage is required',
  containSpacesErrMsg: 'Label must not contain spaces or underscores.',
  slashesPresentErrMsg: "Please don't use slashes.",
  noErrMsg: '',
  // notUniqueErrMsg: 'Label is not unique!Please provide a unique condition label',
  channelsRequiredErrMsg: 'channels is required',
  outcomesRequiredErrMsg: 'outcomes is required',

  // global pattern test data
  globalPattern: {
    patternName: 'Pattern',
    patternType: 'Agent Assist',
    patternDescription: 'Test Description',
    patternChannels: ['RWC', ' IVR'],
    patternOutcomes: ['HITL', 'Abandon'],
  },

  patternName: 'TestPatternName',
  patternType: 'Agent Assist',
  patternDescription: 'Test Description',
  patternChannels: ['RWC', ' IVR'],
  patternOutcomes: ['Abandon', 'HITL'],
  patternNameUpd: 'TestPatternNameUpd',
  patternTypeUpd: 'Mobile App',
  patternDescriptionUpd: 'Test Description Updated',
  patternChannelsUpd: ['SMS', ' VoIP'],
  patternOutcomesUpd: ['Escalate', 'Fallback'],
  noPatternMsg: 'No Patterns yet added, Use (+) Add button below to create a new top-level pattern flow.',
  patternUpdatedMsg: 'Successfully updated pattern',
  patternDeletedMsg: 'Successfully deleted pattern',
  channelsContainsR: ['IVR', 'RWC', 'rSMS'],
  outcomesContainsA: ['Abandon', 'Contained', 'Escalate', 'Fallback'],

  // skill pattern test data
  skillPatternName: 'TestSkillPattern',
  skillPatternDescription: 'Description',
  skillPatternChannels: ['IVR', ' RWC'],
  skillPatternNameUpd: 'TestSkillPatternUpd',
  skillPatternNameUnique: 'UniqueSkillName',
  skillPatternDescriptionUpd: 'Description',
  skillPatternChannelsUpd: ['SMS', ' VoIP'],
  skillPatternChannels9782: ['IVR', 'RWC'],
  noSkillPatternMsg: 'No Skills added, Use (+) Add button below to create a new skill flow.',
  skillPatternUpdatedMsg: 'Successfully edited skill',
  skillPatternDeletedMsg: 'Successfully deleted skill',

  // skill outcome test data
  skillOutcomeName: 'TestSkillOutcome',
  skillOutcomeDescription: 'Description',
  skillOutcomePercentage: '50',
  skillOutcomeValue: 'Negative',
  skillOutcomeColor: '#FFED8F',

  skillOutcomeNameUpd: 'NewSkillOutcome',
  skillOutcomeDescriptionUpd: 'NewDescription',
  skillOutcomePercentageUpd: '35',
  skillOutcomeValueUpd: 'Positive',
  skillOutcomeColorUpd: '#FFDF66',

  skillOutcomeDefaultValue: ' Neutral ',
  skillOutcomeDefaultColor: ' #8FBCFF ',
  noSkillOutcomesMsg: 'No Rows To Show',
  skillOutcomeDeletedMsg: 'Deleted skill outcome',
}

const contentsProject = {
  projectName: 'TestProjectName',
  projectDescription: 'TestProjectDescription',
  projectType: 'POC',
}

export { contentsProject }
export { contentsOutcomes }
