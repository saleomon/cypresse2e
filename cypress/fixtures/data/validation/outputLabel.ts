import { messages } from '../messagesErrorText'

// eslint-disable-next-line import/prefer-default-export
const OutputLabelData: { title: string; input: string; message?: string; error: boolean }[] = [
  {
    title: 'Output Label - exisiting label',
    input: 'default',
    message: messages.uniqueOutputLabel,
    error: true,
  },
  {
    title: 'Output Label - 1 char',
    input: 'a',
    message: messages.labelLength,
    error: true,
  },
  {
    title: 'Output Label - 33 characters',
    input: 'morethan32morethan32morethan32mor',
    message: messages.labelLength,
    error: true,
  },
  {
    title: 'Output Label - space in name',
    input: 'space in it',
    message: messages.spacesAndUnderscores,
    error: true,
  },
  {
    title: 'Output Label - undersore_in_label',
    input: 'undersore_in_label',
    message: messages.spacesAndUnderscores,
    error: true,
  },
  {
    title: 'Output Label - 2 chars',
    input: 'aa',
    error: false,
  },
  {
    title: 'Output Label - 32 chars',
    input: 'longnamelongnamelongnamelongname',
    error: false,
  },
  {
    title: 'Output Label - special symbols',
    input: 'label"with&special//chars',
    error: false,
  },
  {
    title: 'Output Label - digits',
    input: '12345',
    error: false,
  },
  {
    title: 'Output Label - empty',
    input: '',
    message: messages.uniqueOutputLabel,
    error: true,
  },
]

export default OutputLabelData
