// eslint-disable-next-line import/prefer-default-export
export const DesignerTopBarHrefs = {
  outcomes: '/designer/metrics',
  patterns: '/designer/patterns',
  flows: '/designer/nodes',
  content: '/designer/content',
}
