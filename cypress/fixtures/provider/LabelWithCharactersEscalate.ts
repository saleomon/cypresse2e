let labelWithCharactersEscalate: {
  title: string
  linkLabel: string
  linkType: string
  sideText: string
  conditionText: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13919] Global Escalate outcome can be added when link label contains 2 characters',
    preconditionId: '13919',
    linkLabel: 'ab',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Escalate',
  },
  {
    title: '[CMTS-13926] Global Escalate outcome can be added when link label contains letters and digits',
    preconditionId: '13926',
    linkLabel: 'abSSD123',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Escalate',
  },
  {
    title: '[CMTS-13925] Global Escalate outcome can be added when link label contains 32 characters',
    preconditionId: '13925',
    linkLabel: 'AbcdefghijklMNopqrstuVwXyzabcdeF',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Escalate',
  },
]

export default labelWithCharactersEscalate
