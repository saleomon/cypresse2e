let labelWithCharactersFallback: {
  title: string
  linkLabel: string
  linkType: string
  sideText: string
  conditionText: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13979] Global Fallback outcome can be added when link label contains 2 characters',
    preconditionId: '13979',
    linkLabel: 'ab',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Fallback',
  },
  {
    title: '[CMTS-13982] Global Fallback outcome can be added when link label contains letters and digits',
    preconditionId: '13982',
    linkLabel: 'abSSD123',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Fallback',
  },
  {
    title: '[CMTS-13980] Global Fallback outcome outcome can be added when link label contains 32 characters',
    preconditionId: '13980',
    linkLabel: 'AbcdefghijklMNopqrstuVwXyzabcdeF',
    linkType: ' Normal (down) ',
    sideText: 'down',
    conditionText: 'Fallback',
  },
]

export default labelWithCharactersFallback
