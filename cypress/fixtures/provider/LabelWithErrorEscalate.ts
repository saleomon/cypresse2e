let labelWithErrorEscalate: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
  error: string
}[] = [
  {
    title: '[CMTS-13937] Global Escalate outcome cannot be added when a link label contains a space at the end',
    preconditionId: '13937',
    linkLabel: 'ab ',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-13907] Global Escalate outcome cannot be added without a link label',
    preconditionId: '13907',
    linkLabel: '',
    linkType: ' Normal (down) ',
    error: 'Each Link Label must be uniquely named.',
  },
  {
    title: '[CMTS-13934] Global Escalate outcome cannot be added when a link label starts with a digit',
    preconditionId: '13934',
    linkLabel: '123ab',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-13936] Global Escalate outcome cannot be added when a link label contains a space in the middle',
    preconditionId: '13936',
    linkLabel: 'ab ab',
    linkType: ' Normal (down) ',
    error: 'Label must not contain spaces.',
  },
  {
    title: '[CMTS-13930] Global Escalate outcome cannot be added when a link label contains 1 character',
    preconditionId: '13930',
    linkLabel: 'a',
    linkType: ' Normal (down) ',
    error: 'Length should be 2 to 32 chars',
  },
  {
    title: '[CMTS-13931] Global Escalate outcome cannot be added when a link label contains 33 characters',
    preconditionId: '13931',
    linkLabel: 'qqqgsadasdasdasdasgsadasdasdasdas',
    linkType: ' Normal (down) ',
    error: 'Length should be 2 to 32 chars',
  },
]

export default labelWithErrorEscalate
