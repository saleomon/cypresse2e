let labelWithReasonEscalate: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
  reason: string
  sideText: string
  conditionText: string
}[] = [
  {
    title: '[CMTS-13928] Global Escalate outcome can be added without a reason',
    preconditionId: '13928',
    linkLabel: 'down',
    linkType: ' Normal (down) ',
    reason: '',
    sideText: 'down',
    conditionText: 'Escalate',
  },
  {
    title: '[CMTS-13927] Global Escalate outcome reason can be added via the Add Outcome pop-up',
    preconditionId: '13933',
    linkLabel: 'ab_@#%^&',
    linkType: ' Normal (down) ',
    reason: 'Reason for escalation',
    sideText: 'down',
    conditionText: 'Escalate',
  },
]
export default labelWithReasonEscalate
