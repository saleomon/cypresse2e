let userEmptyCreds: {
  title: string
  username: string
  password: string
  errors: { message: string; path: string[]; type: string }
}[] = [
  {
    title: '[CMTS-11005] Login page. Check login with empty username (API)',
    username: '',
    password: Cypress.env('password'),
    errors: {
      message: '"username" is not allowed to be empty',
      path: ['username'],
      type: 'string.empty',
    },
  },
  {
    title: '[CMTS-11006] Login page. Check login with empty password (API)',
    username: Cypress.env('username'),
    password: '',
    errors: {
      message: '"password" is not allowed to be empty',
      path: ['password'],
      type: 'string.empty',
    },
  },
]

export default userEmptyCreds
