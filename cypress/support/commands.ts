import loginAPI from './commands/loginAPI'
import loginUI from './commands/loginUI'
import createNewProject from './commands/createNewProject'
import updateProjectFail from './commands/updateProjectFail'
import createProjectFail from './commands/createProjectFail'
import removeProject from './commands/removeProject'
import sendKeys from './commands/sendKeys'
import fillTagPopUp from './commands/fillTagPopUp'
import selectProjectAndPattern from './commands/selectProjectAndPattern'
import beforeTest from './commands/beforeTest'
import deleteUser from './commands/deleteUser'
import createUser from './commands/createUser'
import precondition from './commands/precondition'
import checkToastMessage from './commands/checkToastMessage'
import checkWarning from './commands/checkWarning'
import selectCustom from './commands/selectCustom'
import multiselect from './commands/multiselect'
import getToken from './commands/getToken'
import getTokenFail from './commands/getTokenFail'
import newProject from './commands/newProject'
import selectNewProject from './commands/selectNewProject'

Cypress.Commands.add('getToken', getToken)
Cypress.Commands.add('getTokenFail', getTokenFail)
Cypress.Commands.add('loginAPI', loginAPI)
Cypress.Commands.add('loginUI', loginUI)
Cypress.Commands.add('createNewProject', createNewProject)
Cypress.Commands.add('updateProjectFail', updateProjectFail)
Cypress.Commands.add('createProjectFail', createProjectFail)
Cypress.Commands.add('removeProject', removeProject)
Cypress.Commands.add('selectProjectAndPattern', selectProjectAndPattern)
Cypress.Commands.add('precondition', precondition)
// @ts-ignore
Cypress.Commands.add('sendKeys', { prevSubject: ['element'] }, sendKeys)
Cypress.Commands.add('fillTagPopUp', fillTagPopUp)
Cypress.Commands.add('checkToastMessage', checkToastMessage)
Cypress.Commands.add('checkWarning', checkWarning)
Cypress.Commands.add('selectCustom', selectCustom)
Cypress.Commands.add('multiselect', multiselect)
Cypress.Commands.add('beforeTest', beforeTest)
Cypress.Commands.add('deleteUser', deleteUser)
Cypress.Commands.add('createUser', createUser)
Cypress.Commands.add('newProject', newProject)
Cypress.Commands.add('selectNewProject', selectNewProject)
