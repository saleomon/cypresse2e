declare global {
  namespace Cypress {
    interface Chainable {
      checkWarning: typeof checkWarning
    }
  }
}

export default function checkWarning(message: string): void {
  cy.get('.ant-message-success').should('exist').and('have.text', message)
  cy.get('.ant-message-success', { timeout: 12000 }).should('not.exist')
}
