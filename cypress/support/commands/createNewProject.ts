import { ProjectInfo } from '../../types/response/ProjectInfo'

declare global {
  namespace Cypress {
    interface Chainable {
      createNewProject: typeof createNewProject
    }
  }
}

export default function createNewProject(projectJson: ProjectInfo, code?: number): Cypress.Chainable<ProjectInfo> {
  let project = projectJson
  project.label = project.label + '_' + Date.now()
  return cy
    .request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/api/projects/import',
      failOnStatusCode: false,
      headers: {
        Authorization: 'Bearer ' + Cypress.env('token'),
      },
      body: project,
    })
    .then((resp) => {
      if (!code) {
        expect(resp.status).equals(201)
        Cypress.env('projectId', resp.body.projectObject.id)
        projectJson.id = resp.body.projectObject.id
        projectJson.label = resp.body.projectObject.label
      } else {
        expect(resp.status).to.not.equal(code)
      }
      return projectJson
    })
}
