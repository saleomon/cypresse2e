import { UserInput } from '../../types/input/UserInput'

declare global {
  namespace Cypress {
    interface Chainable {
      createUser: typeof createUser
    }
  }
}

export default function createUser(userJson: UserInput): Cypress.Chainable<UserInput> {
  let user = userJson
  user[0].username = userJson[0].username + '-' + Date.now()
  let login = 'testuser'
  user[0].email = login + '_' + Date.now() + '@example.com'
  return cy
    .request({
      method: 'POST',
      url: Cypress.env('baseUrlAPI') + '/admin/users',
      headers: {
        Authorization: 'Bearer ' + Cypress.env('token'),
      },
      body: user,
    })
    .then((resp) => {
      expect(resp.status).equals(200)
      Cypress.env('testUser', resp.body.createdUsers[0].username)
      Cypress.env('userEmail', resp.body.createdUsers[0].email)
      expect(resp.body.createdUsers[0].username).equal(userJson[0].username)
      expect(resp.body.createdUsers[0].firstName).equal(userJson[0].firstName)
      expect(resp.body.createdUsers[0].lastName).equal(userJson[0].lastName)
      expect(resp.body.createdUsers[0].email).equal(userJson[0].email)
      expect(resp.body.createdUsers[0].emailVerified).equal(userJson[0].emailVerified)
      expect(resp.body.createdUsers[0].enabled).equal(userJson[0].enabled)
      expect(resp.body.createdUsers[0].credentials[0].temporary).equal(userJson[0].credentials[0].temporary)
      expect(resp.body.createdUsers[0].credentials[0].type).equal(userJson[0].credentials[0].type)
      expect(resp.body.createdUsers[0].credentials[0].value).equal(userJson[0].credentials[0].value)
      expect(resp.body.createdUsers[0].realmRoles[0]).equal(userJson[0].realmRoles[0])
      expect(resp.body.createdUsers[0].clientRoles['heartbeat-app'][0]).equal(userJson[0].clientRoles['heartbeat-app'][0])
      expect(resp.body.createdUsers[0].clientRoles['heartbeat-app'][1]).equal(userJson[0].clientRoles['heartbeat-app'][1])
      expect(resp.body.createdUsers[0].clientRoles['heartbeat-app'][2]).equal(userJson[0].clientRoles['heartbeat-app'][2])
      expect(resp.body.createdUsers[0].clientRoles['heartbeat-app'][3]).equal(userJson[0].clientRoles['heartbeat-app'][3])
      expect(resp.body.createdUsers[0].clientRoles['heartbeat-app'][4]).equal(userJson[0].clientRoles['heartbeat-app'][4])
      expect(resp.body.createdUsers[0].attributes.organization_id).equal(userJson[0].attributes.organization_id)
      return userJson
    })
}
