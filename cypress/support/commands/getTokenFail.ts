declare global {
  namespace Cypress {
    interface Chainable {
      getTokenFail: typeof getTokenFail
    }
  }
}

export default function getTokenFail(username: string, password: string): void {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    failOnStatusCode: false,
    body: {
      username,
      password,
    },
  }).then((response) => {
    expect(response.body.status).eql('failed')
    expect(response.body.message).eql(
      'unable to login user ' + username + ': Error: Request failed with status code 401',
    )
  })
}
