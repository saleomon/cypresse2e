import { ProjectInfo } from '../../types/response/ProjectInfo'

declare global {
  namespace Cypress {
    interface Chainable {
      newProject: typeof newProject
    }
  }
}

export default function newProject(preconditionId: string) {
  cy.fixture('projects-templates/' + preconditionId + '.json').as('project')

  cy.get<ProjectInfo>('@project').then((json: ProjectInfo) => {
    cy.loginAPI()
    cy.createNewProject(json).as('current-project')
    cy.loginUI()
  })

  cy.get<ProjectInfo>('@current-project').then((projectInfo: ProjectInfo) => {
    cy.selectNewProject(projectInfo.label)
  })
}
