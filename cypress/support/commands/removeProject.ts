declare global {
  namespace Cypress {
    interface Chainable {
      removeProject: typeof removeProject
    }
  }
}

export default function removeProject(projectId: string): Cypress.Chainable {
  return cy
    .request({
      method: 'DELETE',
      url: Cypress.env('baseUrlAPI') + '/api/projects/' + projectId,
      headers: {
        Authorization: 'Bearer ' + Cypress.env('token'),
      },
    })
    .then((resp) => {
      expect(resp.body.success).equals(true)
    })
}
