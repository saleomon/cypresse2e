export interface TagPopUpInput {
  nodeLabel: string
  nodeDescription: string
  linkLabel: string
  linkDescription: string
}
