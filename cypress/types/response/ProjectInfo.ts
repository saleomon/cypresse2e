export interface ProjectInfo {
  id: string
  type: string
  label: string
  parent: string
  outcomes: Outcome[]
  parentId: string
  patterns: Pattern[]
  templates: Templates
  description: string
  projectType: string
  destinationPdes: never[]
  outcomesBaseline: never[]
  globalRepairRules: GlobalRepairRule[]
  projectObjectVersion: number
}

export interface GlobalRepairRule {
  id: string
  label: string
  outcome: string
  repairType: string
  description: string
  numberOfAttempts: number
}

export interface Outcome {
  id: string
  type: string
  color: string
  label: string
  reasons: never[]
  percentage: number
  description: string
  outcomeType: OutcomeType
  outcomeValue: OutcomeValue
  destinationPattern: string
  bucket: string
}

export enum OutcomeType {
  Global = 'Global',
  Project = 'Project',
  Skill = 'Skill',
}

export enum OutcomeValue {
  Negative = 'Negative',
  Neutral = 'Neutral',
  Positive = 'Positive',
}

export interface Pattern {
  id: string
  type: string
  label: string
  mapper: never[]
  parent: string
  channels: Channel[]
  entities: never[]
  flowData: PatternFlowDatum[]
  nodeType: string
  outcomes: Outcome[]
  parentId: string
  subflows: Subflow[]
  description: string
  patternType: string
  entityGroups: EntityGroup[]
}

export interface Channel {
  id: string
  type: ChannelType
  label: PrimaryKeyEnum
  description: ChannelDescription
  content?: ChannelContent[]
}

export interface ChannelContent {
  id: string
  value: string
}

export enum ChannelDescription {
  Empty = '',
  RichWebchatUI = 'Rich webchat UI',
  TextMessaging = 'Text messaging',
  VoiceOverThePhone = 'Voice over the phone',
}

export enum PrimaryKeyEnum {
  Default = 'default',
  Ivr = 'IVR',
  Rwc = 'RWC',
  SMS = 'SMS',
}

export enum ChannelType {
  Channel = 'channel',
  OutputCondition = 'outputCondition',
}

export interface EntityGroup {
  id: string
  type: string
  label: string
  dependsOn: never[]
  primaryKey: PrimaryKeyEnum
  description: string
  sharedEndPoint: boolean
}

export interface PatternFlowDatum {
  id: string
  flow: Flow[]
  type: FlowDatumType
  label: string
  parent: string
  nodeType: string
  parentId: string
  description: string
  applyInputsToFlow: boolean
  nodeDisplayOption: NodeDisplayOption
  containsPii: boolean
  subflowId: string
  output: Channel[]
  skills: string[]
  channels: never[]
}

export interface Flow {
  id: string
  type: FlowType
  label: FlowLabel
  order: number
  toNode: string
  description?: FlowDescription
  gotoNodeType?: GotoNodeType
  linkType?: LinkType
}

export enum FlowDescription {
  Default = 'default',
  Empty = '',
  StartExperience = 'Start Experience',
}

export enum GotoNodeType {
  AddPoint = 'addPoint',
  Normal = 'normal',
}

export enum FlowLabel {
  Default = 'default',
  Repair = 'Repair',
}

export enum LinkType {
  Down = 'down',
  Loop = 'loop',
}

export enum FlowType {
  FlowCondition = 'flowCondition',
}

export enum NodeDisplayOption {
  Description = 'description',
}

export enum FlowDatumType {
  Node = 'node',
}

export interface Subflow {
  id: string
  type: string
  label: string
  parent: string
  parentId: string
  subflows: never[]
  description: string
  flowDataSet: FlowDataSet[]
}

export interface FlowDataSet {
  channel: Channel
  flowData: FlowDataSetFlowDatum[]
}

export interface FlowDataSetFlowDatum {
  id: string
  flow: Flow[]
  type: FlowDatumType
  label: string
  parent: string
  nodeType: string
  parentId: string
  description: string
  applyInputsToFlow: boolean
  nodeDisplayOption: NodeDisplayOption
  input?: InputElement[]
  output?: Output[]
  nodeRules?: NodeRules
  templateId?: string
  containsPii?: boolean
  channelSettings?: ChannelSettings
}

export interface ChannelSettings {
  ttsEngine: string
  acceptDTMF: boolean
  talkingSpeed: number
}

export interface InputElement {
  id: string
  type: InputType
  label: string
  order: number
  parser: string
  inputType: string
}

export enum InputType {
  InputCondition = 'inputCondition',
}

export interface NodeRules {
  id?: string
  repairRules: never[]
  useGlobalRepairs: boolean
  useGlobalEscalate: boolean
  useGlobalFallback: boolean
}

export interface Output {
  id: string
  type: ChannelType
  label: string
  edited: boolean
  content: PurpleContent[]
  template: boolean
}

export interface PurpleContent {
  id: string
  value: string
  channel: PrimaryKeyEnum
  dynamic: boolean
  contentType: string
}

export interface Templates {
  YesNoTemplateId: YesNoTemplateID
  defaultOpenTemplate: YesNoTemplateID
  defaultClosedTemplate: YesNoTemplateID
  defaultPromptTemplate: YesNoTemplateID
}

export interface YesNoTemplateID {
  id: string
  label: string
  hasPre: boolean
  hasPost: boolean
  hasRepair: boolean
  description: string
  templateType: string
  channelConditions: ChannelCondition[]
}

export interface ChannelCondition {
  flow: never[]
  input: ChannelConditionInput
  output: never[]
  channelId: string
}

export interface ChannelConditionInput {
  models: Model[]
}

export interface Model {
  id: string
  type: InputType
  label: ModelLabel
  parser: Parser
  inputType: InputTypeEnum
  containsPii: boolean
  description: ModelDescription
}

export enum ModelDescription {
  NegativeUserResponse = 'negative user response',
  PositiveUserResponse = 'positive user response',
}

export enum InputTypeEnum {
  Matcher = 'Matcher',
}

export enum ModelLabel {
  No = 'No',
  Yes = 'Yes',
}

export enum Parser {
  NoNopeNah = 'no, nope, nah',
  YesYeahYah = 'yes, yeah, yah',
}
